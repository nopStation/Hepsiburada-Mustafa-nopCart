//
//  SlideMenuModel.swift
//  NopCart
//
//  Created by BS-125 on 6/15/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper
class SlideMenuModel: NSObject {

}
class Children: Mappable {
    
    var position: String?
    var category_id: String?
    var children: [Children]?
    var level: String?
    var parent_id: String?
    var name: String?
    var is_active: String?
    var isSelected: String? = "NO"
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        position <- map["position"]
        category_id <- map["category_id"]
        children <- map["children"]
        level <- map["level"]
        parent_id <- map["parent_id"]
        name <- map["name"]
        is_active <- map["is_active"]
        isSelected <- map["isSelected"]
    }
}
