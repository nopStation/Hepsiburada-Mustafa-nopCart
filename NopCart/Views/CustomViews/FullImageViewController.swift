//
//  FullImageViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 1/29/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit

class FullImageViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var imageView: UIImageView!

    var productName  = String ()
    
    
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    var productImage: UIImage?

    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.image = productImage

        productNameLbl.text = self.productName
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backBtnAct(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)

    }
    
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView?
    {
        return self.imageView
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
