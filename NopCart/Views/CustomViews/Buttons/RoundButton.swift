//
//  RoundButton.swift
//  NopCart
//
//  Created by BS-125 on 6/13/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
@IBDesignable

   class RoundButton: UIButton {

        override init(frame: CGRect) {
            super.init(frame: frame)
            commonSetup()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            commonSetup()
        }
        
        func commonSetup() {
            layer.cornerRadius = 4.0
            layer.masksToBounds = false
            layer.borderWidth = 2
            layer.borderColor = UIColor(red: 230/255.0, green: 136/255.0, blue: 1/255.0, alpha: 1.0).CGColor
            //layer.shadowColor = UIColor.darkGrayColor().CGColor
            //layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
            //layer.shadowOpacity = 1.0
            //layer.shadowRadius = 1.0
        }


}
