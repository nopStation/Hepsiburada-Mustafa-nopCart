//
//  ProductsGridFlowLayout.swift
//  NopCart
//
//  Created by BS-125 on 8/2/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit

class ProductsGridFlowLayout: UICollectionViewFlowLayout {
    
    // here you can define the height of each cell
    //let itemHeight: CGFloat = 120
    var gridsPerRow = CGFloat ()
    
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    /**
     Sets up the layout for the collectionView. 1pt distance between each cell and 1pt distance between each row plus use a vertical layout
     */
    func setupLayout() {
        minimumInteritemSpacing = 1
        minimumLineSpacing = 1
        scrollDirection = .Vertical
    }
    
    /// here we define the width of each cell, creating a 2 column layout. In case you would create 3 columns, change the number 2 to 3
    func itemWidth() -> CGFloat {
        return (CGRectGetWidth(collectionView!.frame)/gridsPerRow)-1
    }
    
    func itemHeight () -> CGFloat {
        
        let bound = UIScreen.mainScreen().bounds
        let hr: CGFloat = 272/206
        let w: CGFloat = (bound.width/2)-2
        let h: CGFloat
        if gridsPerRow == 3 {
            h  = hr*w - 50
        }else {
         h  = hr*w
        }
        
        return h
    }
    
    
    override var itemSize: CGSize {
        set {
            self.itemSize = CGSizeMake(itemWidth(), itemHeight())
        }
        get {
            return CGSizeMake(itemWidth(), itemHeight())
        }
    }
    
    override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
}