//
//  MenuDropDownVC.swift
//  NopCart
//
//  Created by BS-125 on 6/16/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit


protocol MenuDropDownVCDelegate{
    
    func navigateToSelectedViewController(sender: String)
}

class MenuDropDownVC: UIViewController {

    var delegate:MenuDropDownVCDelegate?
    @IBOutlet weak var tableView: UITableView!
    var customNavBarView : CustomNavBarView?
    var menuList  = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if (defaults.stringForKey("accessToken") != nil) {
            menuList = AppConstants.DropDownMenu.DropDownListForSignOut
        }
        else {
            menuList = AppConstants.DropDownMenu.DropDownListForSignUp
            
        }
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "MenuDropDownCell")
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    
    func initialSetUp () {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if (defaults.stringForKey("accessToken") != nil) {
            menuList = AppConstants.DropDownMenu.DropDownListForSignOut
        }
        else {
            menuList = AppConstants.DropDownMenu.DropDownListForSignUp
            
        }
   
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {


        print("MenuDropDownVC Call")

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - UITableView Delegate & Datasource
    //-----------------------------------------------------------------
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.menuList.count
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
        
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuDropDownCell")!
        cell.backgroundColor = UIColor.clearColor()
        cell.contentView.backgroundColor = UIColor.clearColor()
        
        let data = menuList[indexPath.row]
        let title = data[fieldTitleKey] as? String
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = title
        cell.textLabel?.textColor = UIColor.whiteColor()
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let data = menuList[indexPath.row]
        let VC = data[VCIdentifierKey] as? String
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if (defaults.stringForKey("accessToken") != nil) {
          
            if VC == "SignOut" {
                menuList = []
                defaults.removeObjectForKey("accessToken")
                menuList = AppConstants.DropDownMenu.DropDownListForSignUp
                self.tableView.reloadData()
                self.delegate?.navigateToSelectedViewController(VC!)
                
            }
//            else if VC == "WishlistVC" {
//                
//                
//            }
//            else if VC == "MyOrdersVC" {
                
                
//            }
        else if VC == "BarcodeReaderVC" {
                
                
            }
            else{
                self.delegate?.navigateToSelectedViewController(VC!)
            }
        }else {
            
            if VC == "SettingsVC" {
              self.delegate?.navigateToSelectedViewController(VC!)
            }else if VC == "ContactUsVC" {
              self.delegate?.navigateToSelectedViewController(VC!)
            }else if VC == "BarcodeReaderVC" {
              //self.delegate?.navigateToSelectedViewController(VC!)
            }else {
              self.delegate?.navigateToSelectedViewController("SignInVC")
                
            }
        }
    
    }
    
}
