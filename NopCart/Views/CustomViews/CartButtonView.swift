//
//  CartButtonView.swift
//  NopCart
//
//  Created by BS-125 on 6/16/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit

protocol CartButtonViewDelegate{
    
   func pushToSelectedViewController(sender: String)
}

class CartButtonView: UIView {

    @IBOutlet weak var cartBtn: UIButton!
     var delegate:CartButtonViewDelegate?
    @IBOutlet weak var cartLabel: CustomCartLabel!
    
    
    
    @IBAction func cartBtnAction(sender: AnyObject) {
        
        
        let count = Int(appDelegate.cartView.cartLabel.text!)
        
        if count > 0 {
        
        self.delegate?.pushToSelectedViewController("ShoppingCartVC")
            
        }else {
            
           AppUtility.showToast("Cart Is Empty", view: appDelegate.window!)
            
        }
        
        //NSNotificationCenter.defaultCenter().postNotificationName("com.notification.backHomePage", object: nil);
        
//        let storyboard: UIStoryboard = UIStoryboard (name: "Main", bundle: nil)
//        let vc: ShoppingCartVC = storyboard.instantiateViewControllerWithIdentifier("ShoppingCartVC") as! ShoppingCartVC
//        let currentController = self.getCurrentViewController()
//        currentController?.navigationController?.pushViewController(vc, animated: true)

        
    }
    
    
    //MARK: Constructor
    
//    func getCurrentViewController() -> UIViewController? {
//        
//        if let rootController = UIApplication.sharedApplication().keyWindow?.rootViewController {
//            var currentController: UIViewController! = rootController
//            while( currentController.presentedViewController != nil ) {
//                currentController = currentController.presentedViewController
//            }
//            return currentController
//        }
//        return nil
//        
//    }
//    
//    required init(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)!
//    }
//    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//    }
//    
//    //MARK: Instance Methods
//    
//    func setDelegate(tdelegate:CartButtonViewDelegate){
//        self.delegate = tdelegate
//    }

}
