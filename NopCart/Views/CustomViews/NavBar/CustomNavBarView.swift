//
//  CustomNavBarView.swift
//  CustomTabBarDemo
//
//  Created by Masudur Rahman on 3/10/16.
//  Copyright © 2016 BS-23. All rights reserved.
//

import UIKit
//import DropDown

class CustomNavBarView: UIView {

    
    @IBOutlet weak var suggestionTableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var navigationBackButton: UIButton!
    @IBOutlet weak var navigationTitleLabel: UILabel!
    @IBOutlet weak var navigationLogoImageView: UIImageView!
    @IBOutlet weak var navigationSearchButton: UIButton!
    @IBOutlet weak var navigationMenuButton: UIButton!
   
    @IBOutlet weak var navigationSearchButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var navigationMenuButtonWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var shoppingCartLabel: CustomCartLabel!
    
    var boolFlag : Bool = false
    
    var parentViewController = UIViewController()
    
    @IBAction func navigationBackButtonAction(sender: AnyObject) {
        

        
        if boolFlag {
           appDelegate.slideMenuController.toggleLeftSideMenuCompletion(nil)

        }else {
            self.parentViewController.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func navigationSearchButtonAction(sender: AnyObject) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let searchView = storyBoard.instantiateViewControllerWithIdentifier("SearchVC") as? SearchVC
        self.parentViewController.navigationController?.pushViewController(searchView!, animated: true)
 
        
    }
    
    @IBAction func menuButtonAction(sender: AnyObject) {
        
       appDelegate.slideMenuController.toggleLeftSideMenuCompletion(nil)
        
        
    }
    
    @IBAction func navigationCartButtonAction(sender: AnyObject) {
        
    }
    
    @IBAction func navigationMenuButtonAction(sender: AnyObject) {

    }
    
    func setNavigationTitle(title: String) {
        self.navigationTitleLabel.text = title
    }
    
    /// Back Button
    func showNavigationBackButton(show: Bool) {
        self.navigationBackButton.hidden = !show
    }
    
    /// Title Label
    func showNavigationTitleLabel(show: Bool) {
        self.navigationTitleLabel.hidden = !show
    }
    
    /// Logo
    func showNavigationLogoImageView(show: Bool) {
        self.navigationLogoImageView.hidden = !show
    }
    
    /// Search Button
    func showNavigationSearchButton(show: Bool) {
        self.navigationSearchButton.hidden = !show
        //self.navigationSearchButtonWidthConstraint.constant = (show) ? 35 : 0
    }
    
//    /// Cart Button
//    func showNavigationCartButton(show: Bool) {
//        self.navigationCartButton.hidden = !show
//        //self.navigationCartButtonWidthConstraint.constant = (show) ? 35 : 0
//        self.shoppingCartLabel.hidden = !show
//    }
    
    /// Right Margin
    func createRightMarginToNavigationCartButton() {
        //self.navigationCartButtonTrailingConstraint.constant = 2
    }
    
    /// Menu  Button
    func showNavigationMenuButton(show: Bool) {
        //self.navigationMenuButton.hidden = !show
        //self.navigationMenuButtonWidthConstraint.constant = (show) ? 35 : 0
    }
    
    func updateShoppingCartCount() {
//        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
//            shoppingCartLabel.hidden = true
//            shoppingCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
//        } else {
//            shoppingCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
//            shoppingCartLabel.hidden = false
//        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
