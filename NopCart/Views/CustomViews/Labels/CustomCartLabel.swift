//
//  CustomCartLabel.swift
//  CustomTabBarDemo
//
//  Created by Masudur Rahman on 3/10/16.
//  Copyright © 2016 BS-23. All rights reserved.
//

import UIKit

class CustomCartLabel: UILabel {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 4.0
        self.layer.masksToBounds = true;
    }
    
    override func intrinsicContentSize() -> CGSize {
        let contentSize = super.intrinsicContentSize()
        return CGSizeMake(contentSize.width + 8, contentSize.height)
    }

}
