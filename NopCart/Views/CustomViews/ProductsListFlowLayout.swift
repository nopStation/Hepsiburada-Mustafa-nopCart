//
//  ProductsListFlowLayout.swift
//  NopCart
//
//  Created by BS-125 on 8/2/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit

class ProductsListFlowLayout: UICollectionViewFlowLayout {
    
    //let itemHeight: CGFloat = 120
    
    override init() {
        super.init()
        setupLayout()
    }
    
    /**
     Init method
     
     - parameter aDecoder: aDecoder
     
     - returns: self
     */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    /**
     Sets up the layout for the collectionView. 0 distance between each cell, and vertical layout
     */
    func setupLayout() {
        minimumInteritemSpacing = 0
        minimumLineSpacing = 1
        scrollDirection = .Vertical
    }
    
    
    func itemHeight () -> CGFloat {
        
      let bound = UIScreen.mainScreen().bounds
      let hr: CGFloat = 272/206
      let w: CGFloat = (bound.width)-2

      let h: CGFloat
      h  = hr*w - 110
        
        return h
    }
    
    func itemWidth() -> CGFloat {
        return CGRectGetWidth(collectionView!.frame)
    }
    
    override var itemSize: CGSize {
        set {
            self.itemSize = CGSizeMake(itemWidth(), itemHeight())
        }
        get {
            return CGSizeMake(itemWidth(), itemHeight())
        }
    }
    
    override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
}