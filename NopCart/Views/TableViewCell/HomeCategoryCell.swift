//
//  HomeCategoryCell.swift
//  NopCart
//
//  Created by BS-125 on 6/13/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage


class HomeCategoryCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var array = [AnyObject]()
    var sectionIndex = NSInteger()
    var totalSection = NSInteger()
    
    @IBOutlet weak var titleLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.collectionView.registerNib(UINib(nibName: "HomeCategoryCollectionCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "HomeCategoryCollectionCell")

        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func getCategoryArray(dataArray: [AnyObject] ,sectionIndex:NSInteger, totalSection : NSInteger) -> Void{
        
        self.sectionIndex = sectionIndex
        self.totalSection = totalSection
        self.array = dataArray as [AnyObject]
        self.collectionView?.setContentOffset(CGPointZero, animated: true)
        self.collectionView?.reloadData()
    }
    
}


extension HomeCategoryCell : UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        //return 5
        if self.sectionIndex == 0 {
            return self.array.count
        }
        else if self.sectionIndex == self.totalSection-1 {
            return self.array.count
            
        } else {
            let homePageCategory = self.array as! [HomePageCategoryModel]
            return homePageCategory[0].product!.count
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeCategoryCollectionCell", forIndexPath: indexPath) as! HomeCategoryCollectionCell
        
        if self.sectionIndex == 0 {
            
            let homePageFeaturedArray = self.array as! [HomePageFeaturedModel]
            let imageUrl = homePageFeaturedArray[indexPath.item].DefaultPictureModel?.ImageUrl
            
            cell.productImage!.sd_setImageWithURL(NSURL(string: imageUrl!))
            cell.titleLabel.attributedText = AppUtility.getAttributeString(homePageFeaturedArray[indexPath.item].Name!, secondString: "\n\(homePageFeaturedArray[indexPath.item].ProductPrice!.Price!)", secondStrColor: AppUtility.getPrimaryCellColor())

            
        } else if self.sectionIndex == self.totalSection-1 {
            
            let homePageMenuFactureArray = self.array as! [HomePageManufactureModel]
            let imageUrl = homePageMenuFactureArray[indexPath.item].DefaultPictureModel?.ImageUrl
            cell.productImage!.sd_setImageWithURL(NSURL(string: imageUrl ?? ""))
            cell.titleLabel.text  = homePageMenuFactureArray[indexPath.item].Name
            
            
            
        }else {
            
            let homePageCategory = self.array as! [HomePageCategoryModel]
            cell.productImage!.sd_setImageWithURL(NSURL(string: (homePageCategory[0].product![indexPath.item].DefaultPictureModel?.ImageUrl)!))
            cell.titleLabel.attributedText = AppUtility.getAttributeString(homePageCategory[0].product![indexPath.item].Name!, secondString: "\n\(homePageCategory[0].product![indexPath.item].ProductPrice!.Price!)", secondStrColor: AppUtility.getPrimaryCellColor())
            
        }
        
        return cell
    }

    
    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        
        //return CGSizeMake(collectionView.bounds.size.width/2, 265)
        let bound = UIScreen.mainScreen().bounds
        let hr: CGFloat = 272/200
        let w: CGFloat = (bound.width/2.4)
        let h: CGFloat = hr*w
        return CGSize(width: ceil(w),height: ceil(h))
        
    }
    
    
}

extension HomeCategoryCell : UICollectionViewDelegate{
    
    func  collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        
        print(self.sectionIndex)
        print(indexPath.row)
        print(indexPath.item)

        if self.sectionIndex == 0 {
            
            let homePageFeaturedArray = self.array as! [HomePageFeaturedModel]
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let productDetails = storyBoard.instantiateViewControllerWithIdentifier("ProductDetailsVC") as? ProductDetailsVC
            productDetails!.productId  = homePageFeaturedArray[indexPath.item].Id
            appDelegate.navigationController.pushViewController(productDetails!, animated: true)
            
            
            
        } else if self.sectionIndex == self.totalSection-1 {
            
            let homePageMenuFactureArray = self.array as! [HomePageManufactureModel]
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let productDetails = storyBoard.instantiateViewControllerWithIdentifier("ProductDetailsVC") as? ProductDetailsVC
            productDetails!.productId  = homePageMenuFactureArray[indexPath.item].Id
            appDelegate.navigationController.pushViewController(productDetails!, animated: true)


        }else {
            
            let homePageCategory = self.array as! [HomePageCategoryModel]
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let productDetails = storyBoard.instantiateViewControllerWithIdentifier("ProductDetailsVC") as? ProductDetailsVC
            productDetails!.productId  = homePageCategory[0].product![indexPath.item].Id
            appDelegate.navigationController.pushViewController(productDetails!, animated: true)

            
        }
               
        
        /*
        if self.sectionIndex==0 {
            
            self.homePageProductArray = self.array as! [GetHomePageProducts]
            let productDetailsViewController = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            productDetailsViewController.productId  = self.homePageProductArray[indexPath.item].Id
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.navController?.pushViewController(productDetailsViewController, animated: true)
            
        } else if self.sectionIndex == self.apiManagerClient.categoryArray.count-1 {
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let aVariable = appDelegate.slideContainer
            
            self.homePageMenuFactureArray = self.array as! [HomePageMenuFacture]
            let catDetailsViewController = CategoryDetailsViewController(nibName: "CategoryDetailsViewController", bundle: nil)
            catDetailsViewController.categoryId  = self.homePageMenuFactureArray[indexPath.item].idd
            catDetailsViewController.categoryName = self.homePageMenuFactureArray[indexPath.item].name
            catDetailsViewController.manufacturerFlag = true
            
            
            appDelegate.slideContainer.centerViewController = catDetailsViewController
            let rightMenuViewC = RightSlideMenuViewController(nibName: "RightSlideMenuViewController", bundle: nil)
            aVariable.rightMenuViewController = rightMenuViewC;
            
        }
            
        else {
            
            self.homePageCategoriesModels = self.array as! [HomePageCategoryModel]
            
            let productDetailsViewController = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            productDetailsViewController.productId  = self.homePageCategoriesModels[0].products[indexPath.item].Id
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.navController?.pushViewController(productDetailsViewController, animated: true)
            
        }*/
        
    }
}
