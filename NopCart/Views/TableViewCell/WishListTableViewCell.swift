//
//  WishListTableViewCell.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/12/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import Alamofire

class WishListTableViewCell: UITableViewCell {

    var request: Alamofire.Request?
    
    
    
    @IBOutlet weak var deleteBtn: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!

}
