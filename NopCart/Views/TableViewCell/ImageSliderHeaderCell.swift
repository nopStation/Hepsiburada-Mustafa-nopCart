//
//  ImageSliderHeaderCell.swift
//  NopCart
//
//  Created by BS-125 on 6/13/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import Alamofire


class ImageSliderHeaderCell: UITableViewCell , UIScrollViewDelegate {
    
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var imageArray : [AnyObject] = []
    var counter: CGFloat = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.calculatePageNumber()
    }
    
    func calculatePageNumber() {
        let pageNumber = ceil(self.imageScrollView.contentOffset.x / self.imageScrollView.frame.size.width)
        self.pageControl.currentPage = Int(pageNumber)
    }
    
    func populateImage()
    {
        self.imageScrollView.delegate = self
        
        self.pageControl.numberOfPages = self.imageArray.count
        
        self.imageScrollView.bounds.size.width = self.bounds.width
        self.imageScrollView.bounds.size.height = self.bounds.height
        
        let width = UIScreen.mainScreen().bounds.width - 16
        let height = self.bounds.height
        
        
        for i in 0 ..< self.imageArray.count {
            
            let imageStr = self.imageArray[i]
            
            let imageView : UIImageView = UIImageView()
            imageView.sd_setImageWithURL(NSURL(string: imageStr as! String))
            
            imageView.frame = CGRectMake(width*CGFloat(i), 0.0, width, height)
            self.imageScrollView.addSubview(imageView)
        }
        
        self.imageScrollView.contentSize = CGSizeMake(width*CGFloat(self.imageArray.count),height)
        NSTimer.scheduledTimerWithTimeInterval(15.0, target: self, selector: #selector(ImageSliderHeaderCell.fireScroll), userInfo: nil, repeats: true)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func fireScroll() {
        counter = self.imageScrollView.contentOffset.x
        if counter < self.imageScrollView.contentSize.width - self.bounds.width {
            counter += self.imageScrollView.bounds.width
        } else {
            counter = 0
        }
        
        self.imageScrollView.setContentOffset(CGPointMake(counter, 0), animated: true)
    }
}
