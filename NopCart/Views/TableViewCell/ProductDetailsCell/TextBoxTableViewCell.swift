//
//  TextBoxTableViewCell.swift
//  NopCommerce
//
//  Created by BS-125 on 12/3/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

class TextBoxTableViewCell: UITableViewCell {

    
    @IBOutlet weak var titleLbl: UITextField!
    @IBOutlet weak var nameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
