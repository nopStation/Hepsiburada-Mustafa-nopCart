//
//  SelectedIndexCell.swift
//  NopCart
//
//  Created by BS-125 on 6/16/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit

class SelectedIndexCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
