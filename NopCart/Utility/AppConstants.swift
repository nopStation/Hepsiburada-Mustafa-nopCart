//
//  CategoryListVC.swift
//  NopCart
//
//  Created by BS85 on 5/3/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import Foundation
import UIKit


//-----------------------
enum FieldType: String {
    case TextField = "TextField"
    case TextView = "TextView"
    case DropDown = "DropDown"
}

let formTitleKey = "formTitle"
let formButtonTitleKey = "formButtonTitle"
let fieldTitleKey = "fieldTitle"
let isRequiredKey = "isRequired"
let isSecuredKey = "isSecured"
let buttonTitleKey = "buttonTitle"
let typeKey = "type"
let textField = "textField"
let textView = "textView"
let dropDown = "dropDown"
let VCIdentifierKey = "VCIdentifierKey"
//-----------------------

let emptyString = ""
let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//var BaseURL        = "http://apps.nop-station.com/api/"
struct AppConstants {
    
    
    struct Api {
        
        //var defaultBaseUrl                      = "http://apps.nop-station.com/api/"
        //static var baseURL                      = SessionManager.manager.baseURL
        //"http://apps.nop-station.com/api/"
        
        static let HomePageBanner               = "homepagebanner"
        static let HomePageFeaturedProduct      = "homepageproducts"
        static let HomePageManufacture          = "homepagemanufacture"
        static let HomePageCategory             = "catalog/homepagecategorieswithproduct"
        static let Categories                   = "categories"
        static let Search                       = "catalog/search"
        static let CustomerInfo                 = "customer/info"
        static let ProductDetails               = "productdetails"
        static let ProductPrice                 = "ProductDetailsPagePrice"
        static let RelatedProducts              = "relatedproducts"
        static let AddProductToCart             = "AddProductToCart"
        static let ShoppingCart                 = "ShoppingCart"
        static let ShopppingCart_Delete_Update_Increment = "ShoppingCart/UpdateCart"
        static let CheckoutForGuest             = "checkout/opccheckoutforguest"
        static let ApplyCheckoutAttribut        = "ShoppingCart/applycheckoutattribute"
        static let DiscountCoupon               = "ApplyDiscountCoupon"
        static let RemoveDiscountCoupon         = "RemoveDiscountCoupon"
        static let BillingAddressForm           = "checkout/billingform"
        static let StatesByCountryId            = "country/getstatesbycountryid"
        static let CheckoutSaveAddressID        = "checkout/checkoutsaveadressid"
        static let CheckoutSaveAddress          = "checkout/checkoutsaveadress"
        static let GetPaymentMethod             = "checkout/checkoutgetpaymentmethod"
        static let SavePaymentMethod            = "checkout/checkoutsavepaymentmethod"
        static let GetShippingMethod            = "checkout/checkoutgetshippingmethods"
        static let SaveShippingMethod           = "checkout/checkoutsetshippingmethod"
        static let CheckoutOrderInformation     = "shoppingcart/checkoutorderinformation"
        static let CheckoutComplete             = "checkout/checkoutcomplete"
        static let CheckAuthorizePayment        = "checkout/checkauthorizepayment"
        static let CustomerAddress              = "customer/addresses"
        static let DeleteCustomerAddress        = "customer/address/remove"
        static let EditCustomerAddress          = "customer/address/edit"
        static let AddNewAddress                = "customer/address/add"
        static let WishListInfo                 = "shoppingCart/wishlist"
        static let Products                     = "category/getAllProductByCategory"
        static let AddItemsToCartFromWishlist   = "ShoppingCart/AddItemsToCartFromWishlist"
        static let UpdateWishList               = "ShoppingCart/UpdateWishlist"
        static let CustomerOrders               = "order/customerorders"
        static let OrderDetails                 = "order/details"
        
        
        //Auth
        static let Login = "login"
        static let Register = "customer/register"


        // User Info
        static let ChangePassword = "customer/changePassword"
        static let UpdateInfo = "customer/updateInfo"
        static let ChangeEmail = "customer/changeEmail"
        
    }
    
    struct TableCell {
        static let MenuCell = "MenuCell"
        static let MenuHeaderCell = "MenuHeaderCell"
    }
    
    struct CollectionCell {
        static let HomePageProductCollectionViewCell = "HomePageProductCollectionViewCell"
    }
    
    struct CollectionReusableView {
        static let ScrollingImageCollectionReusableView = "ScrollingImageCollectionReusableView"
    }
    
    struct OAuthInitiate {
        static let oauth_consumer_key = "858a68a2c3866da8a637b0459cfcf511"
        static let oauth_signature_method = "PLAINTEXT"
        static let oauth_nonce = "ZebuLseffrgrergdrg"
        static let oauth_version = "1.0"
        static let oauth_callback = "http://127.0.0.1:8080/magento_1.9.2.2/my_callback"
        static let oauth_consumer_secret_key = "53ab812e34230cb379f2a3f21d03dd16"
        static let oauth_signature = "\(oauth_consumer_secret_key)&"
    }
    
    struct SoapManagerConstants {
        
        // URL
        static let soapWSDLURL = "http://103.4.146.91:9090/magento_1.9.2.2/api/v2_soap/?wsdl"
        
        // Parameter Key
        static let soapUserNameKey      = "username"
        static let soapApiKey           = "apiKey"
        static let soapSessionIdKey     = "sessionId"
        static let soapCategoryIdKey    = "categoryId"
        static let soapProductIdKey     = "productId"
        static let soapProductKey       = "product"
        static let soapAttributeKey     = "attribute"
        
        // Parameter Value
        static let soapUserNameValue    = "TestAdmin"
        static let soapApiKeyValue      = "admin@bs23"
        
        // Response KeyPath
        static let soapAuthenticationResponseKeyPath = "Body.loginResponse.loginReturn"
        
        // Operation Name
        static let soapAuthenticationOperation              = "login"
        static let soapCategoryTreeOperation                =  "catalogCategoryTree"
        static let soapHomePageProductListOperation         =  "catalogProductList"
        static let soapCatalogCategoryAssignedProducts      =  "catalogCategoryAssignedProducts"
        static let soapCatalogProductInfo                   =  "catalogProductInfo"
        static let soapCatalogProductAttributeMediaList     =  "catalogProductAttributeMediaList"
        static let soapCatalogProductAttributeInfo          =  "catalogProductAttributeInfo"
    }
    
    struct KikShaNotification {
        static let loadCategoryProductListNotification_Home = "com.KisSha.Notification.loadCategoryProductListNotification.Home"
        static let loadCategoryProductListNotification_Gift = "com.KisSha.Notification.loadCategoryProductListNotification.Gift"
        static let loadCategoryProductListNotification_Account = "com.KisSha.Notification.loadCategoryProductListNotification.Account"

    }
    
    
    // From Data Model
    struct FormData {
        
        static let aboutYou = [
            [formTitleKey:"About You"],
            [fieldTitleKey:"First Name",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Last Name",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            //[fieldTitleKey:"Email",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            //[fieldTitleKey:"Number",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            //[fieldTitleKey:"Address",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [buttonTitleKey:"Continue"]
        ]
        
        static let changePassword = [
            [formTitleKey:"Change Password"],
            [fieldTitleKey:"Current Password",isRequiredKey:true,typeKey:textField,isSecuredKey:true],
            [fieldTitleKey:"New Password",isRequiredKey:true,typeKey:textField,isSecuredKey:true],
            [fieldTitleKey:"Confirm New Password",isRequiredKey:true,typeKey:textField,isSecuredKey:true],
            [buttonTitleKey:"Save"]
        ]
        
        static let changeEmail = [
            [formTitleKey:"Change Your Email Address",typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"New Email Address",isRequiredKey:false,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Retype New Email Address",isRequiredKey:false,typeKey:textField,isSecuredKey:false],
            [buttonTitleKey:"Save"]
        ]
        
        static let feeback = [
            [formTitleKey:"Feedback"],
            [fieldTitleKey:"To:",isRequiredKey:false,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Cc:",isRequiredKey:false,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Subject:",isRequiredKey:false,typeKey:textView,isSecuredKey:false],
            [buttonTitleKey:"Submit"]
        ]
        
        static let askAQuestion = [
            [formTitleKey:"Ask a Question"],
            [fieldTitleKey:"Type a Question:",isRequiredKey:false,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"In Categories:",isRequiredKey:false,typeKey:dropDown,isSecuredKey:false],
            [buttonTitleKey:"Submit"]
        ]
        
        static let createAccount = [
            [formTitleKey:"Create an Account",formButtonTitleKey:"Sign In"],
            [fieldTitleKey:"First Name",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Last Name",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Email",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Password",isRequiredKey:true,typeKey:textField,isSecuredKey:true],
            [fieldTitleKey:"Confirm Password",isRequiredKey:true,typeKey:textField,isSecuredKey:true],
            [buttonTitleKey:"Continue"]
        ]
        
        static let signIn = [
            [formTitleKey:"Sign In",formButtonTitleKey:"Sign Up"],
            [fieldTitleKey:"Email",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Password",isRequiredKey:true,typeKey:textField,isSecuredKey:true],
            [buttonTitleKey:"Forgot Password?"],
            [buttonTitleKey:"Continue"]
        ]
        
        static let deliveryAddress = [
            [formTitleKey:"Add New Address",formButtonTitleKey:""],
            [fieldTitleKey:"First Name",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Last Name",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Address",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Address 2",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Telephone",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"City",isRequiredKey:true,typeKey:dropDown,isSecuredKey:false],
            [fieldTitleKey:"Country",isRequiredKey:true,typeKey:dropDown,isSecuredKey:false],
            [buttonTitleKey:"Next"]
        ]
        
        static let giftVoucher = [
            [formTitleKey:"Gift Voucher",formButtonTitleKey:""],
            [fieldTitleKey:"Product ID:",isRequiredKey:false,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Voucher Name",isRequiredKey:false,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Voucher Credit(BDT):",isRequiredKey:false,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Voucher Message:",isRequiredKey:false,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Telephone",isRequiredKey:false,typeKey:textField,isSecuredKey:false],
            [fieldTitleKey:"Address",isRequiredKey:false,typeKey:dropDown,isSecuredKey:false],
            [buttonTitleKey:"Send"]
        ]
    }
    
    
    // My Acount Data Model
    struct MyAccountData {
        
        static let MyAccountLoggedIn = [
            [fieldTitleKey:"My Cart", VCIdentifierKey:"CartVC"],
            [fieldTitleKey:"Checkout", VCIdentifierKey:"CheckoutVC"],
            [fieldTitleKey:"My Orders", VCIdentifierKey:"CartVC"],
            [fieldTitleKey:"Personal Details", VCIdentifierKey:"PersonalDetailsVC"],
            [fieldTitleKey:"Log Out", VCIdentifierKey:"Logout"]
        ]
        
        static let MyAccount = [
            [fieldTitleKey:"Register", VCIdentifierKey:"SignUpVC"],
            [fieldTitleKey:"Log In", VCIdentifierKey:"SignInVC"]
        ]
        
        static let passwordReset = [
            [formTitleKey:"Password Reset",formButtonTitleKey:"Sign In"],
            [fieldTitleKey:"Email",isRequiredKey:true,typeKey:textField,isSecuredKey:false],
            [buttonTitleKey:"Continue"]
        ]
        
    }
    
    
    // More Data Model
    struct MoreData {
        
        static let More = [
            [fieldTitleKey:"My Account"],
            [fieldTitleKey:"Save Items"],
            [fieldTitleKey:"Help"],
            [fieldTitleKey:"Contact Us"],
            [fieldTitleKey:"About"],
            [fieldTitleKey:"Follow Us"]
        ]
        
        static let PersonalDetails = [
            [fieldTitleKey:"About You", VCIdentifierKey:"AboutYouVC"],
            [fieldTitleKey:"Change Password", VCIdentifierKey:"ChangePasswordVC"],
            [fieldTitleKey:"Change Email", VCIdentifierKey:"ChangeEmailVC"],
            //[fieldTitleKey:"Marketing Preference"]
        ]
        
        static let ContactUs = [
            [fieldTitleKey:"Contact Details"],
            [fieldTitleKey:"Provide App Feedback"],
            [fieldTitleKey:"Rate in App Store"]
        ]
    }
    
    
    // More Data Model
    struct DropDownMenu {
        

        
        
        
        
        static let DropDownListForSignUp = [
            [fieldTitleKey:"Sign In", VCIdentifierKey:"SignInVC"],
            [fieldTitleKey:"My Account", VCIdentifierKey:"MyAccountVC"],
            [fieldTitleKey:"Address", VCIdentifierKey:"AddressVC"],
            [fieldTitleKey:"My Orders", VCIdentifierKey:"MyOrdersVC"],
            [fieldTitleKey:"Wishlist", VCIdentifierKey:"WishlistVC"],
            [fieldTitleKey:"Settings", VCIdentifierKey:"SettingsVC"],
            [fieldTitleKey:"Contact Us", VCIdentifierKey:"ContactUsVC"],
            [fieldTitleKey:"Barcode Read", VCIdentifierKey:"BarcodeReaderVC"],
        ]
        static let DropDownListForSignOut = [
            [fieldTitleKey:"Sign Out", VCIdentifierKey:"SignOut"],
            [fieldTitleKey:"My Account", VCIdentifierKey:"MyAccountVC"],
            [fieldTitleKey:"Address", VCIdentifierKey:"AddressVC"],
            [fieldTitleKey:"My Orders", VCIdentifierKey:"MyOrdersVC"],
            [fieldTitleKey:"Wishlist", VCIdentifierKey:"WishlistVC"],
            [fieldTitleKey:"Settings", VCIdentifierKey:"SettingsVC"],
            [fieldTitleKey:"Contact Us", VCIdentifierKey:"ContactUsVC"],
            [fieldTitleKey:"Barcode Read", VCIdentifierKey:"BarcodeReaderVC"],
            ]


    }

    
}