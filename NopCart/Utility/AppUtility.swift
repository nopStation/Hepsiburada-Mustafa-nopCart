//
//  CategoryListVC.swift
//  NopCart
//
//  Created by BS85 on 5/3/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import MBProgressHUD
class AppUtility: NSObject {
  
    
    class func hideLoading(view: UIView) {
        MBProgressHUD.hideAllHUDsForView(view, animated: true)
    }
    
    class func showLoading(view: UIView) {
        MBProgressHUD.showHUDAddedTo(view, animated: true)
    }
    
    
    class func showToast(message:String, view: UIView) {
        
        let globalHud = MBProgressHUD.showHUDAddedTo(view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector(#selector(AppUtility.hideLoading(_:)), withObject: view, afterDelay: 2)
    }
    
    
    class func getHeaderDic() -> [String:String] {
        
        let deviceId = UIDevice.currentDevice().identifierForVendor!.UUIDString
        let headers = ["DeviceId": UIDevice.currentDevice().identifierForVendor!.UUIDString]

        var headerDictionary = headers
        
        print(headerDictionary)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            headerDictionary = ["Token":token,"DeviceId": deviceId]
            print(token)
        }
        
        
        return headerDictionary
        
    }
    
    
    class func backToRootView(navC:UINavigationController) {
        
       self.performSelector(#selector(AppUtility.performAfterTwoSecond(_:)), withObject: navC, afterDelay:2)
       
    }
    
    class func performAfterTwoSecond(navC:UINavigationController) {
        
        let array = (navC.viewControllers) as NSArray
        navC.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
        
    }
    
    
    class func getPrimaryCellColor ()-> UIColor {
        
        
       return UIColor(red: 252/255.0, green: 113/255.0, blue: 0/255.0, alpha: 1.0)
    }
    
    class func getAttributeString(firstString: String, secondString: String, secondStrColor: UIColor) -> NSAttributedString {
        
        let mutAttStr = NSMutableAttributedString()
        
        let firstStr = NSAttributedString(string: firstString, attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(15)])
        
        let secondStr = NSAttributedString(string: secondString, attributes: [NSFontAttributeName:UIFont.systemFontOfSize(15),NSForegroundColorAttributeName:secondStrColor])
        
        mutAttStr.appendAttributedString(firstStr)
        mutAttStr.appendAttributedString(secondStr)
        
        return mutAttStr
    }


}

extension String {
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = dataUsingEncoding(NSUTF8StringEncoding)
            else { return nil}
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:NSUTF8StringEncoding], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension String
{
    func trim() -> String {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
}

extension String {
    
    func stringByAppendingPathComponent(path: String) -> String {
        let nsSt = self as NSString
        return nsSt.stringByAppendingPathComponent(path)
    }
}
