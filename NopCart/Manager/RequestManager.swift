//
//  IFRequestManager.swift
//  InvestFeed
//
//  Created by AAPBD on 10/6/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

import UIKit
import Alamofire

class RequestManager: Manager {
    
    //var baseUrl = NSURL(string: SessionManager.manager.baseURL)
    
     static let manager: RequestManager = {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders = RequestManager.defaultHTTPHeaders
        
        return RequestManager(configuration: configuration)
    }()
    
    override func request(
        method: Alamofire.Method,
        _ URLString: URLStringConvertible,
        parameters: [String: AnyObject]? = nil,
        encoding: ParameterEncoding = .URL,
        headers: [String: String]? = nil) -> Request {
        
        let fullUrl = NSURL(string: URLString.URLString, relativeToURL: NSURL(string: SessionManager.manager.baseURL))
        
        return super.request(method, fullUrl!, parameters: parameters, encoding: encoding, headers: headers)
    }
    
    
    //request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
}