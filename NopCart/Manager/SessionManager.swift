//
//  SessionManager.swift
//  NopCart
//
//  Created by BS-125 on 6/13/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper

class SessionManager: Mappable {
    
    /*
     * ---------------------------------------------------------
     * MARK:- Properties -
     * ---------------------------------------------------------
     */
    var userModel: UserModel?
    var isFromMenu: Bool = false
    
    var token: String?
    var cart_id : NSInteger!
    var shoppingCartCount : NSInteger = 0
    var items_count: String?
    var items_qty: String?
    var productModel = ProductsInfoModel?()
    
    var baseURL = "http://apps.nop-station.com/api/"
    var defaultBaseUrl = "http://apps.nop-station.com/api/"

    /*
     * ---------------------------------------------------------
     * MARK: - Singleton -
     * ---------------------------------------------------------
     */
    //    class let manager = IFSessionManager()
    class var manager:SessionManager {
        return Static.instance
    }
    
    
    private struct Static {
        static let instance = Static.createInstance()
        
        static func createInstance()->SessionManager {
            if let savedData = (try? NSString(contentsOfFile: SessionManager.saveFilePath(), encoding: NSUTF8StringEncoding)) as? String {
                //println("sdsds \(savedData)")
                if let storedInstance = Mapper<SessionManager>().map(savedData) {
                    return storedInstance
                }
            }
            
            let myInstance = SessionManager()
            return myInstance
        }
    }
    
    init() {
        print("KSSessionManager init()")
    }
    
    
    /*
     * ---------------------------------------------------------
     * MARK: - Object mapper Methods -
     * ---------------------------------------------------------
     */
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        userModel <- map["user"]
    }
    
    
    /*
     * ---------------------------------------------------------
     * MARK: - Public Methods -
     * ---------------------------------------------------------
     */
    func isLogedIn() -> Bool {
        return (userModel != nil)
    }
    
    func loginWithUser(user: UserModel) {
        self.userModel = user;
        
        //        if ((baseVC) != nil) {
        //            baseVC?.setAppropriateVC()
        //        }
        
        save()
    }
    
    
    
    func logOut() {
        userModel = nil
        save()
        
        //        if (baseVC != nil) {
        //            baseVC?.setAppropriateVC()
        //        }
    }
    
    func save() {
        /*
         if let savedData = NSString(contentsOfFile: IFSessionManager.saveFilePath(), encoding: NSUTF8StringEncoding, error: nil) as? String {
         println("sdsds \(savedData)")
         if let storedInstance = Mapper<IFSessionManager>().map(savedData) {
         return storedInstance
         }
         }
         */
        
        if let data = Mapper<SessionManager>().toJSONString(self, prettyPrint: false) {
            do {
                try data.writeToFile(SessionManager.saveFilePath(), atomically: true, encoding: NSUTF8StringEncoding)
            } catch let error as NSError {
                NSLog("\(error.localizedDescription)")
            }
            
            //data.writeToFile(KSSessionManager.saveFilePath(), atomically: true, encoding: NSUTF8StringEncoding, error: nil)
        }
    }
    
    
    /*
     * ---------------------------------------------------------
     * MARK: - Private Methods -
     * ---------------------------------------------------------
     */
    class func saveFilePath() -> String {
        
        let folder = NSSearchPathForDirectoriesInDomains(.ApplicationSupportDirectory, .UserDomainMask, true)[0]
        
        if !NSFileManager.defaultManager().fileExistsAtPath(folder) {
            do {
                try NSFileManager.defaultManager().createDirectoryAtPath(folder, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("\(error.localizedDescription)")
            }
        }
        
        let className = NSStringFromClass(self).componentsSeparatedByString(".").last!
        return folder.stringByAppendingPathComponent("\(className).plist")
        
    }
    
}