//
//  ConfirmOrderModel.swift
//  NopCart
//
//  Created by BS-125 on 8/18/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper

class ConfirmOrderModel: Mappable {

    var ShoppingCartModel: Shoppingcartmodel?

    var SuccessMessage: String?

    var ErrorList: [String]?

    var StatusCode: Int = 0

    var OrderTotalModel: Ordertotalmodel?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ShoppingCartModel <- map["ShoppingCartModel"]
        SuccessMessage <- map["SuccessMessage"]
        ErrorList <- map["ErrorList"]
        StatusCode <- map["StatusCode"]
        OrderTotalModel <- map["OrderTotalModel"]
    }
}

class Shoppingcartmodel: Mappable {

    var SuccessMessage: String?

    var ShowSku: Bool = false

    var TermsOfServiceOnOrderConfirmPage: Bool = false

    var CheckoutAttributeInfo: String?

    var StatusCode: Int = 0

    var items: [Items]?

    var giftCardBox: Giftcardbox?

    var OnePageCheckoutEnabled: Bool = false

    var OrderReviewData: Orderreviewdata?

    var Count: Int = 0

    var CheckoutAttributes: [Checkoutattributes]?

    var Warnings: [String]?

    var DiscountBox: Discountbox?

    var TermsOfServiceOnShoppingCartPage: Bool = false

    var ShowProductImages: Bool = false

    var ErrorList: [String]?

    var OrderTotalResponseModel: Ordertotalresponsemodel?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        SuccessMessage <- map["SuccessMessage"]
        ShowSku <- map["ShowSku"]
        TermsOfServiceOnOrderConfirmPage <- map["TermsOfServiceOnOrderConfirmPage"]
        CheckoutAttributeInfo <- map["CheckoutAttributeInfo"]
        StatusCode <- map["StatusCode"]
        items <- map["Items"]
        giftCardBox <- map["GiftCardBox"]
        OnePageCheckoutEnabled <- map["OnePageCheckoutEnabled"]
        OrderReviewData <- map["OrderReviewData"]
        Count <- map["Count"]
        CheckoutAttributes <- map["CheckoutAttributes"]
        Warnings <- map["Warnings"]
        DiscountBox <- map["DiscountBox"]
        TermsOfServiceOnShoppingCartPage <- map["TermsOfServiceOnShoppingCartPage"]
        ShowProductImages <- map["ShowProductImages"]
        ErrorList <- map["ErrorList"]
        OrderTotalResponseModel <- map["OrderTotalResponseModel"]
    }


}

//class Picture: NSObject {
//
//    var ImageUrl: String?
//
//    var CustomProperties: Customproperties?
//
//    var Title: String?
//
//    var FullSizeImageUrl: String?
//
//    var AlternateText: String?
//
//}


class Ordertotalmodel: Mappable {

    var IsEditable: Bool = false

    var Tax: String?

    var DisplayTaxRates: Bool = false

    var SubTotalDiscount: String?

    var SelectedShippingMethod: String?

    var OrderTotal: String?

    var SubTotal: String?

    var SuccessMessage: String?

    var ErrorList: [String]?

    var TaxRates: [Taxrates]?

    var AllowRemovingSubTotalDiscount: Bool = false

    var AllowRemovingOrderTotalDiscount: Bool = false

    var StatusCode: Int = 0

    var Shipping: String?

    var DisplayTax: Bool = false

    var RequiresShipping: Bool = false

    var GiftCards: [String]?

    var RedeemedRewardPointsAmount: String?

    var OrderTotalDiscount: String?

    var PaymentMethodAdditionalFee: String?

    var RedeemedRewardPoints: Int = 0

    var WillEarnRewardPoints: Int = 0
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        IsEditable <- map["IsEditable"]
        Tax <- map["Tax"]
        DisplayTaxRates <- map["DisplayTaxRates"]
        SubTotalDiscount <- map["SubTotalDiscount"]
        SelectedShippingMethod <- map["SelectedShippingMethod"]
        OrderTotal <- map["OrderTotal"]
        SubTotal <- map["SubTotal"]
        SuccessMessage <- map["SuccessMessage"]
        ErrorList <- map["ErrorList"]
        TaxRates <- map["TaxRates"]
        AllowRemovingSubTotalDiscount <- map["AllowRemovingSubTotalDiscount"]
        AllowRemovingOrderTotalDiscount <- map["AllowRemovingOrderTotalDiscount"]
        StatusCode <- map["StatusCode"]
        Shipping <- map["Shipping"]
        DisplayTax <- map["DisplayTax"]
        RequiresShipping <- map["RequiresShipping"]
        GiftCards <- map["GiftCards"]
        
        RedeemedRewardPointsAmount <- map["RedeemedRewardPointsAmount"]
        OrderTotalDiscount <- map["OrderTotalDiscount"]
        PaymentMethodAdditionalFee <- map["PaymentMethodAdditionalFee"]
        RedeemedRewardPoints <- map["RedeemedRewardPoints"]
        WillEarnRewardPoints <- map["WillEarnRewardPoints"]
    }


}


