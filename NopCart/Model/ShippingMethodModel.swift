//
//  ShippingMethodModel.swift
//  NopCart
//
//  Created by BS-125 on 8/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper
class ShippingMethodModel: Mappable {
    
    var NotifyCustomerAboutShippingFromMultipleLocations: Bool = false
    
    var SuccessMessage: String?
    
    var ShippingMethods: [Shippingmethods]?
    
    var Warnings: [String]?
    
    var StatusCode: Int = 0
    
    var ErrorList: [String]?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        NotifyCustomerAboutShippingFromMultipleLocations <- map["NotifyCustomerAboutShippingFromMultipleLocations"]
        SuccessMessage <- map["SuccessMessage"]
        ShippingMethods <- map["ShippingMethods"]
        Warnings <- map["Warnings"]
        StatusCode <- map["StatusCode"]
        StatusCode <- map["StatusCode"]
        ErrorList <- map["ErrorList"]
    }
    
}
class Shippingmethods: Mappable {
    
    var Selected: Bool = false
    
    var ShippingRateComputationMethodSystemName: String?
    
    var ShippingOption: Shippingoption?
    
    var Fee: String?
    
    var CustomProperties: Customproperties?
    
    var Name: String?
    
    var Description: String?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Selected <- map["Selected"]
        ShippingRateComputationMethodSystemName <- map["ShippingRateComputationMethodSystemName"]
        ShippingOption <- map["ShippingOption"]
        Fee <- map["Fee"]
        CustomProperties <- map["CustomProperties"]
        Name <- map["Name"]
        Description <- map["Description"]
    }
}

class Shippingoption: Mappable {
    
    var Description: String?
    
    var Name: String?
    
    var Rate: Int = 0
    
    var ShippingRateComputationMethodSystemName: String?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Description <- map["Description"]
        Name <- map["Name"]
        Rate <- map["Rate"]
        ShippingRateComputationMethodSystemName <- map["ShippingRateComputationMethodSystemName"]
    }
    
    
}
