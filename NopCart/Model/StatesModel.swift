//
//  StatesModel.swift
//  NopCart
//
//  Created by BS-125 on 8/16/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper
class StatesModel: Mappable {
    

    var ErrorList: [String]?

    var SuccessMessage: String?

    var StatusCode: Int = 0

    var data: [Data]?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ErrorList <- map["ErrorList"]
        SuccessMessage <- map["SuccessMessage"]
        StatusCode <- map["StatusCode"]
        data <- map["Data"]
    }
    
}
class Data: Mappable {

    var id: Int = 0

    var name: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
    }

}

