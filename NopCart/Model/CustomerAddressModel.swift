//
//  CustomerAddressModel.swift
//  NopCommerce
//
//  Created by BS-125 on 8/23/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper
class CustomerAddressModel: Mappable {
    
    var SuccessMessage: String?

    var existingAddresses: [Existingaddresses]?

    var StatusCode: Int = 0

    var ErrorList: [String]?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        
        SuccessMessage   <- map["SuccessMessage"]
        
        existingAddresses <- map["ExistingAddresses"]
        
        StatusCode      <- map["StatusCode"]
        
        ErrorList <- map["ErrorList"]
    }
    
}


