//
//  UserModel.swift
//  KikSha
//
//  Created by BS85 on 4/20/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper

class UserModel: Mappable {

    var email: String?
    var middlename: String?
    var lastname: String?
    var entity_id: String?
    var firstname: String?
    
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        
        email <- map["email"]
        middlename <- map["middlename"]
        lastname <- map["lastname"]
        entity_id <- map["entity_id"]
        firstname <- map["firstname"]
    }
    
}

/*
{
    "entity_id": "42522",
    "email": "t@t.com",
    "firstname": "t",
    "middlename": null,
    "lastname": "t"
}
*/