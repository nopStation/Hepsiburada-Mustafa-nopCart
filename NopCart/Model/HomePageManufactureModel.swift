//
//  HomePageManufactureModel.swift
//  NopCart
//
//  Created by BS-125 on 6/14/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper
class HomePageManufactureModel: Mappable {
    
    
    var DefaultPictureModel: Defaultpicture_model?
    var Name: String?
    var Id: Int = 0
    required init?(_ map: Map) {
        
    }
    func mapping(map: Map) {
       
        DefaultPictureModel <- map["DefaultPictureModel"]
        Name <- map["Name"]
        Id <- map["Id"]
        
    }
    

}
class Defaultpicture_model: Mappable {

    var ImageUrl: String?

    var BigImageUrl: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ImageUrl  <- map["ImageUrl"]
        BigImageUrl  <- map["BigImageUrl"]
        
        
    }

}

