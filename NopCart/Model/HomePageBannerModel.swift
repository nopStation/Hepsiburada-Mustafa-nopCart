//
//  HomePageBannerModel.swift
//  NopCart
//
//  Created by BS-125 on 6/13/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper

class HomePageBannerModel: Mappable {
    
    var ImageUrl: String?
    var ProdOrCatId: String?
    var Text: String?
    var Link: String?
    var IsProduct: Int = 0
    
    required init?(_ map: Map) {
    
    }

   func mapping(map: Map) {
        ImageUrl <- map["ImageUrl"]
        ProdOrCatId <- map["ProdOrCatId"]
        Text <- map["Text"]
        Link <- map["Link"]
        IsProduct <- map["IsProduct"]
    }
}
