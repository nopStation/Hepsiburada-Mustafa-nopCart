//
//  CategoryModel.swift
//  NopCart
//
//  Created by BS-125 on 6/14/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit

import ObjectMapper

class CategoryModel: Mappable {
    
    var ParentCategoryId: Int = 0

    var Extension: String?

    var Name: String?

    var DisplayOrder: Int = 0

    var IconPath: String?

    var Id: Int = 0
    
    var subCategories       :  [CategoryModel] = []
    
    var collapsed           :  Bool = false
    
    var isLowestItem        :  Bool = false
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ParentCategoryId <- map["ParentCategoryId"]
        Extension <- map["Extension"]
        Name <- map["Name"]
        DisplayOrder <- map["DisplayOrder"]
        Name <- map["Name"]
        IconPath <- map["IconPath"]
        Id <- map["Id"]
        
        subCategories <- map["subCategories"]
        collapsed <- map["collapsed"]
        isLowestItem <- map["isLowestItem"]



    }
    

}
