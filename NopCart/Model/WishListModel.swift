//
//  WishListModel.swift
//  NopCart
//
//  Created by BS-125 on 8/30/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper

class WishListModel: Mappable {
    
    var SuccessMessage: String?

    var ShowSku: Bool = false

    var StatusCode: Int = 0

    var DisplayAddToCart: Bool = false

    var CustomerFullname: String?

    var CustomerGuid: String?

    var EmailWishlistEnabled: Bool = false

    var Count: Int = 0

    var Warnings: [String]?

    var DisplayTaxShippingInfo: Bool = false

    var IsEditable: Bool = false

    var ShowProductImages: Bool = false

    var ErrorList: [String]?

    var items: [Items]?
    
    
    required init?(_ map: Map) {
        
    
    }
    
    func mapping(map: Map) {
        
        SuccessMessage <- map["SuccessMessage"]
        ShowSku <- map["ShowSku"]
        StatusCode <- map["StatusCode"]
        DisplayAddToCart <- map["DisplayAddToCart"]
        CustomerFullname <- map["CustomerFullname"]
        CustomerGuid <- map["CustomerGuid"]
        EmailWishlistEnabled <- map["EmailWishlistEnabled"]
        Count <- map["Count"]
        Warnings <- map["Warnings"]
        DisplayTaxShippingInfo <- map["DisplayTaxShippingInfo"]
        IsEditable <- map["IsEditable"]
        ShowProductImages <- map["ShowProductImages"]
        ErrorList <- map["ErrorList"]
        items <- map["Items"]
    }
}
/*
class Items: NSObject {

    var Sku: String?

    var UnitPrice: String?

    var RentalInfo: String?

    var Id: Int = 0

    var ProductId: Int = 0

    var Picture: Picture?

    var ProductName: String?

    var ProductSeName: String?

    var SubTotal: String?

    var AttributeInfo: String?

    var AllowedQuantities: [String]?

    var Warnings: [String]?

    var Discount: String?

    var Quantity: Int = 0

    var RecurringInfo: String?

    var CustomProperties: Customproperties?

}

class Picture: NSObject {

    var ImageUrl: String?

    var CustomProperties: Customproperties?

    var Title: String?

    var FullSizeImageUrl: String?

    var AlternateText: String?

}

class Customproperties: NSObject {

}

class Customproperties: NSObject {

}*/

