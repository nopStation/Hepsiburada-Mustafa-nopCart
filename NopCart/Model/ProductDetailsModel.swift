//
//  ProductDetailsModel.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper
class ProductDetailsModel: Mappable {


    var ProductManufacturers: [String]?

    var breadcrumb: Breadcrumb?

    var PictureModels: [Picturemodels]?

    var Name: String?

    var Url: String?

    var IsRental: Bool = false

    var VendorModel: Vendormodel?

    var GiftCard: Giftcard?

    var DisplayBackInStockSubscription: Bool = false

    var CompareProductsEnabled: Bool = false

    var ProductTemplateViewPath: String?

    var ProductAttributes: [Productattributes]?

    var ManufacturerPartNumber: String?

    var CustomProperties: Customproperties?

    var NextProduct: Int = 0

    var FreeShippingNotificationEnabled: Bool = false

    var HasSampleDownload: Bool = false

    var IsShipEnabled: Bool = false

    var ProductSpecifications: [String]?

    var FullDescription: String?

    var AssociatedProducts: [String]?

    var DefaultPictureModel: Defaultpicturemodel?

    var ShortDescription: String?

    var IsFreeShipping: Bool = false

    var RentalStartDate: String?

    var DefaultPictureZoomEnabled: Bool = false

    var ShowManufacturerPartNumber: Bool = false

    var AddToCart: Addtocart?

    var ProductReviewOverview: Reviewoverviewmodel?

    var RentalEndDate: String?

    var EmailAFriendEnabled: Bool = false

    var ProductPrice: Productprice?

    var ShowVendor: Bool = false

    var PreviousProduct: Int = 0

    var DeliveryDate: String?

    var TierPrices: [String]?

    var StockAvailability: String?

    var Id: Int = 0

    var ProductTags: [Producttags]?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ProductManufacturers <- map["ProductManufacturers"]
        breadcrumb <- map["Breadcrumb"]
        PictureModels <- map["PictureModels"]
        Name <- map["Name"]
        Url <- map["Url"]
        IsRental <- map["IsRental"]
        VendorModel <- map["VendorModel"]
        GiftCard <- map["GiftCard"]
        DisplayBackInStockSubscription <- map["DisplayBackInStockSubscription"]
        CompareProductsEnabled <- map["CompareProductsEnabled"]
        ProductTemplateViewPath <- map["ProductTemplateViewPath"]
        ProductAttributes <- map["ProductAttributes"]
        ManufacturerPartNumber <- map["ManufacturerPartNumber"]
        CustomProperties <- map["CustomProperties"]
        NextProduct <- map["NextProduct"]
        FreeShippingNotificationEnabled <- map["FreeShippingNotificationEnabled"]
        HasSampleDownload <- map["HasSampleDownload"]
        IsShipEnabled <- map["IsShipEnabled"]
        ProductSpecifications <- map["ProductSpecifications"]
        FullDescription <- map["FullDescription"]
        AssociatedProducts <- map["AssociatedProducts"]
        DefaultPictureModel <- map["DefaultPictureModel"]
        ShortDescription <- map["ShortDescription"]
        IsFreeShipping <- map["IsFreeShipping"]
        RentalStartDate <- map["RentalStartDate"]
        DefaultPictureZoomEnabled <- map["DefaultPictureZoomEnabled"]
        ShowManufacturerPartNumber <- map["ShowManufacturerPartNumber"]
        AddToCart <- map["AddToCart"]
        ProductReviewOverview <- map["ProductReviewOverview"]
        
        RentalEndDate <- map["RentalEndDate"]
        EmailAFriendEnabled <- map["EmailAFriendEnabled"]
        ProductPrice <- map["ProductPrice"]
        ShowVendor <- map["ShowVendor"]
        PreviousProduct <- map["PreviousProduct"]
        DeliveryDate <- map["DeliveryDate"]
        
        TierPrices <- map["TierPrices"]
        StockAvailability <- map["StockAvailability"]
        Id <- map["Id"]
        ProductTags <- map["ProductTags"]


        
    }
    
    

}

class Breadcrumb: Mappable {

    var ProductId: Int = 0

    var CustomProperties: Customproperties?

    var ProductSeName: String?

    var Enabled: Bool = false

    var CategoryBreadcrumb: [Categorybreadcrumb]?

    var ProductName: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ProductId <- map["ProductId"]
        CustomProperties <- map["CustomProperties"]
        ProductSeName <- map["ProductSeName"]
        Enabled <- map["Enabled"]
        CategoryBreadcrumb <- map["CategoryBreadcrumb"]
        ProductName <- map["ProductName"]
    }

}


class Categorybreadcrumb: Mappable {

    var SeName: String?

    var NumberOfProducts: String?

    var Id: Int = 0

    var IncludeInTopMenu: Bool = false

    var CustomProperties: Customproperties?

    var SubCategories: [String]?

    var Name: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        SeName <- map["SeName"]
        NumberOfProducts <- map["NumberOfProducts"]
        Id <- map["Id"]
        IncludeInTopMenu <- map["IncludeInTopMenu"]
        CustomProperties <- map["CustomProperties"]
        SubCategories <- map["SubCategories"]
        Name <- map["Name"]
    }

}


class Addtocart: Mappable {

    var CustomerEnteredPrice: Int = 0

    var ProductId: Int = 0

    var DisableWishlistButton: Bool = false

    var AvailableForPreOrder: Bool = false

    var AllowedQuantities: [String]?

    var IsRental: Bool = false

    var DisableBuyButton: Bool = false

    var EnteredQuantity: Int = 0

    var CustomerEntersPrice: Bool = false

    var UpdatedShoppingCartItemId: Int = 0

    var CustomerEnteredPriceRange: String?

    var PreOrderAvailabilityStartDateTimeUtc: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        CustomerEnteredPrice <- map["CustomerEnteredPrice"]
        ProductId <- map["ProductId"]
        DisableWishlistButton <- map["DisableWishlistButton"]
        AvailableForPreOrder <- map["AvailableForPreOrder"]
        AllowedQuantities <- map["AllowedQuantities"]
        IsRental <- map["IsRental"]
        DisableBuyButton <- map["DisableBuyButton"]
        
        EnteredQuantity <- map["EnteredQuantity"]
        CustomerEntersPrice <- map["CustomerEntersPrice"]
        UpdatedShoppingCartItemId <- map["UpdatedShoppingCartItemId"]
        CustomerEnteredPriceRange <- map["CustomerEnteredPriceRange"]
        PreOrderAvailabilityStartDateTimeUtc <- map["PreOrderAvailabilityStartDateTimeUtc"]
    }
}

class Vendormodel: Mappable {

    var CustomProperties: Customproperties?

    var Name: String?

    var SeName: String?

    var Id: Int = 0

    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        CustomProperties <- map["CustomProperties"]
        Name <- map["Name"]
        SeName <- map["SeName"]
        Id <- map["Id"]
    }
}



class Giftcard: Mappable {

    var CustomProperties: Customproperties?

    var RecipientName: String?

    var SenderEmail: String?

    var IsGiftCard: Bool = false

    var GiftCardType: Int = 0

    var RecipientEmail: String?

    var SenderName: String?

    var Message: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        CustomProperties <- map["CustomProperties"]
        RecipientName <- map["RecipientName"]
        SenderEmail <- map["SenderEmail"]
        IsGiftCard <- map["IsGiftCard"]
        GiftCardType <- map["GiftCardType"]
        RecipientEmail <- map["RecipientEmail"]
        SenderName <- map["SenderName"]
        Message <- map["Message"]
    }

}

class Picturemodels: Mappable {

    var ImageUrl: String?

    var BigImageUrl: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ImageUrl <- map["ImageUrl"]
        BigImageUrl <- map["BigImageUrl"]

    }

}
class Picturemodel: Mappable {
    
    var ImageUrl: String?
    
    var BigImageUrl: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ImageUrl <- map["ImageUrl"]
        BigImageUrl <- map["BigImageUrl"]
        
    }
    
}

class Productattributes: Mappable {

    var Description: String?

    var ProductAttributeId: Int = 0

    var Name: String?

    var SelectedMonth: String?

    var values: [Values]?

    var ProductId: Int = 0

    var Id: Int = 0

    var SelectedDay: String?

    var SelectedYear: String?

    var IsRequired: Bool = false

    var AllowedFileExtensions: [String]?

    var DefaultValue: String?

    var AttributeControlType: Int = 0

    var TextPrompt: String?

    var CustomProperties: Customproperties?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Description <- map["Description"]
        ProductAttributeId <- map["ProductAttributeId"]
        Name <- map["Name"]
        SelectedMonth <- map["SelectedMonth"]
        values <- map["Values"]
        ProductId <- map["ProductId"]
        Id <- map["Id"]
        SelectedDay <- map["SelectedDay"]
        
        SelectedYear <- map["SelectedYear"]
        IsRequired <- map["IsRequired"]
        AllowedFileExtensions <- map["AllowedFileExtensions"]
        DefaultValue <- map["DefaultValue"]
        AttributeControlType <- map["AttributeControlType"]
        TextPrompt <- map["TextPrompt"]
        CustomProperties <- map["CustomProperties"]

        
    }


}


class Values: Mappable {

    var PictureModel: Picturemodel?

    var PriceAdjustmentValue: Int = 0

    var Id: Int = 0

    var CustomProperties: Customproperties?

    var Name: String?

    var PriceAdjustment: String?

    var IsPreSelected: Bool = false

    var ColorSquaresRgb: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        PictureModel <- map["PictureModel"]
        PriceAdjustmentValue <- map["PriceAdjustmentValue"]
        Id <- map["Id"]
        CustomProperties <- map["CustomProperties"]
        Name <- map["Name"]
        PriceAdjustment <- map["PriceAdjustment"]
        IsPreSelected <- map["IsPreSelected"]
        ColorSquaresRgb <- map["ColorSquaresRgb"]

    }

}




class Producttags: Mappable {

    var CustomProperties: Customproperties?

    var Name: String?

    var SeName: String?

    var ProductCount: Int = 0

    var Id: Int = 0
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        CustomProperties <- map["CustomProperties"]
        Name <- map["Name"]
        SeName <- map["SeName"]
        ProductCount <- map["ProductCount"]
        Id <- map["Id"]
    }


}


