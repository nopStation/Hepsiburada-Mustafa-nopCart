//
//  OrderDetailsModel.swift
//  NopCart
//
//  Created by BS-125 on 9/2/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper
class OrderDetailsModel: Mappable {
    
    var ShippingStatus: String?

    var CheckoutAttributeInfo: String?

    var ShippingMethod: String?

    var ErrorList: [String]?

    var PaymentMethodStatus: String?

    var GiftCards: [String]?

    var DisplayTaxRates: Bool = false

    var CustomValues: Customvalues?

    var DisplayTaxShippingInfo: Bool = false

    var OrderSubtotal: String?

    var IsShippable: Bool = false

    var OrderTotal: String?

    var PricesIncludeTax: Bool = false

    var IsReOrderAllowed: Bool = false

    var ShowSku: Bool = false

    var TaxRates: [Taxrates]?

    var SuccessMessage: String?

    var OrderNotes: [String]?

    var VatNumber: String?

    var OrderShipping: String?

    var Tax: String?

    var RedeemedRewardPoints: Int = 0

    var PdfInvoiceDisabled: Bool = false

    var PaymentMethod: String?

    var PickUpInStore: Bool = false

    var ShippingAddress: Shippingaddress?

    var IsReturnRequestAllowed: Bool = false

    var DisplayTax: Bool = false

    var CanRePostProcessPayment: Bool = false

    var OrderStatus: String?

    var CreatedOn: String?

    var items: [Items]?

    var OrderSubTotalDiscount: String?

    var RedeemedRewardPointsAmount: String?

    var OrderTotalDiscount: String?

    var PrintMode: Bool = false

    var PaymentMethodAdditionalFee: String?

    var BillingAddress: Billingaddress?

    var StatusCode: Int = 0

    var Id: Int = 0

    var Shipments: [String]?
    
    
    
    required init?(_ map: Map) {
        
        
    }
    
    func mapping(map: Map) {
        
        ShippingStatus <- map["ShippingStatus"]
        CheckoutAttributeInfo <- map["CheckoutAttributeInfo"]
        ShippingMethod <- map["ShippingMethod"]
        ErrorList <- map["ErrorList"]
        PaymentMethodStatus <- map["PaymentMethodStatus"]
        GiftCards <- map["GiftCards"]
        DisplayTaxRates <- map["DisplayTaxRates"]
        CustomValues <- map["CustomValues"]
        DisplayTaxShippingInfo <- map["DisplayTaxShippingInfo"]
        OrderSubtotal <- map["OrderSubtotal"]
        IsShippable <- map["IsShippable"]
        OrderTotal <- map["OrderTotal"]
        PricesIncludeTax <- map["PricesIncludeTax"]
        IsReOrderAllowed <- map["IsReOrderAllowed"]
        ShowSku <- map["ShowSku"]
        TaxRates <- map["TaxRates"]
        SuccessMessage <- map["SuccessMessage"]
        OrderNotes <- map["OrderNotes"]
        VatNumber <- map["VatNumber"]
        OrderShipping <- map["OrderShipping"]
        Tax <- map["Tax"]
        RedeemedRewardPoints <- map["RedeemedRewardPoints"]
        PdfInvoiceDisabled <- map["PdfInvoiceDisabled"]
        PaymentMethod <- map["PaymentMethod"]
        PickUpInStore <- map["PickUpInStore"]
        ShippingAddress <- map["ShippingAddress"]
        IsReturnRequestAllowed <- map["IsReturnRequestAllowed"]
        DisplayTax <- map["DisplayTax"]
        CanRePostProcessPayment <- map["CanRePostProcessPayment"]
        OrderStatus <- map["OrderStatus"]
        CreatedOn <- map["CreatedOn"]
        items <- map["Items"]
        OrderSubTotalDiscount <- map["OrderSubTotalDiscount"]
        RedeemedRewardPointsAmount <- map["RedeemedRewardPointsAmount"]
        OrderTotalDiscount <- map["OrderTotalDiscount"]
        PrintMode <- map["PrintMode"]
        PaymentMethodAdditionalFee <- map["PaymentMethodAdditionalFee"]
        BillingAddress <- map["BillingAddress"]
        StatusCode <- map["StatusCode"]
        Id <- map["Id"]
        Shipments <- map["Shipments"]
    }


}




//class Taxrates: NSObject {
//
//    var Rate: String?
//
//    var Value: String?
//
//    var CustomProperties: Customproperties?
//
//}

