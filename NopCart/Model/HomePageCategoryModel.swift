//
//  HomePageCategoryModel.swift
//  NopCart
//
//  Created by BS-125 on 6/13/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper

class HomePageCategoryModel: Mappable {

    var product: [Product]?

    var category: Category?

    var subCategory: [Subcategory]?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        product <- map["Product"]
        category <- map["Category"]
        subCategory <- map["SubCategory"]
    }
    
}

class Category: Mappable {

    var DefaultPictureModel: Defaultpicturemodel?

    var Name: String?

    var Id: Int = 0
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        DefaultPictureModel <- map["DefaultPictureModel"]
        Name <- map["Name"]
        Id <- map["Id"]
    }

}

class Defaultpicturemodel: Mappable {

    var ImageUrl: String?

    var BigImageUrl: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        ImageUrl <- map["ImageUrl"]
        BigImageUrl <- map["BigImageUrl"]
    }


}

class Product: Mappable {

    var ReviewOverviewModel: Reviewoverviewmodel?

    var Id: Int = 0

    var CustomProperties: Customproperties?

    var DefaultPictureModel: Defaultpicturemodel?

    var Name: String?

    var ShortDescription: String?

    var ProductPrice: Productprice?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        ReviewOverviewModel <- map["ReviewOverviewModel"]
        CustomProperties <- map["CustomProperties"]
        Id <- map["Id"]
        DefaultPictureModel <- map["DefaultPictureModel"]
        Name <- map["Name"]
        ShortDescription <- map["ShortDescription"]
        ProductPrice <- map["ProductPrice"]
    }

}

class Customproperties: Mappable {

    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
}


class Reviewoverviewmodel: Mappable {

    var ProductId: Int = 0

    var TotalReviews: Int = 0

    var RatingSum: Int = 0

    var AllowCustomerReviews: Bool = false
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ProductId <- map["ProductId"]
        TotalReviews <- map["TotalReviews"]
        RatingSum <- map["RatingSum"]
        AllowCustomerReviews <- map["AllowCustomerReviews"]
    }

}

//class Productprice: Mappable {
//
//    var OldPrice: String?
//    var Price: String?
//
//    required init?(_ map: Map) {
//        
//    }
//    
//    func mapping(map: Map) {
//        
//        OldPrice <- map["OldPrice"]
//        Price <- map["Price"]
//    }
//
//}


class Productprice: Mappable {
    
    var RentalPrice: String?
    
    var PriceWithDiscountValue: Int = 0
    
    var PriceValue: Int = 0
    
    var OldPrice: String?
    
    var PriceWithDiscount: String?
    
    var HidePrices: Bool = false
    
    var ProductId: Int = 0
    
    var CurrencyCode: String?
    
    var BasePricePAngV: String?
    
    var IsRental: Bool = false
    
    var DisplayTaxShippingInfo: Bool = false
    
    var Price: String?
    
    var CustomerEntersPrice: Bool = false
    
    var CallForPrice: Bool = false
    
    var CustomProperties: Customproperties?
    
    required init?(_ map: Map) {
    
    }
    
    func mapping(map: Map) {
            
        RentalPrice <- map["RentalPrice"]
        PriceWithDiscountValue <- map["PriceWithDiscountValue"]
        PriceValue <- map["PriceValue"]
        OldPrice <- map["OldPrice"]
        PriceWithDiscount <- map["PriceWithDiscount"]
        HidePrices <- map["HidePrices"]
        ProductId <- map["ProductId"]
        CurrencyCode <- map["CurrencyCode"]
        BasePricePAngV <- map["BasePricePAngV"]
        IsRental <- map["IsRental"]
        DisplayTaxShippingInfo <- map["DisplayTaxShippingInfo"]
        Price <- map["Price"]
        CustomerEntersPrice <- map["CustomerEntersPrice"]
        CallForPrice <- map["CallForPrice"]
        CustomProperties <- map["CustomProperties"]
        
    }
    
}
class Subcategory: Mappable {

    var IconPath: String?

    var Id: Int = 0

    var Name: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        IconPath <- map["IconPath"]
        Id <- map["Id"]
        Name <- map["Name"]

    }

}

