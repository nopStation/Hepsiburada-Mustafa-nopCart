//
//  BillingAndShippingAddressModel.swift
//  NopCart
//
//  Created by BS-125 on 8/16/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper

class BillingAndShippingAddressModel: Mappable {
    
    
    var NewAddressPreselected: Bool = false
    var SuccessMessage: String?
    var NewAddress: Newaddress?
    var ExistingAddresses: [Existingaddresses]?
    var StatusCode: Int = 0
    var ErrorList: [String]?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        NewAddressPreselected <- map["NewAddressPreselected"]
        SuccessMessage <- map["SuccessMessage"]
        NewAddress <- map["NewAddress"]
        ExistingAddresses <- map["ExistingAddresses"]
        StatusCode <- map["StatusCode"]
        ErrorList <- map["ErrorList"]
        
    }


}
class Newaddress: Mappable {

    var Company: String?

    var FaxRequired: Bool = false

    var FaxEnabled: Bool = false

    var AvailableCountries: [Availablecountries]?

    var Address1: String?

    var City: String?

    var Email: String?

    var StreetAddress2Enabled: Bool = false

    var CountryEnabled: Bool = false

    var LastName: String?

    var CountryId: String?

    var CountryName: String?

    var CityRequired: Bool = false

    var FaxNumber: String?

    var CompanyRequired: Bool = false

    var AvailableStates: [Availablestates]?

    var StateProvinceName: String?

    var FormattedCustomAddressAttributes: String?

    var CustomProperties: Customproperties?

    var ZipPostalCode: String?

    var StateProvinceId: String?

    var CompanyEnabled: Bool = false

    var CustomAddressAttributes: [String]?

    var PhoneRequired: Bool = false

    var StreetAddressEnabled: Bool = false

    var CityEnabled: Bool = false

    var StateProvinceEnabled: Bool = false

    var Address2: String?

    var ZipPostalCodeEnabled: Bool = false

    var PhoneNumber: String?

    var StreetAddress2Required: Bool = false

    var FirstName: String?

    var PhoneEnabled: Bool = false

    var ZipPostalCodeRequired: Bool = false

    var Id: Int = 0

    var StreetAddressRequired: Bool = false

    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Company <- map["Company"]
        FaxRequired <- map["FaxRequired"]
        FaxEnabled <- map["FaxEnabled"]
        AvailableCountries <- map["AvailableCountries"]
        Address1 <- map["Address1"]
        City <- map["City"]
        
        
        Email <- map["Email"]
        StreetAddress2Enabled <- map["StreetAddress2Enabled"]
        CountryEnabled <- map["CountryEnabled"]
        LastName <- map["LastName"]
        CountryId <- map["CountryId"]
        CountryName <- map["CountryName"]
        
        CityRequired <- map["CityRequired"]
        FaxNumber <- map["FaxNumber"]
        CompanyRequired <- map["CompanyRequired"]
        AvailableStates <- map["AvailableStates"]
        StateProvinceName <- map["StateProvinceName"]
        FormattedCustomAddressAttributes <- map["FormattedCustomAddressAttributes"]
        
        CustomProperties <- map["CustomProperties"]
        ZipPostalCode <- map["ZipPostalCode"]
        StateProvinceId <- map["StateProvinceId"]
        CompanyEnabled <- map["CompanyEnabled"]
        CustomAddressAttributes <- map["CustomAddressAttributes"]
        PhoneRequired <- map["PhoneRequired"]
        
        StreetAddressEnabled <- map["StreetAddressEnabled"]
        CityEnabled <- map["CityEnabled"]
        StateProvinceEnabled <- map["StateProvinceEnabled"]
        Address2 <- map["Address2"]
        ZipPostalCodeEnabled <- map["ZipPostalCodeEnabled"]
        PhoneNumber <- map["PhoneNumber"]
        
        StreetAddress2Required <- map["StreetAddress2Required"]
        FirstName <- map["FirstName"]
        PhoneEnabled <- map["PhoneEnabled"]
        ZipPostalCodeRequired <- map["ZipPostalCodeRequired"]
        Id <- map["Id"]
        StreetAddressRequired <- map["StreetAddressRequired"]
        
    }
}



class Availablestates: Mappable {

    var Selected: Bool = false

    var Group: String?

    var Disabled: Bool = false

    var Text: String?

    var Value: String?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Selected <- map["Selected"]
        Group <- map["Group"]
        Disabled <- map["Disabled"]
        Text <- map["Text"]
        Value <- map["Value"]
    }


}

class Availablecountries: Mappable {

    var Selected: Bool = false

    var Group: String?

    var Disabled: Bool = false

    var Text: String?

    var Value: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Selected <- map["Selected"]
        Group <- map["Group"]
        Disabled <- map["Disabled"]
        Text <- map["Text"]
        Value <- map["Value"]
    }

}

class Existingaddresses: Mappable {

    var Company: String?

    var FaxRequired: Bool = false

    var FaxEnabled: Bool = false

    var AvailableCountries: [String]?

    var Address1: String?

    var City: String?

    var Email: String?

    var StreetAddress2Enabled: Bool = false

    var CountryEnabled: Bool = false

    var LastName: String?

    var CountryId: Int = 0

    var CountryName: String?

    var CityRequired: Bool = false

    var FaxNumber: String?

    var CompanyRequired: Bool = false

    var AvailableStates: [String]?

    var StateProvinceName: String?

    var FormattedCustomAddressAttributes: String?

    var CustomProperties: Customproperties?

    var ZipPostalCode: String?

    var StateProvinceId: Int = 0

    var CompanyEnabled: Bool = false

    var CustomAddressAttributes: [String]?

    var PhoneRequired: Bool = false

    var StreetAddressEnabled: Bool = false

    var CityEnabled: Bool = false

    var StateProvinceEnabled: Bool = false

    var Address2: String?

    var ZipPostalCodeEnabled: Bool = false

    var PhoneNumber: String?

    var StreetAddress2Required: Bool = false

    var FirstName: String?

    var PhoneEnabled: Bool = false

    var ZipPostalCodeRequired: Bool = false

    var Id: Int = 0

    var StreetAddressRequired: Bool = false
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Company <- map["Company"]
        FaxRequired <- map["FaxRequired"]
        FaxEnabled <- map["FaxEnabled"]
        AvailableCountries <- map["AvailableCountries"]
        Address1 <- map["Address1"]
        City <- map["City"]
        
        
        Email <- map["Email"]
        StreetAddress2Enabled <- map["StreetAddress2Enabled"]
        CountryEnabled <- map["CountryEnabled"]
        LastName <- map["LastName"]
        CountryId <- map["CountryId"]
        CountryName <- map["CountryName"]
        
        CityRequired <- map["CityRequired"]
        FaxNumber <- map["FaxNumber"]
        CompanyRequired <- map["CompanyRequired"]
        AvailableStates <- map["AvailableStates"]
        StateProvinceName <- map["StateProvinceName"]
        FormattedCustomAddressAttributes <- map["FormattedCustomAddressAttributes"]
        
        CustomProperties <- map["CustomProperties"]
        ZipPostalCode <- map["ZipPostalCode"]
        StateProvinceId <- map["StateProvinceId"]
        CompanyEnabled <- map["CompanyEnabled"]
        CustomAddressAttributes <- map["CustomAddressAttributes"]
        PhoneRequired <- map["PhoneRequired"]
        
        StreetAddressEnabled <- map["StreetAddressEnabled"]
        CityEnabled <- map["CityEnabled"]
        StateProvinceEnabled <- map["StateProvinceEnabled"]
        Address2 <- map["Address2"]
        ZipPostalCodeEnabled <- map["ZipPostalCodeEnabled"]
        PhoneNumber <- map["PhoneNumber"]
        
        StreetAddress2Required <- map["StreetAddress2Required"]
        FirstName <- map["FirstName"]
        PhoneEnabled <- map["PhoneEnabled"]
        ZipPostalCodeRequired <- map["ZipPostalCodeRequired"]
        Id <- map["Id"]
        StreetAddressRequired <- map["StreetAddressRequired"]
    }


}


