//
//  ShoppingCartModel.swift
//  NopCart
//
//  Created by BS-125 on 8/10/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper

class ShoppingCartModel: Mappable {

    var OrderTotalResponseModel: Ordertotalresponsemodel?

    var ShowSku: Bool = false

    var StatusCode: Int = 0

    var SuccessMessage: String?

    var TermsOfServiceOnOrderConfirmPage: Bool = false

    var CheckoutAttributeInfo: String?

    var GiftCardBox: Giftcardbox?

    var OnePageCheckoutEnabled: Bool = false

    var OrderReviewData: Orderreviewdata?

    var Count: Int = 0

    var CheckoutAttributes: [Checkoutattributes]?

    var Warnings: [String]?

    var TermsOfServiceOnShoppingCartPage: Bool = false

    var DiscountBox: Discountbox?

    var ShowProductImages: Bool = false

    var ErrorList: [String]?

    var items: [Items]?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        OrderTotalResponseModel <- map["OrderTotalResponseModel"]
        ShowSku <- map["ShowSku"]
        StatusCode <- map["StatusCode"]
        SuccessMessage <- map["SuccessMessage"]
        TermsOfServiceOnOrderConfirmPage <- map["TermsOfServiceOnOrderConfirmPage"]
        CheckoutAttributeInfo <- map["CheckoutAttributeInfo"]
        GiftCardBox <- map["GiftCardBox"]
        
        
        OnePageCheckoutEnabled <- map["OnePageCheckoutEnabled"]
        OrderReviewData <- map["OrderReviewData"]
        Count <- map["Count"]
        CheckoutAttributes <- map["CheckoutAttributes"]
        Warnings <- map["Warnings"]
        TermsOfServiceOnShoppingCartPage <- map["TermsOfServiceOnShoppingCartPage"]
        DiscountBox <- map["DiscountBox"]
        ShowProductImages <- map["ShowProductImages"]
        ErrorList <- map["ErrorList"]
        items <- map["Items"]


        
        
        
    }

    
    
    
}
class Discountbox: Mappable {

    var CustomProperties: Customproperties?

    var Message: String?

    var Display: Bool = false

    var CurrentCode: String?

    var IsApplied: Bool = false

    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        CustomProperties <- map["CustomProperties"]
        Message <- map["Message"]
        Display <- map["Display"]
        CurrentCode <- map["CurrentCode"]
        IsApplied <- map["IsApplied"]

    }
}


class Ordertotalresponsemodel: Mappable {

    var IsEditable: Bool = false

    var Tax: String?

    var DisplayTaxRates: Bool = false

    var SubTotalDiscount: String?

    var SelectedShippingMethod: String?

    var OrderTotal: String?

    var SubTotal: String?

    var SuccessMessage: String?

    var ErrorList: [String]?

    var TaxRates: [Taxrates]?

    var AllowRemovingSubTotalDiscount: Bool = false

    var AllowRemovingOrderTotalDiscount: Bool = false

    var StatusCode: Int = 0

    var Shipping: String?

    var DisplayTax: Bool = false

    var RequiresShipping: Bool = false

    var GiftCards: [String]?

    var RedeemedRewardPointsAmount: String?

    var OrderTotalDiscount: String?

    var PaymentMethodAdditionalFee: String?

    var RedeemedRewardPoints: Int = 0

    var WillEarnRewardPoints: Int = 0
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        IsEditable <- map["IsEditable"]
        Tax <- map["Tax"]
        DisplayTaxRates <- map["DisplayTaxRates"]
        SubTotalDiscount <- map["SubTotalDiscount"]
        SelectedShippingMethod <- map["SelectedShippingMethod"]
        OrderTotal <- map["OrderTotal"]
        SubTotal <- map["SubTotal"]
        
        
        SuccessMessage <- map["SuccessMessage"]
        ErrorList <- map["ErrorList"]
        TaxRates <- map["TaxRates"]
        AllowRemovingSubTotalDiscount <- map["AllowRemovingSubTotalDiscount"]
        AllowRemovingOrderTotalDiscount <- map["AllowRemovingOrderTotalDiscount"]
        StatusCode <- map["StatusCode"]
        Shipping <- map["Shipping"]
        DisplayTax <- map["DisplayTax"]
        RequiresShipping <- map["RequiresShipping"]
        GiftCards <- map["GiftCards"]
        
        
        RedeemedRewardPointsAmount <- map["RedeemedRewardPointsAmount"]
        OrderTotalDiscount <- map["OrderTotalDiscount"]
        PaymentMethodAdditionalFee <- map["PaymentMethodAdditionalFee"]
        RedeemedRewardPoints <- map["RedeemedRewardPoints"]
        WillEarnRewardPoints <- map["WillEarnRewardPoints"]
        
        
        
        
    }


}

class Taxrates: Mappable {

    var Rate: String?

    var Value: String?

    var CustomProperties: Customproperties?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Rate <- map["Rate"]
        Value <- map["Value"]
        CustomProperties <- map["CustomProperties"]
        
    }

}


class Orderreviewdata: Mappable {

    var ShippingMethod: String?

    var CustomProperties: Customproperties?

    var ShippingAddress: Shippingaddress?

    var CustomValues: Customvalues?

    var PaymentMethod: String?

    var SelectedPickUpInStore: Bool = false

    var Display: Bool = false

    var IsShippable: Bool = false

    var BillingAddress: Billingaddress?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ShippingMethod <- map["ShippingMethod"]
        CustomProperties <- map["CustomProperties"]
        ShippingAddress <- map["ShippingAddress"]
        CustomValues <- map["CustomValues"]
        PaymentMethod <- map["PaymentMethod"]
        SelectedPickUpInStore <- map["SelectedPickUpInStore"]
        Display <- map["Display"]
        IsShippable <- map["IsShippable"]
        BillingAddress <- map["BillingAddress"]
    }


}


class Customvalues: Mappable {
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }

}

class Billingaddress: Mappable {

    var Company: String?

    var FaxRequired: Bool = false

    var FaxEnabled: Bool = false

    var AvailableCountries: [String]?

    var Address1: String?

    var City: String?

    var Email: String?

    var StreetAddress2Enabled: Bool = false

    var CountryEnabled: Bool = false

    var LastName: String?

    var CountryId: String?

    var CountryName: String?

    var CityRequired: Bool = false

    var FaxNumber: String?

    var CompanyRequired: Bool = false

    var AvailableStates: [String]?

    var StateProvinceName: String?

    var FormattedCustomAddressAttributes: String?

    var CustomProperties: Customproperties?

    var ZipPostalCode: String?

    var StateProvinceId: String?

    var CompanyEnabled: Bool = false

    var CustomAddressAttributes: [String]?

    var PhoneRequired: Bool = false

    var StreetAddressEnabled: Bool = false

    var CityEnabled: Bool = false

    var StateProvinceEnabled: Bool = false

    var Address2: String?

    var ZipPostalCodeEnabled: Bool = false

    var PhoneNumber: String?

    var StreetAddress2Required: Bool = false

    var FirstName: String?

    var PhoneEnabled: Bool = false

    var ZipPostalCodeRequired: Bool = false

    var Id: Int = 0

    var StreetAddressRequired: Bool = false
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Company <- map["Company"]
        FaxRequired <- map["FaxRequired"]
        FaxEnabled <- map["FaxEnabled"]
        AvailableCountries <- map["AvailableCountries"]
        Address1 <- map["Address1"]
        City <- map["City"]
        
        
        Email <- map["Email"]
        StreetAddress2Enabled <- map["StreetAddress2Enabled"]
        CountryEnabled <- map["CountryEnabled"]
        LastName <- map["LastName"]
        CountryId <- map["CountryId"]
        CountryName <- map["CountryName"]
        
        CityRequired <- map["CityRequired"]
        FaxNumber <- map["FaxNumber"]
        CompanyRequired <- map["CompanyRequired"]
        AvailableStates <- map["AvailableStates"]
        StateProvinceName <- map["StateProvinceName"]
        FormattedCustomAddressAttributes <- map["FormattedCustomAddressAttributes"]
        
        CustomProperties <- map["CustomProperties"]
        ZipPostalCode <- map["ZipPostalCode"]
        StateProvinceId <- map["StateProvinceId"]
        CompanyEnabled <- map["CompanyEnabled"]
        CustomAddressAttributes <- map["CustomAddressAttributes"]
        PhoneRequired <- map["PhoneRequired"]
        
        StreetAddressEnabled <- map["StreetAddressEnabled"]
        CityEnabled <- map["CityEnabled"]
        StateProvinceEnabled <- map["StateProvinceEnabled"]
        Address2 <- map["Address2"]
        ZipPostalCodeEnabled <- map["ZipPostalCodeEnabled"]
        PhoneNumber <- map["PhoneNumber"]
        
        StreetAddress2Required <- map["StreetAddress2Required"]
        FirstName <- map["FirstName"]
        PhoneEnabled <- map["PhoneEnabled"]
        ZipPostalCodeRequired <- map["ZipPostalCodeRequired"]
        Id <- map["Id"]
        StreetAddressRequired <- map["StreetAddressRequired"]
    }


}



class Shippingaddress: Mappable {

    var Company: String?

    var FaxRequired: Bool = false

    var FaxEnabled: Bool = false

    var AvailableCountries: [String]?

    var Address1: String?

    var City: String?

    var Email: String?

    var StreetAddress2Enabled: Bool = false

    var CountryEnabled: Bool = false

    var LastName: String?

    var CountryId: String?

    var CountryName: String?

    var CityRequired: Bool = false

    var FaxNumber: String?

    var CompanyRequired: Bool = false

    var AvailableStates: [String]?

    var StateProvinceName: String?

    var FormattedCustomAddressAttributes: String?

    var CustomProperties: Customproperties?

    var ZipPostalCode: String?

    var StateProvinceId: String?

    var CompanyEnabled: Bool = false

    var CustomAddressAttributes: [String]?

    var PhoneRequired: Bool = false

    var StreetAddressEnabled: Bool = false

    var CityEnabled: Bool = false

    var StateProvinceEnabled: Bool = false

    var Address2: String?

    var ZipPostalCodeEnabled: Bool = false

    var PhoneNumber: String?

    var StreetAddress2Required: Bool = false

    var FirstName: String?

    var PhoneEnabled: Bool = false

    var ZipPostalCodeRequired: Bool = false

    var Id: Int = 0

    var StreetAddressRequired: Bool = false
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Company <- map["Company"]
        FaxRequired <- map["FaxRequired"]
        FaxEnabled <- map["FaxEnabled"]
        AvailableCountries <- map["AvailableCountries"]
        Address1 <- map["Address1"]
        City <- map["City"]
        
        
        Email <- map["Email"]
        StreetAddress2Enabled <- map["StreetAddress2Enabled"]
        CountryEnabled <- map["CountryEnabled"]
        LastName <- map["LastName"]
        CountryId <- map["CountryId"]
        CountryName <- map["CountryName"]
        
        CityRequired <- map["CityRequired"]
        FaxNumber <- map["FaxNumber"]
        CompanyRequired <- map["CompanyRequired"]
        AvailableStates <- map["AvailableStates"]
        StateProvinceName <- map["StateProvinceName"]
        FormattedCustomAddressAttributes <- map["FormattedCustomAddressAttributes"]
        
        CustomProperties <- map["CustomProperties"]
        ZipPostalCode <- map["ZipPostalCode"]
        StateProvinceId <- map["StateProvinceId"]
        CompanyEnabled <- map["CompanyEnabled"]
        CustomAddressAttributes <- map["CustomAddressAttributes"]
        PhoneRequired <- map["PhoneRequired"]
        
        StreetAddressEnabled <- map["StreetAddressEnabled"]
        CityEnabled <- map["CityEnabled"]
        StateProvinceEnabled <- map["StateProvinceEnabled"]
        Address2 <- map["Address2"]
        ZipPostalCodeEnabled <- map["ZipPostalCodeEnabled"]
        PhoneNumber <- map["PhoneNumber"]
        
        StreetAddress2Required <- map["StreetAddress2Required"]
        FirstName <- map["FirstName"]
        PhoneEnabled <- map["PhoneEnabled"]
        ZipPostalCodeRequired <- map["ZipPostalCodeRequired"]
        Id <- map["Id"]
        StreetAddressRequired <- map["StreetAddressRequired"]
    }


}


class Giftcardbox: Mappable {

    var CustomProperties: Customproperties?

    var Message: String?

    var Display: Bool = false

    var IsApplied: Bool = false
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        CustomProperties <- map["CustomProperties"]
        Message <- map["Message"]
        Display <- map["Display"]
        IsApplied <- map["IsApplied"]
    }

}



class Items: Mappable {

    var Sku: String?

    var UnitPrice: String?

    var RentalInfo: String?

    var Id: Int = 0

    var ProductId: Int = 0

    var picture: Picture?

    var ProductName: String?

    var ProductSeName: String?

    var SubTotal: String?

    var AttributeInfo: String?

    var AllowItemEditing: Bool = false

    var AllowedQuantities: [String]?

    var Warnings: [String]?

    var Discount: String?

    var Quantity: Int = 0

    var RecurringInfo: String?

    var CustomProperties: Customproperties?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Sku <- map["Sku"]
        UnitPrice <- map["UnitPrice"]
        RentalInfo <- map["RentalInfo"]
        Id <- map["Id"]
        ProductId <- map["ProductId"]
        picture <- map["Picture"]
        ProductName <- map["ProductName"]
        ProductSeName <- map["ProductSeName"]
        SubTotal <- map["SubTotal"]
        AttributeInfo <- map["AttributeInfo"]
        AllowItemEditing <- map["AllowItemEditing"]
        AllowedQuantities <- map["AllowedQuantities"]
        Warnings <- map["Warnings"]
        Discount <- map["Discount"]
        Quantity <- map["Quantity"]
        RecurringInfo <- map["RecurringInfo"]
        CustomProperties <- map["CustomProperties"]
        
    }


}

class Picture: Mappable {
    
    var ImageUrl: String?
    
    var BigImageUrl: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ImageUrl <- map["ImageUrl"]
        BigImageUrl <- map["BigImageUrl"]

    }

    
}

class Checkoutattributes: Mappable {

    var Name: String?

    var SelectedMonth: String?

    var values: [Values]?

    var Id: Int = 0

    var SelectedDay: String?

    var SelectedYear: String?

    var IsRequired: Bool = false

    var AllowedFileExtensions: [String]?

    var DefaultValue: String?

    var AttributeControlType: Int = 0

    var TextPrompt: String?

    var CustomProperties: Customproperties?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Name <- map["Name"]
        SelectedMonth <- map["SelectedMonth"]
        values <- map["Values"]
        Id <- map["Id"]
        SelectedDay <- map["SelectedDay"]
        SelectedYear <- map["SelectedYear"]
        IsRequired <- map["IsRequired"]
        AllowedFileExtensions <- map["AllowedFileExtensions"]
        DefaultValue <- map["DefaultValue"]
        
        AttributeControlType <- map["AttributeControlType"]
        TextPrompt <- map["TextPrompt"]
        CustomProperties <- map["CustomProperties"]
   
    }


}




