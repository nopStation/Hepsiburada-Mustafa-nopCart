//
//  ProductsInfoModel.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper
class ProductsInfoModel: Mappable {
    
    var NotFilteredItems: [Notfiltereditems]?

    var AvailableSortOptions: [Availablesortoptions]?

    var ErrorList: [String]?

    var PriceRange: Pricerange?

    var StatusCode: Int = 0

    var TotalPages: Int = 0

    var Name: String?

    var SuccessMessage: String?

    var AlreadyFilteredItems: [Notfiltereditems]?

    var products: [Products] = []
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        NotFilteredItems <- map["NotFilteredItems"]
        AvailableSortOptions <- map["AvailableSortOptions"]
        ErrorList <- map["ErrorList"]
        PriceRange <- map["PriceRange"]
        StatusCode <- map["StatusCode"]
        TotalPages <- map["TotalPages"]
        Name <- map["Name"]
        
        SuccessMessage <- map["SuccessMessage"]
        AlreadyFilteredItems <- map["AlreadyFilteredItems"]
        products <- map["Products"]
        
        
        
    }
    

}
class Pricerange: Mappable {

    var From: Double = 0

    var To: Double = 0
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        From <- map["From"]
        To <- map["To"]
        
        
    }

}

class Products: Mappable {

    var ReviewOverviewModel: Reviewoverviewmodel?

    var Id: Int = 0

    var CustomProperties: Customproperties?

    var DefaultPictureModel: Defaultpicturemodel?

    var Name: String?

    var ShortDescription: String?

    var ProductPrice: Productprice?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ReviewOverviewModel <- map["ReviewOverviewModel"]
        Id <- map["Id"]
        CustomProperties <- map["CustomProperties"]
        DefaultPictureModel <- map["DefaultPictureModel"]
        Name <- map["Name"]
        ShortDescription <- map["ShortDescription"]
        ProductPrice <- map["ProductPrice"]
    }

}

class Availablesortoptions: Mappable {

    var Selected: Bool = false

    var Group: String?

    var Disabled: Bool = false

    var Text: String?

    var Value: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Selected <- map["Selected"]
        Group <- map["Group"]
        Disabled <- map["Disabled"]
        Text <- map["Text"]
        Value <- map["Value"]
    }

}

class Notfiltereditems: NSObject {

    var CustomProperties: Customproperties?

    var SpecificationAttributeName: String?

    var FilterId: Int = 0

    var SpecificationAttributeOptionName: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        CustomProperties <- map["CustomProperties"]
        SpecificationAttributeName <- map["SpecificationAttributeName"]
        FilterId <- map["FilterId"]
        SpecificationAttributeOptionName <- map["SpecificationAttributeOptionName"]
    }

}


