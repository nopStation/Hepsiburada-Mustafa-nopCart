//
//  RelatedProductsModel.swift
//  NopCart
//
//  Created by BS-125 on 7/29/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper
class RelatedProductsModel: Mappable {
    
    var ReviewOverviewModel: Reviewoverviewmodel?

    var Id: Int = 0

    var CustomProperties: Customproperties?

    var DefaultPictureModel: Defaultpicturemodel?

    var Name: String?

    var ShortDescription: String?

    var ProductPrice: Productprice?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ReviewOverviewModel <- map["ReviewOverviewModel"]
        Id <- map["Id"]
        CustomProperties <- map["CustomProperties"]
        DefaultPictureModel <- map["DefaultPictureModel"]
        Name <- map["Name"]
        ShortDescription <- map["ShortDescription"]
        ProductPrice <- map["ProductPrice"]
        
    }


}


