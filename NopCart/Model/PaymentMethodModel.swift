//
//  PaymentMethodModel.swift
//  NopCart
//
//  Created by BS-125 on 8/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper
class PaymentMethodModel: Mappable {
    
    
    var DisplayRewardPoints: Bool = false
    
    var RewardPointsBalance: Int = 0
    
    var UseRewardPoints: Bool = false
    
    var ErrorList: [String]?
    
    var StatusCode: Int = 0
    
    var RewardPointsAmount: String?
    
    var SuccessMessage: String?
    
    var PaymentMethods: [Paymentmethods]?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        DisplayRewardPoints <- map["DisplayRewardPoints"]
        RewardPointsBalance <- map["RewardPointsBalance"]
        UseRewardPoints <- map["UseRewardPoints"]
        ErrorList <- map["ErrorList"]
        StatusCode <- map["StatusCode"]
        RewardPointsAmount <- map["RewardPointsAmount"]
        SuccessMessage <- map["SuccessMessage"]
        PaymentMethods <- map["PaymentMethods"]
    }

    
    
}
class Paymentmethods: Mappable {
    
    var CustomProperties: Customproperties?
    
    var PaymentMethodSystemName: String?
    
    var Name: String?
    
    var Fee: String?
    
    var Selected: Bool = false
    
    var LogoUrl: String?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        CustomProperties <- map["CustomProperties"]
        PaymentMethodSystemName <- map["PaymentMethodSystemName"]
        Name <- map["Name"]
        Fee <- map["Fee"]
        Selected <- map["Selected"]
        LogoUrl <- map["LogoUrl"]
    }
}




