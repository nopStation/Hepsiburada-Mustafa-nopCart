//
//  MyOrdersModel.swift
//  NopCart
//
//  Created by BS-125 on 8/31/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper
class MyOrdersModel: Mappable {
    var RecurringOrders: [String]?

    var SuccessMessage: String?

    var CancelRecurringPaymentErrors: [String]?

    var ErrorList: [String]?

    var orders: [Orders]?

    var StatusCode: Int = 0
    
    
    required init?(_ map: Map) {
        
        
    }
    
    func mapping(map: Map) {
        
        SuccessMessage <- map["SuccessMessage"]
        CancelRecurringPaymentErrors <- map["CancelRecurringPaymentErrors"]
        ErrorList <- map["ErrorList"]
        orders <- map["Orders"]
        StatusCode <- map["StatusCode"]
    }
    

}
class Orders: Mappable {

    var IsReturnRequestAllowed: Bool = false

    var CustomProperties: Customproperties?

    var Id: Int = 0

    var PaymentStatus: String?

    var OrderTotal: String?

    var ShippingStatus: String?

    var OrderStatusEnum: Int = 0

    var CreatedOn: String?

    var OrderStatus: String?
    
    
    required init?(_ map: Map) {
        
        
    }
    
    func mapping(map: Map) {
        
        IsReturnRequestAllowed <- map["IsReturnRequestAllowed"]
        CustomProperties <- map["CustomProperties"]
        Id <- map["Id"]
        PaymentStatus <- map["PaymentStatus"]
        OrderTotal <- map["OrderTotal"]
        ShippingStatus <- map["ShippingStatus"]
        OrderStatusEnum <- map["OrderStatusEnum"]
        CreatedOn <- map["CreatedOn"]
        OrderStatus <- map["OrderStatus"]
    }

}

