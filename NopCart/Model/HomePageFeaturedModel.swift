//
//  HomePageFeaturedModel.swift
//  NopCart
//
//  Created by BS-125 on 6/14/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper

class HomePageFeaturedModel: Mappable {
    
    var ReviewOverviewModel: Reviewoverview_model?

    var Id: Int = 0

    var CustomProperties: Custom_properties?

    var DefaultPictureModel: Defaultpicturemodel2?

    var Name: String?

    var ShortDescription: String?

    var ProductPrice: Product_price?
    
    
    required init?(_ map: Map) {
        
    }
    func mapping(map: Map) {
        
        ReviewOverviewModel <- map["ReviewOverviewModel"]
        CustomProperties <- map["CustomProperties"]
        Id <- map["Id"]
        DefaultPictureModel <- map["DefaultPictureModel"]
        Name <- map["Name"]
        ShortDescription <- map["ShortDescription"]
        ProductPrice <- map["ProductPrice"]

    }
    

}

class Defaultpicturemodel2: Mappable {
    
    var ImageUrl: String?
    
    var BigImageUrl: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        ImageUrl <- map["ImageUrl"]
        BigImageUrl <- map["BigImageUrl"]
    }
    
    
}


class Custom_properties: Mappable {
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
}

class Reviewoverview_model: Mappable {
    
    var ProductId: Int = 0
    
    var TotalReviews: Int = 0
    
    var RatingSum: Int = 0
    
    var AllowCustomerReviews: Bool = false
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        ProductId <- map["ProductId"]
        TotalReviews <- map["TotalReviews"]
        RatingSum <- map["RatingSum"]
        AllowCustomerReviews <- map["AllowCustomerReviews"]
    }
    
}

class Product_price: Mappable {
    
    var OldPrice: String?
    
    var Price: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        OldPrice <- map["OldPrice"]
        Price <- map["Price"]
    }
    
}

