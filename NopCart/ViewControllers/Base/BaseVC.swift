//
//  BaseVC.swift
//  NopCart
//
//  Created by BS85 on 5/3/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit

class BaseVC: UIViewController, UISearchBarDelegate , MenuDropDownVCDelegate ,  CartButtonViewDelegate{

    //MARK: - Properties
    var customNavBarView: CustomNavBarView!
    let viewSize = UIScreen.mainScreen().bounds
    var menuDropDownVC = MenuDropDownVC ()
    //var cartBtnView = CartButtonView ()
    
    var token: dispatch_once_t = 0
    //------------------------------------------------------------------------
    //MARK: - ViewController LifeCycle
    //------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.menuDropDownVC = MenuDropDownVC.init(nibName: "MenuDropDownVC", bundle: nil)
        self.menuDropDownVC.delegate = self
        self.menuDropDownVC.view.frame = CGRectMake(0, 64, self.view.bounds.width, 0)
        self.view.addSubview(self.menuDropDownVC.view)
        
        
        //appDelegate.cartView.delegate = self
        showCustomNavigationBarView()
        
    }

    override func viewWillAppear(animated: Bool) {
        
        appDelegate.cartView.delegate = self
        self.menuDropDownVC.initialSetUp()
        
    }
    
    func showCustomNavigationBarView() {
        
        
        
        
        self.customNavBarView = NSBundle.mainBundle().loadNibNamed("CustomNavBarView", owner: self, options: nil).last as! CustomNavBarView
        self.customNavBarView.frame = CGRect(x: 0, y: 20, width: self.view.bounds.width, height: 44)
        self.customNavBarView.parentViewController = self
        
        self.customNavBarView.showNavigationBackButton(false)
        self.customNavBarView.showNavigationTitleLabel(false)
        self.customNavBarView.showNavigationSearchButton(true)
        self.customNavBarView.showNavigationMenuButton(true)
        self.customNavBarView.showNavigationLogoImageView(true)
        
        self.customNavBarView.createRightMarginToNavigationCartButton()
        self.customNavBarView.navigationMenuButton.addTarget(self, action: #selector(BaseVC.menuDropDownAction(_:)), forControlEvents: .TouchUpInside)
        
        self.view.addSubview(self.customNavBarView)
   
    }
    
    
    @IBAction func menuDropDownAction(sender: UIButton?) {
        
        if self.customNavBarView.navigationMenuButton.selected == false {
        
            self.customNavBarView.navigationMenuButton.selected = true
            
        UIView.animateWithDuration(0.25,
                                   delay: 0.0,
                                   options: UIViewAnimationOptions.CurveEaseIn,
                                   animations: { () -> Void in
                                    self.menuDropDownVC.view.frame = CGRectMake(0.0, 64, self.viewSize.width, self.viewSize.height)
        }) { (finished) -> Void in
            
        }
        }else {
            
            self.customNavBarView.navigationMenuButton.selected = false
            UIView.animateWithDuration(0.25,
                                       delay: 0.0,
                                       options: UIViewAnimationOptions.CurveEaseIn,
                                       animations: { () -> Void in
                                        self.menuDropDownVC.view.frame = CGRectMake(0.0, 64, self.viewSize.width, 0)
            }) { (finished) -> Void in
                
            }

        }
        
    }
    
    
    
    //-----------------------------------------------------------------
    // MARK: - Setup Search Bar -
    //-----------------------------------------------------------------


    
    
    //---------------------------------------------------------------------
    // MARK: - MenuDropDownDelegate
    //---------------------------------------------------------------------
    
    func navigateToSelectedViewController(VC: String) {
        
        if VC == "SignOut" {
            
           appDelegate.cartView.cartLabel.hidden = true
           appDelegate.cartView.cartLabel.text = "\(0)"
           AppUtility.showToast("Log Out Successful", view: self.view)
           AppUtility.backToRootView(self.navigationController!)
            
            
        }else {
          let storyBoard = UIStoryboard(name: "Main", bundle: nil)
          let selectedVC  = storyBoard.instantiateViewControllerWithIdentifier(VC)
          self.navigationController?.pushViewController(selectedVC, animated: true)
        }

        
    }

    //---------------------------------------------------------------------
    // MARK: - CartbtnDelegate
    //---------------------------------------------------------------------
    
    func pushToSelectedViewController(sender: String) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let selectedVC  = storyBoard.instantiateViewControllerWithIdentifier("ShoppingCartVC")
        self.navigationController?.pushViewController(selectedVC, animated: true)
    }
    
    
    
    //------------------------------------------------------------------------
    //MARK: - Memory Management
    //------------------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
