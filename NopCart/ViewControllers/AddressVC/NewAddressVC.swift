//
//  NewAddressVC.swift
//  NopCart
//
//  Created by BS-125 on 8/23/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import MBProgressHUD
import DropDown
import SwiftyJSON
import ObjectMapper
class NewAddressVC: BaseVC ,UITextFieldDelegate {
    
    
    
    @IBOutlet weak var addressOptionLabel: UILabel!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var newAddressScrollView: UIScrollView!
    @IBOutlet weak var continueButtonBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var firstNameRequiredLabel: UILabel!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var lastNameRequiredLabel: UILabel!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailRequiredLabel: UILabel!
    
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var companyRequiredLabel: UILabel!
    
    @IBOutlet weak var countryDropDownView: UIView!
    @IBOutlet weak var countryLabel: UILabel!
    
    @IBOutlet weak var stateDropDownView: UIView!
    @IBOutlet weak var stateLabel: UILabel!
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var cityRequiredLabel: UILabel!
    
    @IBOutlet weak var address1TextField: UITextField!
    @IBOutlet weak var address1RequiredLabel: UILabel!
    
    @IBOutlet weak var address2TextField: UITextField!
    @IBOutlet weak var address2RequiredLabel: UILabel!
    
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var zipCodeRequiredLabel: UILabel!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var phoneNumberRequiredTextField: UILabel!
    
    @IBOutlet weak var faxNumberTextField: UITextField!
    @IBOutlet weak var faxNumberRequiredLabel: UILabel!
    
    var customerAddressId = Int?()
    var requiredTextFieldArray = [UITextField]()
    var newBillingAddress = Newaddress?()
    var statesModel = StatesModel?()
    let countryDropDown = DropDown()
    let stateDropDown = DropDown()
    var selectedCountryId: Int?
    var selectedStateId: Int?

    var newAddressesSelected: Bool?
    var selectedExistingAddressId: Int?
    //var editCustomerAddressModel = EditCustomerAddressModel?()
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = false
        
        
        
        self.selectedCountryId = 0
        self.selectedStateId = -1
        countryDropDown.anchorView = self.countryDropDownView
        countryDropDown.bottomOffset = CGPoint(x: self.countryDropDownView.bounds.origin.x, y: self.countryDropDownView.bounds.origin.y + 50)
        
        countryDropDown.selectionAction = {
            [unowned self] (index, item) in
            
            self.countryLabel.text = item
            
            let country = self.newBillingAddress!.AvailableCountries![index]
            
            self.selectedCountryId = Int(country.Value! as String)
            self.selectedStateId = -1
            self.stateLabel.text = "Select state"
            
            self.stateDropDown.dataSource.removeAll()
            
            if self.selectedCountryId == 0 {
                return
            }
            
            
            self.getStatesInfo(self.selectedCountryId!)
        }
        
        stateDropDown.anchorView = self.stateDropDownView
        stateDropDown.bottomOffset = CGPoint(x: self.stateDropDownView.bounds.origin.x, y: self.stateDropDownView.bounds.origin.y + 50)
        stateDropDown.selectionAction = {
            [unowned self] (index, item) in
            
            let state = self.statesModel!.data![index]
            self.selectedStateId = Int(state.id)
            self.stateLabel.text = item

        }
        
        if (self.newBillingAddress != nil) {
            self.populateNewAddressForm()
        }
     }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if (self.newBillingAddress == nil) {
            
            //self.currentlyAvailabeStates.removeAll()
            self.countryDropDown.dataSource.removeAll()
            self.stateDropDown.dataSource.removeAll()
            self.requiredTextFieldArray.removeAll()
            self.countryLabel.text = "Select country"
            self.stateLabel.text = "Select state"
            self.selectedCountryId = 0
            self.selectedStateId = -1
            
            self.getBillingAddressForm()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func populateNewAddressForm() {
        if let firstName = self.newBillingAddress!.FirstName {
            self.firstNameTextField.text = firstName as String
        } else {
            self.firstNameTextField.text = ""
        }
        self.requiredTextFieldArray.append(self.firstNameTextField)
        
        if let lastName = self.newBillingAddress?.LastName {
            
            self.lastNameTextField.text = lastName as String
            
        } else {
            self.lastNameTextField.text = ""
        }
        self.requiredTextFieldArray.append(self.lastNameTextField)
        
        if let email = newBillingAddress!.Email {
            self.emailTextField.text = email as String
        } else {
            self.emailTextField.text = ""
        }
        self.requiredTextFieldArray.append(self.emailTextField)
        
        if let company = newBillingAddress!.Company {
            self.companyTextField.text = company as String
        } else {
            self.companyTextField.text = ""
        }
        self.companyRequiredLabel.hidden = true
        if newBillingAddress!.CompanyRequired == true {
            self.companyRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.companyTextField)
        }
        
        for country in (self.newBillingAddress?.AvailableCountries)! {
            self.countryDropDown.dataSource.append(country.Text! as String)
        }
        
        if let city = self.newBillingAddress?.City {
            self.cityTextField.text = city as String
        } else {
            self.cityTextField.text = ""
        }
        self.cityRequiredLabel.hidden = true
        if self.newBillingAddress?.CityRequired == true{
            self.cityRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.cityTextField)
        }
        
        if let address1 = self.newBillingAddress?.Address1 {
            self.address1TextField.text = address1 as String
        } else {
            self.address1TextField.text = ""
        }
        self.address1RequiredLabel.hidden = true
        if self.newBillingAddress?.StreetAddressRequired == true {
            self.address1RequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.address1TextField)
        }
        
        if let address2 = self.newBillingAddress?.Address2 {
            self.address2TextField.text = address2 as String
        } else {
            self.address2TextField.text = ""
        }
        self.address2RequiredLabel.hidden = true
        if self.newBillingAddress?.StreetAddress2Required == true {
            self.address2RequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.address2TextField)
        }
        
        if let zipCode = self.newBillingAddress?.ZipPostalCode {
            self.zipCodeTextField.text = zipCode as String
        } else {
            self.zipCodeTextField.text = ""
        }
        self.zipCodeRequiredLabel.hidden = true
        if self.newBillingAddress?.ZipPostalCodeRequired == true {
            self.zipCodeRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.zipCodeTextField)
        }
        
        if let phone = self.newBillingAddress?.PhoneNumber {
            self.phoneNumberTextField.text = phone as String
        } else {
            self.phoneNumberTextField.text = ""
        }
        self.phoneNumberRequiredTextField.hidden = true
        if self.newBillingAddress?.PhoneRequired == true {
            self.phoneNumberRequiredTextField.hidden = false
            self.requiredTextFieldArray.append(self.phoneNumberTextField)
        }
        
        if let fax = self.newBillingAddress?.FaxNumber {
            self.faxNumberTextField.text = fax as String
        } else {
            self.faxNumberTextField.text = ""
        }
        self.faxNumberRequiredLabel.hidden = true
        if self.newBillingAddress?.FaxRequired == true {
            self.faxNumberRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.faxNumberTextField)
        }
    }
    
    
    
    @IBAction func saveBtnAct(sender: AnyObject) {
        
        
        
        for textField in self.requiredTextFieldArray {
            if textField.text?.characters.count == 0 {
                AppUtility.showToast("Please fill up all the required fields.", view: self.view)
                return
            }
        }
        
        //if self.selectedCountryId == 0 || self.selectedStateId == -1 {
          //  AppUtility.showToast("Please fill up all the required fields.",view: self.view)
            //return
        //}
        
        let firstNameDictionary = ["value":self.firstNameTextField.text!, "key":"Address.FirstName"]
        let lastNameDictionary = ["value":self.lastNameTextField.text!, "key":"Address.LastName"]
        let emailDictionary = ["value":self.emailTextField.text!, "key":"Address.Email"]
        let companyDictionary = ["value":self.companyTextField.text!, "key":"Address.Company"]
        let countryDictionary = ["value":String(self.selectedCountryId!), "key":"Address.CountryId"]
        let stateDictionary = ["value":String(self.selectedStateId!), "key":"Address.StateProvinceId"]
        let cityDictionary = ["value":self.cityTextField.text!, "key":"Address.City"]
        let address1Dictionary = ["value":self.address1TextField.text!, "key":"Address.Address1"]
        let address2Dictionary = ["value":self.address2TextField.text!, "key":"Address.Address2"]
        let zipCodeDictionary = ["value":self.zipCodeTextField.text!, "key":"Address.ZipPostalCode"]
        let phoneDictionary = ["value":self.phoneNumberTextField.text!, "key":"Address.PhoneNumber"]
        let faxDictionary = ["value":self.faxNumberTextField.text!, "key":"Address.FaxNumber"]
        
        let parameters = [firstNameDictionary, lastNameDictionary, emailDictionary, companyDictionary, countryDictionary, stateDictionary, cityDictionary, address1Dictionary, address2Dictionary, zipCodeDictionary, phoneDictionary, faxDictionary]
        print(parameters)
        
        
        if self.customerAddressId != nil {
            
            self.EditCustomerAddress(parameters)
        }
        else{
            
            self.AddNewAddress(parameters)
        }
    }
    
    
    
    
    @IBAction func countryDropDownTapped(sender: AnyObject) {
        if countryDropDown.hidden {
            countryDropDown.show()
        } else {
            countryDropDown.hide()
        }
    }
    
    @IBAction func stateDropDownTapped(sender: AnyObject) {
        if stateDropDown.hidden {
            stateDropDown.show()
        } else {
            stateDropDown.hide()
        }
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    

    func getBillingAddressForm () {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@%@", SessionManager.manager.baseURL,AppConstants.Api.BillingAddressForm ) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        
                        if let list = Mapper<BillingAndShippingAddressModel>().map(json.dictionaryObject) {
                            
                            self.newBillingAddress = list.NewAddress
                            
                            self.populateNewAddressForm()
                            
                        }
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }
    
    func EditCustomerAddress(parameters: [AnyObject])
    {
        AppUtility.showLoading(self.view)
        let editCustomerAddress = String(format: "%@%@/%d",SessionManager.manager.baseURL,AppConstants.Api.EditCustomerAddress, self.customerAddressId!)
        
        let manager = RequestManager.manager
        let request = NSMutableURLRequest(URL: NSURL(string: editCustomerAddress)!)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = AppUtility.getHeaderDic()
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        manager.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .Success:
                    print("Success")
                    
                    AppUtility.hideLoading(self.view)
                    
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        
                        print(json)
                        if json["StatusCode"].intValue == 200 {
                            
                            AppUtility.showToast("Successfully Updated Info", view: self.view)
                            
                         }else {
                            var errorMessage = ""
                            if let errorList = json["ErrorList"].arrayObject {
                                for error in errorList {
                                    errorMessage += "\(error as! String)\n"
                                }
                            }
                            AppUtility.showToast(errorMessage, view: self.view)
                        }
                    }
                    
                case .Failure:
                    print("Failure")
                    print("Error:\(response.result.error?.localizedDescription)")
                    AppUtility.showToast("\(response.result.error?.localizedDescription)", view: self.view)
                }
        }
    }

    
    func AddNewAddress(parameters: [AnyObject])
    {
        AppUtility.showLoading(self.view)
        let editCustomerAddress = String(format: "%@%@",SessionManager.manager.baseURL,AppConstants.Api.AddNewAddress)
        
        let manager = RequestManager.manager
        let request = NSMutableURLRequest(URL: NSURL(string: editCustomerAddress)!)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = AppUtility.getHeaderDic()
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        manager.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .Success:
                    print("Success")
                    
                    AppUtility.hideLoading(self.view)
                    
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        
                        print(json)
                        if json["StatusCode"].intValue == 200 {
                            
                           AppUtility.showToast("Successfully Added Info", view: self.view)
                            
                        }else {
                            var errorMessage = ""
                            if let errorList = json["ErrorList"].arrayObject {
                                for error in errorList {
                                    errorMessage += "\(error as! String)\n"
                                }
                            }
                            AppUtility.showToast(errorMessage, view: self.view)
                        }
                    }
                    
                case .Failure:
                    print("Failure")
                    print("Error:\(response.result.error?.localizedDescription)")
                    AppUtility.showToast("\(response.result.error?.localizedDescription)", view: self.view)
                }
        }
    }
    
    
    func getStatesInfo(countryId:NSInteger) {
        
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@%@/%d", SessionManager.manager.baseURL,AppConstants.Api.StatesByCountryId,countryId ) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if let list = Mapper<StatesModel>().map(json.dictionaryObject) {
                            self.statesModel = list
                            if let data = self.statesModel?.data {
                                for state in data {
                                    self.stateDropDown.dataSource.append(state.name! as String)
                                    
                                }
                            }
                            
                        }
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }
    
    // MARK: - UITextField Delegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(textField: UITextField) {
        print("TextField did end editing method called")
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        print("TextField should snd editing method called")
        return true;
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        print("TextField should return method called")
        
        textField.resignFirstResponder();
        
        
        return true;
    }
}
