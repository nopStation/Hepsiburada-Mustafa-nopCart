//
//  AddressVC.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import MBProgressHUD
import Alamofire
import DropDown

class AddressVC: BaseVC {

    
    @IBOutlet weak var tableView: UITableView!
    
    var customerAddressModel = CustomerAddressModel?()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = false
        
        
        
        tableView.registerNib(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "AddressTableViewCell")
        
        self.tableView.estimatedRowHeight = 150.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        // Do any additional setup after loading the view.
        
        
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
      
        self.getCustomerAddress()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let address = customerAddressModel?.existingAddresses {
          return address.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        var cell = tableView.dequeueReusableCellWithIdentifier("AddressTableViewCell") as! AddressTableViewCell!
        if cell == nil {
            cell = AddressTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "AddressTableViewCell")
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.deleteBtn.tag = indexPath.row
        cell.editBtn.tag = indexPath.row
        
        cell.deleteBtn.addTarget(self, action: #selector(AddressVC.deleteBtnAct(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.editBtn.addTarget(self, action: #selector(AddressVC.editBtnAct(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        cell.containerView.layer.cornerRadius = 4.0
        
        let address = customerAddressModel?.existingAddresses![indexPath.row]
        
        var firstName = ""
        if let fName = address?.FirstName {
            firstName = fName
        }
        
        var lastName = ""
        if let lName = address?.LastName {
            lastName = lName
        }
        
        cell.nameLbl.text = String(format: "%@ %@", firstName, lastName)
        
        
        var email = ""
        if let mail = address?.Email {
            email = mail
        }
        
        var phoneNo = ""
        if let phone  = address?.PhoneNumber {
            phoneNo = phone
        }
        
        var faxNo = ""
        if let fax   = address?.FaxNumber {
            faxNo = fax
        }
        
        var address1 = ""
        if let address    = address?.Address1 {
            address1 = address
        }
        
        var cityName = ""
        if let city  = address?.City {
            cityName = city
        }
        
        var countryName = ""
        if let country  = address?.CountryName {
            countryName = country
        }
        
        let detailsInfo = String(format: "Email : %@\nPhone number : %@\nFax number : %@\n%@, %@\n%@",email, phoneNo , faxNo, address1, cityName, countryName)
        
        cell.detailsInfoLbl.text =  detailsInfo
        
        return cell
        
    }
    
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    
    
    
    
    func deleteBtnAct(sender:UIButton!)
    {
        self.deleteCustomerAddress(sender.tag)
    }
    
    func editBtnAct(sender:UIButton!)
    {
        self.EditCustomerAddress(sender.tag)
    }

    
    //-----------------------------------------------------------------
    // MARK: - User Interaction
    //-----------------------------------------------------------------

    @IBAction func addNewBtnAct(sender: AnyObject) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let  createNewAddVC = storyBoard.instantiateViewControllerWithIdentifier("NewAddressVC") as? NewAddressVC
        self.navigationController!.pushViewController(createNewAddVC!, animated: true)
        
    }

    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------

    func getCustomerAddress()
    {
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, AppConstants.Api.CustomerAddress, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if let list = Mapper<CustomerAddressModel>().map(json.dictionaryObject) {
                            self.customerAddressModel = list
                            self.tableView.reloadData()
                            
                        }
                        
                    }
                    else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
    }

    
    
    func EditCustomerAddress(addressId:NSInteger)
      {
        AppUtility.showLoading(self.view)
        let editCustomerAddress = String(format: "%@%@/%d",SessionManager.manager.baseURL,AppConstants.Api.EditCustomerAddress,(self.customerAddressModel?.existingAddresses![addressId].Id)!)

        let manager = RequestManager.manager
        manager.request(.GET,editCustomerAddress , parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if let list = Mapper<EditCustomerAddressModel>().map(json.dictionaryObject){
                           
                            let storyBoard   = UIStoryboard(name: "Main", bundle: nil)
                            let editCustomer = storyBoard.instantiateViewControllerWithIdentifier("NewAddressVC") as? NewAddressVC
                            editCustomer!.newBillingAddress = list.address
                            editCustomer!.customerAddressId = self.customerAddressModel?.existingAddresses![addressId].Id
                            self.navigationController!.pushViewController(editCustomer!, animated: true)
                        }
                     }
                    else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
    }

    
    func deleteCustomerAddress(addressId:NSInteger)
    {
        AppUtility.showLoading(self.view)
        
        let customerAddressID = String(format: "%@%@/%d",SessionManager.manager.baseURL, AppConstants.Api.DeleteCustomerAddress,(self.customerAddressModel?.existingAddresses![addressId].Id)!)
        let manager = RequestManager.manager
        manager.request(.GET, customerAddressID, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        self.customerAddressModel?.existingAddresses?.removeAtIndex(addressId)
                        self.tableView.reloadData()
                        AppUtility.showToast("Successfully Deleted Address!", view: self.view)

                        
                    }
                    else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
    }

    
}
