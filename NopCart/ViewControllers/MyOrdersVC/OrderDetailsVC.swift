//
//  OrderDetailsVC.swift
//  NopCart
//
//  Created by BS-125 on 9/2/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
class OrderDetailsVC: BaseVC  {

    @IBOutlet weak var contentScrollView: UIScrollView!
    
    @IBOutlet weak var attributsLbl: UILabel!
    @IBOutlet weak var contentTableViewHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var orderNumberLbl: UILabel!
    @IBOutlet weak var orderTotalLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    
    @IBOutlet weak var billingAddressLbl: UILabel!
    @IBOutlet weak var shippingAddressLbl: UILabel!
    @IBOutlet weak var shippingMethod: UILabel!
    
    @IBOutlet weak var contentTableView: UITableView!
    
    @IBOutlet weak var billingMethod: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var shippingLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    
    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var billingAddressLblView: UIView!
    @IBOutlet weak var shippingAddressLblView: UIView!
    @IBOutlet weak var billingLblView: UIView!
    @IBOutlet weak var shippingLblView: UIView!
    @IBOutlet weak var attributsLblView: UIView!
    
    @IBOutlet weak var attributesLblViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var attributesLblBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var attributesLblTopConstraint: NSLayoutConstraint!
    
    
    var flag = Bool ()
    
    var orderDetailsInfo = OrderDetailsModel?()
    var orderInfo            = Orders?()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = false
        
        
        
        self.contentScrollView.layoutIfNeeded()
        self.orderView.layer.cornerRadius = 4.0
        self.billingAddressLblView.layer.cornerRadius = 4.0
        self.shippingAddressLblView.layer.cornerRadius = 4.0
        self.billingLblView.layer.cornerRadius = 4.0
        self.shippingLblView.layer.cornerRadius = 4.0
        self.attributsLblView.layer.cornerRadius = 4.0
        
        
        
        self.getOrderDetailsInfo((self.orderInfo?.Id)!)
        
        /*self.aPIManager!.getOrderDetails(self.orderInfo.Id, onSuccess: {
            gotOrderDetails in
            
            self.orderDetailsInfo = gotOrderDetails
            
            self.contentTableViewHeightConstant.constant = CGFloat(self.orderDetailsInfo.items.count) * 150.0
            
            
            let billingAddress = "\(self.orderDetailsInfo.billingAddress!.firstName ?? "")\(self.orderDetailsInfo.billingAddress!.lastName ?? "")\n\(self.orderDetailsInfo.billingAddress!.email ?? "")\n\(self.orderDetailsInfo.billingAddress!.phoneNumber ?? "")\n\(self.orderDetailsInfo.billingAddress!.address1 ?? ""),\(self.orderDetailsInfo.billingAddress!.cityName ?? "")\n\(self.orderDetailsInfo.billingAddress!.countryName ?? "")"
            self.billingAddressLbl.text = billingAddress
            
            
            let shippingAddress = "\(self.orderDetailsInfo.shippingAddress!.firstName ?? "")\(self.orderDetailsInfo.shippingAddress!.lastName ?? "")\n\(self.orderDetailsInfo.shippingAddress!.email ?? "")\n\(self.orderDetailsInfo.shippingAddress!.phoneNumber ?? "")\n\(self.orderDetailsInfo.shippingAddress!.address1 ?? ""),\(self.orderDetailsInfo.shippingAddress!.cityName ?? "")\n\(self.orderDetailsInfo.shippingAddress!.countryName ?? "")"
            
            
            print(shippingAddress)
            self.shippingAddressLbl.text = shippingAddress
            
            
            
            
            self.orderDateLbl.text    =  NSString(format: "Order Date : %@",self.orderInfo.OrderStatus) as String
            self.orderStatusLbl.text  =  NSString(format: "Order Status : %@",self.orderInfo.OrderStatus) as String
            self.orderTotalLbl.text   =  NSString(format: "Order Total : %@",self.orderInfo.OrderTotal) as String
            self.orderNumberLbl.text  =  NSString(format: "ORDER # %d",self.orderInfo.Id) as String
            
            
            let billingMethod = String(format: "Payment Method : %@\nPayment Status : %@", self.orderDetailsInfo.PaymentMethod ?? "", self.orderDetailsInfo.PaymentMethodStatus ?? "")
            self.billingMethod.text = billingMethod
            
            
            let shippingMethod = String(format: "Shipping Method : %@\nShipping Status : %@", self.orderDetailsInfo.ShippingMethod ?? "", self.orderDetailsInfo.ShippingStatus ?? "")
            self.shippingMethod.text = shippingMethod
            
            
            if let subTotal = self.orderDetailsInfo.OrderSubtotal {
                self.subTotalLbl.text = subTotal
            }
            
            if let shipping = self.orderDetailsInfo.OrderShipping {
                self.shippingLbl.text = shipping
            }
            
            if let  total = self.orderDetailsInfo.OrderTotal {
                self.totalLbl.text = total
            }
            
            if let tax = self.orderDetailsInfo.Tax {
                self.taxLbl.text = tax
            }
            
            let attributsArray:[String] = self.orderDetailsInfo.CheckoutAttributeInfo!.componentsSeparatedByString("<br />")
            
            var attributsLabelText = ""
            
            for attributs in attributsArray {
                attributsLabelText += String(format: "%@\n",attributs)
            }
            
            attributsLabelText = attributsLabelText.substringToIndex(attributsLabelText.endIndex.predecessor())
            
            self.attributsLbl.text = attributsLabelText
            
            if attributsLabelText.characters.count == 0 {
                self.attributesLblViewTopConstraint.constant = 0
                self.attributesLblBottomConstraint.constant = 0
                self.attributesLblTopConstraint.constant = 0
                self.attributsLblView.hidden = true
            }
            
            self.contentTableView.reloadData()
            
            self.contentScrollView.hidden = false
            self.hideLoading()
            
            },
                                         onError: { message in
                                            print(message)
                                            self.hideLoading()
                                            self.showToast(message)
                                            
        })*/

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    func getOrderDetailsInfo(orderId:NSInteger) {
        
        
        AppUtility.showLoading(self.view)
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@/%d", AppConstants.Api.OrderDetails, orderId) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if let list = Mapper<OrderDetailsModel>().map(json.dictionaryObject) {
                            
                            self.orderDetailsInfo = list
                            
                            self.contentTableViewHeightConstant.constant = CGFloat(self.orderDetailsInfo!.items!.count) * 150.0
                            
                            let billingAddress = "\(self.orderDetailsInfo?.BillingAddress!.FirstName ?? "")\(self.orderDetailsInfo!.BillingAddress!.LastName ?? "")\n\(self.orderDetailsInfo!.BillingAddress!.Email ?? "")\n\(self.orderDetailsInfo!.BillingAddress!.PhoneNumber ?? "")\n\(self.orderDetailsInfo!.BillingAddress!.Address1 ?? ""),\(self.orderDetailsInfo!.BillingAddress!.City ?? "")\n\(self.orderDetailsInfo!.BillingAddress!.CountryName ?? "")"
                            
                            self.billingAddressLbl.text = billingAddress
                            
                            let shippingAddress = "\(self.orderDetailsInfo?.ShippingAddress!.FirstName ?? "")\(self.orderDetailsInfo!.ShippingAddress!.LastName ?? "")\n\(self.orderDetailsInfo!.ShippingAddress!.Email ?? "")\n\(self.orderDetailsInfo!.ShippingAddress!.PhoneNumber ?? "")\n\(self.orderDetailsInfo!.BillingAddress!.Address1 ?? ""),\(self.orderDetailsInfo!.ShippingAddress!.City ?? "")\n\(self.orderDetailsInfo!.ShippingAddress!.CountryName ?? "")"
                            
                            self.shippingAddressLbl.text = shippingAddress

                            self.orderDateLbl.text    =  String(format: "Order Date : %@",self.orderInfo!.OrderStatus!)
                            self.orderStatusLbl.text  =  String(format: "Order Status : %@",self.orderInfo!.OrderStatus!)
                            self.orderTotalLbl.text   =  String(format: "Order Total : %@",self.orderInfo!.OrderTotal!)
                            self.orderNumberLbl.text  =  String(format: "ORDER # %d",self.orderInfo!.Id)
                            
                            
                            let billingMethod = String(format: "Payment Method : %@\nPayment Status : %@", self.orderDetailsInfo!.PaymentMethod ?? "", self.orderDetailsInfo!.PaymentMethodStatus ?? "")
                            self.billingMethod.text = billingMethod
                            
                            
                            let shippingMethod = String(format: "Shipping Method : %@\nShipping Status : %@", self.orderDetailsInfo!.ShippingMethod ?? "", self.orderDetailsInfo!.ShippingStatus ?? "")
                            self.shippingMethod.text = shippingMethod
                            
                            
                            if let subTotal = self.orderDetailsInfo!.OrderSubtotal {
                                self.subTotalLbl.text = subTotal
                            }
                            
                            if let shipping = self.orderDetailsInfo!.OrderShipping {
                                self.shippingLbl.text = shipping
                            }
                            
                            if let  total = self.orderDetailsInfo!.OrderTotal {
                                self.totalLbl.text = total
                            }
                            
                            if let tax = self.orderDetailsInfo!.Tax {
                                self.taxLbl.text = tax
                            }
                            
                            let attributsArray:[String] = self.orderDetailsInfo!.CheckoutAttributeInfo!.componentsSeparatedByString("<br />")
                            
                            var attributsLabelText = ""
                            
                            for attributs in attributsArray {
                                attributsLabelText += String(format: "%@\n",attributs)
                            }
                            
                            attributsLabelText = attributsLabelText.substringToIndex(attributsLabelText.endIndex.predecessor())
                            
                            self.attributsLbl.text = attributsLabelText
                            
                            if attributsLabelText.characters.count == 0 {
                                self.attributesLblViewTopConstraint.constant = 0
                                self.attributesLblBottomConstraint.constant = 0
                                self.attributesLblTopConstraint.constant = 0
                                self.attributsLblView.hidden = true
                            }
                            
                            self.contentTableView.reloadData()
                            self.contentScrollView.hidden = false
                        }
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
        
    }
    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.contentTableView {
            
            if let orderdetails = self.orderDetailsInfo?.items {
                
                return orderdetails.count
            }
            return 0
            
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        
            var cell: ConfirmOrderTableViewCell! = tableView.dequeueReusableCellWithIdentifier("ConfirmOrderTableViewCell") as? ConfirmOrderTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "ConfirmOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "ConfirmOrderTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("ConfirmOrderTableViewCell") as? ConfirmOrderTableViewCell
            }
            
            cell.itemImageView.layer.borderWidth   = 1.0
            cell.itemImageView.layer.masksToBounds = false
            cell.itemImageView.layer.borderColor   = UIColor.whiteColor().CGColor
            cell.itemImageView.layer.cornerRadius  = 13
            cell.itemImageView.layer.cornerRadius  = cell.itemImageView.frame.size.height/2
            cell.itemImageView.clipsToBounds = true
            let imageUrl = self.orderDetailsInfo!.items![indexPath.row].picture!.ImageUrl
            
            cell.itemImageView!.sd_setImageWithURL(NSURL(string:imageUrl!))
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.titleLbl.text = self.orderDetailsInfo!.items![indexPath.row].ProductName
            cell.UnitPrice.text = self.orderDetailsInfo!.items![indexPath.row].UnitPrice
            cell.subTotal.text = self.orderDetailsInfo!.items![indexPath.row].SubTotal
            cell.quantity.text = String(format: "Quantity:%d", self.orderDetailsInfo!.items![indexPath.row].Quantity)
            
            
            //self.roundRectToAllView(cell.containerView)
            
            if (indexPath.row == self.orderDetailsInfo!.items!.count-1) {
                cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            }
        
            return cell
    }
    
    
    
    func roundRectToAllView(view: UIView )->Void {
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.whiteColor()
        view.layer.shadowColor = UIColor.darkGrayColor().CGColor
        view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        view.layer.shadowOpacity = 0.50
        view.layer.shadowRadius = 1
        view.layer.masksToBounds = true
        view.clipsToBounds = false
        
    }
    
    // MARK: - Table view data source
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        if tableView == contentTableView {
            
            
        }
    }


}
