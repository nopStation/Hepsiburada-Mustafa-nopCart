//
//  MyOrdersVC.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
class MyOrdersVC: BaseVC {

    
    @IBOutlet weak var tableView: UITableView!
    var customerOrderModel = MyOrdersModel?()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = false
        
        
        
        tableView.registerNib(UINib(nibName: "OrdersTableViewCell", bundle: nil), forCellReuseIdentifier:"OrdersTableViewCell")
        self.getCustomerOrderInfo()
    }

    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    func getCustomerOrderInfo() {
        
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, AppConstants.Api.CustomerOrders, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if let list = Mapper<MyOrdersModel>().map(json.dictionaryObject) {
                          
                            self.customerOrderModel = list
                            self.tableView.reloadData()
                            
                        }
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
        
    }

    
    
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let orders = self.customerOrderModel?.orders {
        return orders.count
        }
        
        return 0
        
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:OrdersTableViewCell! = tableView.dequeueReusableCellWithIdentifier("OrdersTableViewCell") as? OrdersTableViewCell
        if(cell == nil)
        {
            cell = OrdersTableViewCell(style: UITableViewCellStyle.Subtitle,reuseIdentifier:"OrdersTableViewCell")
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        
        let orders = self.customerOrderModel?.orders![indexPath.row]
        
        cell.containerView!.layer.cornerRadius = 4.0
        cell.orderNumberId.text   = String(format: "Order Number:%d", (orders?.Id)!)
        cell.orderStatus.text     = String(format: "Order Status:%@",(orders?.OrderStatus)!)
        cell.orderDate.text       = String(format: "Order Date:%@",(orders?.CreatedOn)!)
        cell.orderTotal.text      = String(format: "Order Total:%@",(orders?.OrderTotal)!)
        
        return cell
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let orderDetailsVC = storyBoard.instantiateViewControllerWithIdentifier("OrderDetailsVC") as? OrderDetailsVC
        orderDetailsVC?.orderInfo  = self.customerOrderModel?.orders![indexPath.row]
        self.navigationController!.pushViewController(orderDetailsVC!, animated: true)
    }

}
