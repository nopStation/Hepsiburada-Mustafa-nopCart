//
//  CategoryListVC.swift
//  NopCart
//
//  Created by BS85 on 5/3/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyJSON
import SlideMenuControllerSwift


enum AnimationType:Int {
    case Fade = 1,                   //淡入淡出
    Push,                       //推挤
    Reveal,                     //揭开
    MoveIn,                     //覆盖
    Cube,                       //立方体
    SuckEffect,                 //吮吸
    OglFlip,                    //翻转
    RippleEffect,               //波纹
    PageCurl,                   //翻页
    PageUnCurl,                 //反翻页
    CameraIrisHollowOpen,       //开镜头
    CameraIrisHollowClose,      //关镜头
    CurlDown,                   //下翻页
    CurlUp,                     //上翻页
    FlipFromLeft,               //左翻转
    FlipFromRight             //右翻转
    
}



class CategoryListVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var menuItems = [Children]()
    var selectedCategoryArray = NSMutableArray ()
    var backBtnBool : Bool = true
    var categoryTitleBool : Bool = false
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    
    
    //---------------------------------------------------------------------
    //MARK: - ViewController LifeCycle
    //---------------------------------------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        backBtn.hidden = backBtnBool
        categoryLabel.hidden = categoryTitleBool
        
        self.view.backgroundColor = UIColor.darkGrayColor()
        
        tableView.registerNib(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryTableViewCell")
        tableView.registerNib(UINib(nibName: "SelectedIndexCell", bundle: nil), forCellReuseIdentifier: "SelectedIndexCell")

        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(CategoryListVC.reloadContainerTableView(_:)), name:"com.notification.reloadTableView", object: nil)
        
        
        //self.animateTable()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    
    /*deinit {
        
        if self.view != nil {
            
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.reloadTableView", object: nil)
        }
        
    }*/

    //---------------------------------------------------------------------
    //MARK: - Utility Methods
    //---------------------------------------------------------------------

    func reloadContainerTableView(notification: NSNotification) {

        
        selectedCategoryArray.addObject("Home")
        self.readLocalJSON()
        tableView.reloadData()
        
    }

    /*
    func animateTable() {
        
        
        tableView.reloadData()
    
        
         let cells = tableView.visibleCells
        let tableWidth: CGFloat = tableView.bounds.size.width
         for i in cells {
         let cell: UITableViewCell = i 
         cell.transform = CGAffineTransformMakeTranslation(-tableWidth, 0)
         }
         
         var index = 0
         
         for a in cells {
         let cell: UITableViewCell = a
            
            UIView.animateWithDuration(0.4, delay: 0.05 * Double(index++) , usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: {
                
                cell.transform = CGAffineTransformMakeTranslation(0, 0)
                }, completion: nil)
            
         }
    }*/
    
    // Loacal JSON
    
    func readLocalJSON() {
        if let path  = NSBundle.mainBundle().pathForResource("document", ofType: "json") {
            
            if let jsonData = try? NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe) {
                
                let jsonResult: NSDictionary = try! NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                
                let json  = JSON(jsonResult)
                menuItems = Mapper<Children>().mapArray(json["data"]["children"].arrayObject)!
                appDelegate.tempCategoryList = Mapper<Children>().mapArray(json["data"]["children"].arrayObject)!
                
                
                appDelegate.categoryDic.setObject(appDelegate.tempCategoryList, forKey: "Home")
                
            }
        }
        
    }
    
    //---------------------------------------------------------------------
    //MARK: - UITableViewDataSource
    //---------------------------------------------------------------------
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.menuItems.count > 0 {
         return self.menuItems.count + selectedCategoryArray.count
        }
        return 0
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if indexPath.row < selectedCategoryArray.count {
            
            var cell = tableView.dequeueReusableCellWithIdentifier("SelectedIndexCell") as! SelectedIndexCell!
            if cell == nil {
                cell = SelectedIndexCell(style: UITableViewCellStyle.Default, reuseIdentifier: "SelectedIndexCell")
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.titleLabel.textColor = UIColor(red: 252/255.0, green: 113/255.0, blue: 0/255.0, alpha: 1.0)
            cell.titleLabel.text = self.selectedCategoryArray[indexPath.row] as? String
            return cell

            
        }else {
            
            var cell = tableView.dequeueReusableCellWithIdentifier("CategoryTableViewCell") as! CategoryTableViewCell!
            if cell == nil {
                cell = CategoryTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CategoryTableViewCell")
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.None

           let index = abs(self.selectedCategoryArray.count-indexPath.row)
           cell.titleLabel.text = self.menuItems[index].name
            return cell

            
        }
        
        
    }
    
    
    //---------------------------------------------------------------------
    //MARK: - UITableViewDelegate
    //---------------------------------------------------------------------
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 44
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        if indexPath.row < selectedCategoryArray.count {
            
            if indexPath.row == 0 {
                
                print("Home Btn Action")
                appDelegate.slideMenuController.centerViewController    = appDelegate.homeVC
                appDelegate.slideMenuController.leftMenuViewController = appDelegate.categoryListVC
                appDelegate.slideMenuController.toggleLeftSideMenuCompletion(nil)

                
            }
                
            else if indexPath.row == 1 {
                
                
                if selectedCategoryArray.count == 3 {
                    selectedCategoryArray.removeObjectAtIndex(selectedCategoryArray.count-1)
                    selectedCategoryArray.removeObjectAtIndex(selectedCategoryArray.count-1)
                }
                else { selectedCategoryArray.removeObjectAtIndex(selectedCategoryArray.count-1)
                }
                appDelegate.categoryListVC!.menuItems = appDelegate.tempCategoryList
                appDelegate.categoryListVC!.backBtnBool = true
                appDelegate.categoryListVC!.categoryTitleBool = false
                appDelegate.categoryListVC!.selectedCategoryArray = self.selectedCategoryArray
                SlideMenuOptions.contentViewScale = 1
                SlideMenuOptions.hideStatusBar = false
                let animation = CATransition()
                animation.duration = 0.4
                animation.type = kCATransitionPush;
                animation.subtype = kCATransitionFromLeft;
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                appDelegate.categoryListVC!.view.layer.addAnimation(animation, forKey: "animation")
                
                self.tableView.reloadData()
            }
            else if indexPath.row == 2 {
                
                selectedCategoryArray.removeObjectAtIndex(selectedCategoryArray.count-1)
                let categoryName = selectedCategoryArray[selectedCategoryArray.count-1] as! String
                let parent = appDelegate.categoryDic.objectForKey(categoryName) as! Children
                appDelegate.categoryListVC!.menuItems = parent.children!
                appDelegate.categoryListVC!.backBtnBool = true
                appDelegate.categoryListVC!.categoryTitleBool = false
                appDelegate.categoryListVC!.selectedCategoryArray = self.selectedCategoryArray
                SlideMenuOptions.contentViewScale = 1
                SlideMenuOptions.hideStatusBar = false
                let animation = CATransition()
                animation.duration = 0.4
                animation.type = kCATransitionPush;
                animation.subtype = kCATransitionFromLeft;
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                appDelegate.categoryListVC!.view.layer.addAnimation(animation, forKey: "animation")
                
                self.tableView.reloadData()
                
            }
            
            
            
            
        }else {
            
            let index = abs(self.selectedCategoryArray.count-indexPath.row)
            let parent = self.menuItems[index]
            
            if parent.children?.count > 0 {
                appDelegate.categoryListVC!.menuItems = parent.children!
                appDelegate.categoryListVC!.backBtnBool = false
                appDelegate.categoryListVC!.categoryTitleBool = true
                selectedCategoryArray.addObject(parent.name!)
                appDelegate.categoryListVC!.selectedCategoryArray = self.selectedCategoryArray
                SlideMenuOptions.contentViewScale = 1
                SlideMenuOptions.hideStatusBar = false
                appDelegate.categoryDic.setObject(parent, forKey: parent.name!)
                
                let animation = CATransition()
                animation.duration = 0.4
                animation.type = kCATransitionPush;
                animation.subtype = kCATransitionFromRight;
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                appDelegate.categoryListVC!.view.layer.addAnimation(animation, forKey: "animation")
                self.tableView.reloadData()
                
            }else {
                
                
                print("parent.category_id====%@",parent.category_id!)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let productVC = storyBoard.instantiateViewControllerWithIdentifier("ProductVC") as? ProductVC
                SessionManager.manager.productModel = nil
                
                let filterVC = storyBoard.instantiateViewControllerWithIdentifier("FilterVC") as? FilterVC
                appDelegate.slideMenuController.centerViewController    = productVC
                appDelegate.slideMenuController.rightMenuViewController = filterVC
                appDelegate.slideMenuController.toggleLeftSideMenuCompletion(nil)
                
            }
            
        }
    }
    
    
    //---------------------------------------------------------------------
    //MARK: - UserInterection Methods
    //---------------------------------------------------------------------
    
    @IBAction func backButtonAction(sender: AnyObject) {
        
        
        if selectedCategoryArray.count>2 {
            
         let storyBoard = UIStoryboard(name: "Main", bundle: nil)
         let categoryListVC = storyBoard.instantiateViewControllerWithIdentifier("CategoryListVC") as! CategoryListVC
            
         selectedCategoryArray.removeObjectAtIndex(selectedCategoryArray.count-1)
   
         let categoryName = selectedCategoryArray[selectedCategoryArray.count-1] as! String
            
         let parent = appDelegate.categoryDic.objectForKey(categoryName) as! Children

         categoryListVC.selectedCategoryArray = self.selectedCategoryArray
         categoryListVC.menuItems = parent.children!
         categoryListVC.backBtnBool = false
         categoryListVC.categoryTitleBool = true
         appDelegate.slideMenuController.leftMenuViewController = categoryListVC
         SlideMenuOptions.contentViewScale = 1
         SlideMenuOptions.hideStatusBar = false
        }else if selectedCategoryArray.count == 2 {
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let categoryListVC = storyBoard.instantiateViewControllerWithIdentifier("CategoryListVC") as! CategoryListVC
            selectedCategoryArray.removeObjectAtIndex(selectedCategoryArray.count-1)
            categoryListVC.backBtnBool = true
            categoryListVC.categoryTitleBool = false
            categoryListVC.selectedCategoryArray = self.selectedCategoryArray
            categoryListVC.menuItems = appDelegate.tempCategoryList
            appDelegate.slideMenuController.leftMenuViewController = categoryListVC
            SlideMenuOptions.contentViewScale = 1
            SlideMenuOptions.hideStatusBar = false
        }

    }
    
    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
