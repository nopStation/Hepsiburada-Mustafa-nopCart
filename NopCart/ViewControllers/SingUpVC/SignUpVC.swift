//
//  SignUpVC.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import ObjectMapper

class SignUpVC: BaseVC , UITextFieldDelegate{

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var newsLetterButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPassTextField: UITextField!
    
    @IBOutlet weak var dropDownContainerView: UIView!
    //@IBOutlet weak var totalCartLabel: CustomLabel!
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    var dobSelected: Bool = false
    var dobDay: Int = 0
    var dobMonth: Int = 0
    var dobYear: Int = 0
    
    var genderSelected: Bool = false
    var gender: String = ""
    
    var newsLetterSelected: Bool = false
    
    
    
    //---------------------------------------------------------------------
    //MARK: - ViewController LifeCycle
    //---------------------------------------------------------------------
    
    

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = true
        
        self.toggleNewsLetterOption(UIButton())
        self.containerView.layer.cornerRadius = 4.0
    }
    
    
    override func viewWillAppear(animated: Bool)
    {
        
        
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - User Interaction
    //-----------------------------------------------------------------

    
    @IBAction func showDOBPicker(sender: AnyObject) {
        
        DatePickerDialog().show("Choose Date of birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .Date) {
            (date) -> Void in
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            self.dobSelected = true
            self.dobLabel.text = "\(dateFormatter.stringFromDate(date))"
            
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Day , .Month , .Year], fromDate: date)
            
            self.dobDay = components.day
            self.dobMonth = components.month
            self.dobYear =  components.year
        }
        
    }
    
    @IBAction func selectMale(sender: UIButton) {
        self.maleRadioButton.selected = true
        self.femaleRadioButton.selected = false
        
        self.genderSelected = true
        self.gender = "M"
    }
    
    @IBAction func selectFemale(sender: UIButton) {
        self.femaleRadioButton.selected = true
        self.maleRadioButton.selected = false
        
        self.genderSelected = true
        self.gender = "F"
    }
    
    @IBAction func toggleNewsLetterOption(sender: UIButton) {
        if self.newsLetterButton.selected {
            self.newsLetterButton.selected = false
            self.newsLetterSelected = false
        } else {
            self.newsLetterButton.selected = true
            self.newsLetterSelected = true
        }
    }
    
    @IBAction func register(sender: UIButton) {
        
        let firstName = self.firstNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if firstName.characters.count == 0 {
            AppUtility.showToast("Enter First Name", view: self.view)
            return
        }
        
        let lastName = self.lastNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if lastName.characters.count == 0 {
            AppUtility.showToast("Enter Last Name", view: self.view)
            return
        }
        
        //        if self.dobSelected == false {
        //            self.showToast("Choose Date of birth")
        //            return
        //        }
        //
        //        if self.genderSelected == false {
        //            self.showToast("Choose gender")
        //            return
        //        }
        
        let email = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if email.characters.count == 0 {
            AppUtility.showToast("Enter E-Mail address", view: self.view)
            return
        }
        
        
        let phoneNumber = self.phoneNumberTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if phoneNumber.characters.count == 0 {
            AppUtility.showToast("Enter Phone Number", view: self.view)
            return
        }
        
        if self.isValidEmail(email) == false {
            AppUtility.showToast("Invalid E-Mail address", view: self.view)
            return
        }
        
        //        let company = self.companyTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        //        if company.characters.count == 0 {
        //            self.showToast("Enter company name")
        //            return
        //        }
        
        let password = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if password.characters.count == 0 {
            AppUtility.showToast("Enter password", view: self.view)
            return
        }
        
        let confirmedPassword = self.confirmPassTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if confirmedPassword.characters.count == 0 {
            AppUtility.showToast("Confirm your password", view: self.view)
            return
        }
        
        if password != confirmedPassword {
            AppUtility.showToast("Passwords don't match", view: self.view)
            return
        }
        
        self.proceedToRegister()
    }
    
    
    
    
    //---------------------------------------------------------------------
    //MARK: - Utility Methods
    //---------------------------------------------------------------------
    
    
    func isValidEmail(testEmail:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testEmail)
    }
    
    func proceedToRegister()
    {
        var params = [String: AnyObject]()
        params["FirstName"] = self.firstNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["LastName"] = self.lastNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["DateOfBirthDay"] = self.dobDay
        params["DateOfBirthMonth"] = self.dobMonth
        params["DateOfBirthYear"] = self.dobYear
        params["Email"] = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["Phone"] = self.phoneNumberTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        params["Company"] = self.companyTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["Newsletter"] = self.newsLetterSelected ? "true":"false"
        params["Gender"] = self.gender
        params["Password"] = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["ConfirmPassword"] = self.confirmPassTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        

        self.registerCustomer(params)
    }
    
    
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    func registerCustomer(params : [String: AnyObject]) {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.POST, AppConstants.Api.Register, parameters: params, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
    
                        AppUtility.showToast("Registration Successful", view: self.view)
                        
                        AppUtility.backToRootView(self.navigationController!)
                    }
                else {
                    var errorMessage = ""
                    if let errorList = json["ErrorList"].arrayObject {
                    for error in errorList {
                        errorMessage += "\(error as! String)\n"
                      }
                    }
                    AppUtility.showToast(errorMessage, view: self.view)
                        
                }
              }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }

    
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder();
        return true;
    }

}
