//
//  SearchVC.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import MBProgressHUD


class SearchVC: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate{

    
    @IBOutlet weak var search_bar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    var productsInfoModel = ProductsInfoModel?()
    
    //---------------------------------------------------------------------
    //MARK: - ViewController LifeCycle
    //---------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.registerNib(UINib(nibName: "HomeCategoryCollectionCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "HomeCategoryCollectionCell")

        
        let textFieldInsideSearchBar = search_bar.valueForKey("searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.whiteColor()
        
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.valueForKey("placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.whiteColor()
        
        self.search_bar.tintColor = UIColor.whiteColor()

    }

    override func viewWillAppear(animated: Bool) {
        
        appDelegate.statusBar.hidden = true
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        appDelegate.statusBar.hidden = false
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let bound = UIScreen.mainScreen().bounds
        let hr: CGFloat = 256/190
        let w: CGFloat = (bound.width/2)-12
        let h: CGFloat = hr*w
        return CGSize(width: ceil(w),height: ceil(h))
        //return CGSizeMake(collectionView.bounds.size.width/2-15, 236)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if let products = productsInfoModel?.products {
           return products.count
        }
        
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell : HomeCategoryCollectionCell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeCategoryCollectionCell", forIndexPath: indexPath) as! HomeCategoryCollectionCell
        
        let imageUrl = productsInfoModel!.products[indexPath.item].DefaultPictureModel!.ImageUrl

        cell.productImage!.sd_setImageWithURL(NSURL(string:imageUrl!))
        cell.titleLabel.attributedText = AppUtility.getAttributeString(productsInfoModel!.products[indexPath.item].Name!, secondString: "\n\(productsInfoModel!.products[indexPath.item].ProductPrice!.Price!)", secondStrColor: UIColor(red: 252/255.0, green: 113/255.0, blue: 0/255.0, alpha: 1.0))

        return cell
    }
    
    // MARK: UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {

        
    }


    //-----------------------------------------------------------------
    // MARK: - User Interaction
    //-----------------------------------------------------------------
    
    
    @IBAction func backButtonAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    //-----------------------------------------------------------------
    // MARK: UISearchBarDelegate
    //-----------------------------------------------------------------
    
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar)
    {
        self.search_bar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked( searchBar: UISearchBar)
    {
        self.search_bar.text = searchBar.text
        self.search_bar.resignFirstResponder()
        self.getSearchResult()
        
    }

    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    func getSearchResult() {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.POST, AppConstants.Api.Search, parameters:["q":search_bar.text!], headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    //if json["StatusCode"].intValue == 200 {
                        if let list = Mapper<ProductsInfoModel>().map(json.dictionaryObject) {
                            
                            self.productsInfoModel = list
                            self.collectionView.reloadData()
                        }
                        
                    //}
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }

}
