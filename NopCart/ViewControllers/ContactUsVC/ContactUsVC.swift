//
//  ContactUsVC.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import MBProgressHUD
import DropDown
import MessageUI
class ContactUsVC: BaseVC , MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, UIAlertViewDelegate{

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    let mailRecipient = "razib@brainstation-23.com"
    let mailSubject = "Enquiry"
    let mailBody = ""
    let textMessageRecipient = "+8801912055164"
    let textMessageBody = ""
    
    let phoneNumber = "+8801912055164"
    
    let skypeId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = false

        self.containerView.layer.cornerRadius = 4.0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func emailBtnAct(sender: AnyObject) {
        
        if MFMailComposeViewController.canSendMail() {
            let mailComposeViewController = configuredMailComposeViewController()
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients([self.mailRecipient])
        mailComposerVC.setSubject(self.mailSubject)
        mailComposerVC.setMessageBody(self.mailBody, isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
//        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
//        
//        sendMailErrorAlert.show()
        
        let alertController = UIAlertController(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action:UIAlertAction!) in
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        presentViewController(alertController, animated: true, completion: nil);

    }
    
    @IBAction func smsBtnAct(sender: AnyObject) {
        
        if (MFMessageComposeViewController.canSendText()) {
            let messageComposeVC = configuredMessageComposeViewController()
            self.presentViewController(messageComposeVC, animated: true, completion: nil)
        } else {
            self.showSendSMSErrorAlert()
        }
    }
    
    func configuredMessageComposeViewController() -> MFMessageComposeViewController {
        let messageComposeVC = MFMessageComposeViewController()
        messageComposeVC.messageComposeDelegate = self
        
        messageComposeVC.recipients = [self.textMessageRecipient]
        messageComposeVC.body = self.textMessageBody
        
        return messageComposeVC
    }
    
    func showSendSMSErrorAlert() {
        //let errorAlert = UIAlertView(title: "Cannot Send Text Message", message: "Your device is not able to send text messages.", delegate: self, cancelButtonTitle: "OK")
        //errorAlert.show()
        
        let alertController = UIAlertController(title: "Cannot Send Text Message", message: "Your device is not able to send text messages.", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action:UIAlertAction!) in
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        presentViewController(alertController, animated: true, completion: nil);
        
        
    }
    
    @IBAction func phoneCallBtnAct(sender: AnyObject) {
        let phoneUrl = NSURL(string: "telprompt:\(self.phoneNumber)")
        
        if UIApplication.sharedApplication().canOpenURL(phoneUrl!)
        {
            UIApplication.sharedApplication().openURL(phoneUrl!)
        }
        else
        {
            //self.showPhoneCallErrorAlert()
        }
    }
    
//    func showPhoneCallErrorAlert() {
//        let errorAlert = UIAlertView(title: "Cannot Make Phone Call", message: "Your device is not able to make phone calls.", delegate: self, cancelButtonTitle: "OK")
//        errorAlert.show()
//    }
    
    @IBAction func skypeBtnAct(sender: AnyObject) {
        
        let skypeInstalledUrl = NSURL(string: "skype:")
        let skypeCallUrl = NSURL(string: "skype://razib.bs23?call")
        let skypeInstallationUrl = NSURL(string: "https://itunes.apple.com/app/skype/id304878510?mt=8")
        
        let installed = UIApplication.sharedApplication().canOpenURL(skypeInstalledUrl!)
        if installed
        {
            UIApplication.sharedApplication().openURL(skypeCallUrl!)
        }
        else
        {
            UIApplication.sharedApplication().openURL(skypeInstallationUrl!)
        }
    }

    
    
    
    
    // MARK: MFMailComposeViewControllerDelegate Method
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        controller.dismissViewControllerAnimated(true, completion: nil)
        
        switch (result) {
        case MFMailComposeResultCancelled:
            AppUtility.showToast("Mail cancelled",view: self.view)
            break
        case MFMailComposeResultSaved:
            AppUtility.showToast("Mail saved", view: self.view)
            break
        case MFMailComposeResultSent:
            AppUtility.showToast("Mail sent", view: self.view)
            break
        case MFMailComposeResultFailed:
            AppUtility.showToast("Mail couldn't be sent. \(error!.localizedDescription)",view: self.view)
            break
        default:
            break
        }
    }
    
    // MARK: MFMessageComposeViewControllerDelegate Method
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        
        controller.dismissViewControllerAnimated(true, completion: nil)
        
        switch (result) {
        case MessageComposeResultCancelled:
            AppUtility.showToast("SMS cancelled",view: self.view)
            break
        case MessageComposeResultFailed:
            AppUtility.showToast("SMS couldn't be sent", view:  self.view)
            break
        case MessageComposeResultSent:
            AppUtility.showToast("SMS sent",view:  self.view)
            break
        default:
            break
        }
    }


}
