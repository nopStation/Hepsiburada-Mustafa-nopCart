//
//  ProductVC.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import MBProgressHUD
import MFSideMenu


class ProductVC: BaseVC , UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource , UITableViewDelegate, UITableViewDataSource{


    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var customViewTopConstrainsts: NSLayoutConstraint!
    
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortBtn: UIButton!
    @IBOutlet weak var tableViewHeightConstraints: NSLayoutConstraint!
    
    
    var manufacturerFlag = false
    var pagingIndex = NSInteger ()
    var categoryId : NSInteger!
    var categoryName : String!
    var gridsPerRow = NSInteger ()
    var gridsBtnSelectionCounter = NSInteger ()
    var sortedValue : String = "0"
    private var headerViewHeight:CGFloat = 88
    private var scrollInsentOffset:CGFloat = 88
    private var naviScrollOldConstant:CGFloat = 0
    private var naviScrollOldVal:CGFloat = 0
    var previousContentOffset = CGPoint()
    var hidingNavBarManager: HidingNavigationBarManager?
    
    //---------------------------------------------------------------------
    //MARK: - ViewController LifeCycle
    //---------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        
        gridsBtnSelectionCounter = 1
        gridsPerRow = 1
        
         self.collectionView.registerNib(UINib(nibName: "HomeCategoryCollectionCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "HomeCategoryCollectionCell")
        
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.tableFooterView = UIView(frame: CGRectZero)

        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(ProductVC.reloadProductsTableView(_:)), name:"com.notification.reloadProductsTableView", object: nil)


        
        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = false
        self.customNavBarView.frame = CGRectMake(0, 20, self.customView.frame.size.width, 44)
        self.customView.addSubview(self.customNavBarView)

        self.getProductInfo()
    }
    
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.reloadProductsTableView", object: nil)
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        appDelegate.slideMenuController.panMode = MFSideMenuPanModeNone
        self.customNavBarView.boolFlag = true
    }

    override func viewWillDisappear(animated: Bool) {
     
        appDelegate.slideMenuController.panMode = MFSideMenuPanModeDefault
        self.customNavBarView.boolFlag = false
    }
    
    //---------------------------------------------------------------------
    // MARK: UICollectionViewDataSource
    //---------------------------------------------------------------------
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let products = SessionManager.manager.productModel?.products {
        return products.count
        }
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell : HomeCategoryCollectionCell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeCategoryCollectionCell", forIndexPath: indexPath) as! HomeCategoryCollectionCell
        
        
        let product =  SessionManager.manager.productModel!.products[indexPath.item]
        let imageStr = product.DefaultPictureModel!.ImageUrl
        
        cell.productImage.sd_setImageWithURL(NSURL(string: imageStr!))
        
        cell.titleLabel.attributedText = AppUtility.getAttributeString((product.Name)!, secondString: "\n\(product.ProductPrice!.Price!)", secondStrColor: AppUtility.getPrimaryCellColor())
        
        
        //let tableWidth: CGFloat = self.myCollectionView.bounds.size.width/2
        
//        cell.transform = CGAffineTransformMakeTranslation(self.myCollectionView.bounds.size.width/2, 0)
//        
//        UIView.animateWithDuration(1.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: UIViewAnimationOptions.CurveEaseInOut, animations: ({
//            cell.transform = CGAffineTransformMakeTranslation(0, 0)
//            cell.bounds = CGRect(x: cell.bounds.origin.x, y: cell.bounds.origin.y, width: cell.bounds.size.width + 10, height: cell.bounds.size.height)
//            
//        }), completion: nil)
        
        
//        UIView.animateWithDuration(0.3) {
//          
//            cell.transform = CGAffineTransformMakeTranslation(0, 0)
//            cell.bounds = CGRect(x: cell.bounds.origin.x - 20, y: cell.bounds.origin.y, width: cell.bounds.size.width + 60, height: cell.bounds.size.height)
//
//            //cell.layer.transform = CATransform3DIdentity
//        }
        

//        UIView.animateWithDuration(0.2, delay: 0.2 * Double( indexPath.item ) , usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.TransitionFlipFromLeft, animations: {
//            
//            cell.transform = CGAffineTransformMakeTranslation(0, 0)
//            }, completion: nil)
//    
    
        return cell
    }
    

    
    //---------------------------------------------------------------------
    // MARK: UICollectionViewDelegate
    //---------------------------------------------------------------------
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        
        
        let product =  SessionManager.manager.productModel!.products[indexPath.item]
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let productDetails = storyBoard.instantiateViewControllerWithIdentifier("ProductDetailsVC") as? ProductDetailsVC
        productDetails!.productId  = product.Id
        self.navigationController!.pushViewController(productDetails!, animated: true)
        
        
        
    }
    
//    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
//        
//        let bound = UIScreen.mainScreen().bounds
//        let hr: CGFloat = 272/206
//        let w: CGFloat = (bound.width/CGFloat(gridsPerRow))-2
//        
//        let h: CGFloat
//        if gridsPerRow == 1 {
//         h = hr*w - CGFloat(100)
//        }else {
//         h  = hr*w
//        }
//        
//        return CGSize(width: ceil(w),height: ceil(h))
//        
//    }

    
    //-----------------------------------------------------------------
    // MARK: - UITableView Datasource
    //-----------------------------------------------------------------
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let productModel = SessionManager.manager.productModel {
          return productModel.AvailableSortOptions!.count
        }
          return 0
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
        
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")!
        cell.backgroundColor = UIColor.clearColor()
        cell.contentView.backgroundColor = UIColor.clearColor()
        
        let data = SessionManager.manager.productModel!.AvailableSortOptions![indexPath.row]
        let title = data.Text
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = title
        cell.textLabel?.textColor = UIColor.whiteColor()
        return cell
    }
    //-----------------------------------------------------------------
    // MARK: - UITableView Delegate
    //-----------------------------------------------------------------
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let data = SessionManager.manager.productModel!.AvailableSortOptions![indexPath.row]
         self.sortedValue = data.Value!
        
        sortBtn.selected = false
        self.tableViewHeightConstraints.constant = 0
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadProductsTableView", object:nil);
        
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - User Interaction
    //-----------------------------------------------------------------
    @IBAction func gridsBtnAction(sender: AnyObject) {
        
        sortBtn.selected = false
        self.tableViewHeightConstraints.constant = 0
        
        gridsBtnSelectionCounter += 1
        
        if gridsBtnSelectionCounter > 3 {
            gridsBtnSelectionCounter = 1
        }
        gridsPerRow = gridsBtnSelectionCounter
        //self.collectionView.reloadData()
        
        
        
        if gridsBtnSelectionCounter > 1 {
        
            let productsGridFlowLayout = ProductsGridFlowLayout ()
            productsGridFlowLayout.gridsPerRow =  CGFloat(gridsBtnSelectionCounter)
            UIView.animateWithDuration(0.4) { () -> Void in
                self.collectionView.collectionViewLayout.invalidateLayout()
                self.collectionView.setCollectionViewLayout(productsGridFlowLayout, animated: true)
            }
        }
        
        
        else if gridsBtnSelectionCounter == 1  {
    
            let productsListFlowLayout = ProductsListFlowLayout ()
            
            UIView.animateWithDuration(0.4) { () -> Void in
            self.collectionView.collectionViewLayout.invalidateLayout()
            self.collectionView.setCollectionViewLayout(productsListFlowLayout, animated: true)
    
        }
    
      }
    }
    
    func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
    
    @IBAction func searchBtnAction(sender: AnyObject) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let searchView = storyBoard.instantiateViewControllerWithIdentifier("SearchVC") as? SearchVC
        self.navigationController?.pushViewController(searchView!, animated: true)
        
        
    }

    @IBAction func filterBtnAction(sender: AnyObject) {
        
        sortBtn.selected = false
        self.tableViewHeightConstraints.constant = 0
        
        appDelegate.slideMenuController.toggleRightSideMenuCompletion(nil)
    }
    
    @IBAction func sortBtnAction(sender: AnyObject) {
        
        if sortBtn.selected {
            sortBtn.selected = false
            
            UIView.animateWithDuration(0.25,
                                       delay: 0.0,
                                       options: UIViewAnimationOptions.CurveEaseIn,
                                       animations: { () -> Void in
                                       self.tableViewHeightConstraints.constant = 0
            }) { (finished) -> Void in
                
            }
        }else {
            sortBtn.selected = true
            
            self.customNavBarView.navigationMenuButton.selected = false
            UIView.animateWithDuration(0.25,
                                       delay: 0.0,
                                       options: UIViewAnimationOptions.CurveEaseIn,
                                       animations: { () -> Void in
                                        self.tableViewHeightConstraints.constant = self.view.frame.height - 108
                                        self.tableView.reloadData()
            }) { (finished) -> Void in
                
            }
        }
    }
    
    

    //-----------------------------------------------------------------
    // MARK: - UIScrollView Delegate
    //-----------------------------------------------------------------
    
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        
        
        if scrollView == self.collectionView {
        
        let offsetY = scrollView.contentOffset.y
        if scrollView.contentSize.height < offsetY { return }
        if offsetY < 0 && self.customViewTopConstrainsts.constant < 0 {
            self.customViewTopConstrainsts.constant = 0
            return
        }
        if offsetY >= 0 {
            var constant = naviScrollOldConstant - (offsetY - naviScrollOldVal)
            
            if constant < -200 { constant = -200 }
            if constant > 0   { constant =  0  }
            
            
            self.customViewTopConstrainsts.constant = max(constant, 0-headerViewHeight)
            
            //scrollView.contentInset.top          = max(constant, 1-scrollInsentOffset) + scrollInsentOffset
            //scrollView.scrollIndicatorInsets.top = max(constant, 1-scrollInsentOffset) + scrollInsentOffset
            
            naviScrollOldVal      = offsetY
            naviScrollOldConstant = constant
         }
      }
    }
    
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        print("scrollViewWillEndDragging \(velocity.y)")
        print("scrollViewWillEndDragging \(targetContentOffset.memory.y)")
        
        
     if scrollView == self.collectionView {
        
        if velocity.y == 0 {
            
            let offsetY = scrollView.contentOffset.y
            if scrollView.contentSize.height < offsetY { return }
            if offsetY >= 0 {
                
                let constant = self.customViewTopConstrainsts.constant
                print(constant)
                if constant  < -headerViewHeight { return }
                if constant >= 0 { return }
                
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.customViewTopConstrainsts.constant = constant < -self.headerViewHeight/2 ? -self.headerViewHeight : 0
                    self.view.layoutIfNeeded()
                });
                
            }
        }
      }
    }

    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
                let offset = scrollView.contentOffset
                let bounds = scrollView.bounds
                let size = scrollView.contentSize
                let inset = scrollView.contentInset
                let y = Int (offset.y + bounds.size.height - inset.bottom)
                let h = Int(size.height);
                NSLog("offset: %f", offset.y);
        
                let reload_distance = 20 ;
        
                // if offset.y > 10 {
                
        if(offset.y > 0 &&  y > (h+reload_distance) && scrollView == self.collectionView ) {

        //if ((scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height)  && scrollView == collectionView){
          if self.pagingIndex < SessionManager.manager.productModel!.TotalPages {
            
                AppUtility.showLoading(self.view)
                var paramDic = [String:AnyObject]()
                self.pagingIndex += 1
                paramDic["pagenumber"] = self.pagingIndex
            
                if let minPrice = SessionManager.manager.productModel?.PriceRange?.From, let maxPrice = SessionManager.manager.productModel?.PriceRange?.To
                {
                    paramDic["price"] = String(format: "%f-%f", minPrice, maxPrice)
                }
            
               if !(self.sortedValue == "0") {
                   paramDic["orderBy"] = self.sortedValue
               }
            
                if (SessionManager.manager.productModel?.AlreadyFilteredItems!.count > 0)
                {
                    var specsString = "\(SessionManager.manager.productModel?.AlreadyFilteredItems![0].FilterId)"
                    for var i = 1; i < SessionManager.manager.productModel?.AlreadyFilteredItems!.count ; i += 1
                    {
                        specsString += ",\(SessionManager.manager.productModel?.AlreadyFilteredItems![i].FilterId)"
                    }
                    
                    paramDic["specs"] = specsString
                }

               self.getMoreProductInfo(self.manufacturerFlag, categoryId: self.categoryId, paramDic: paramDic)
            }
        }
    }
    
    
    
    //-----------------------------------------------------------------
    // MARK: - Utilities Methods
    //-----------------------------------------------------------------
    func reloadProductsTableView(notification: NSNotification) {
        
        
        AppUtility.showLoading(self.view)
        var paramDic = [String:AnyObject]()
        self.pagingIndex = 1
        paramDic["pagenumber"] = 1
        
        if let minPrice = SessionManager.manager.productModel?.PriceRange?.From, let maxPrice = SessionManager.manager.productModel?.PriceRange?.To
        {
            paramDic["price"] = String(format: "%f-%f", minPrice, maxPrice)
        }
        
        if !(self.sortedValue == "0") {
            paramDic["orderBy"] = self.sortedValue
        }
        
        if (SessionManager.manager.productModel?.AlreadyFilteredItems!.count > 0)
        {
            var specsString = "\(SessionManager.manager.productModel?.AlreadyFilteredItems![0].FilterId)"
            for var i = 1; i < SessionManager.manager.productModel?.AlreadyFilteredItems!.count ; i += 1
            {
                specsString += ",\(SessionManager.manager.productModel?.AlreadyFilteredItems![i].FilterId)"
            }
            
            paramDic["specs"] = specsString
        }
        
        self.updateFilteredProducts(self.manufacturerFlag, categoryId: self.categoryId, paramDic: paramDic)
        
    }

    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    func updateFilteredProducts(manufacturerFlag: Bool ,categoryId: NSInteger,  paramDic :[String:AnyObject]) {
        
        
        AppUtility.showLoading(self.view)
        
        //pagingIndex = 1
        //var paramDic = [String:AnyObject]()
        //paramDic["pagenumber"] = 1
        
        
        var urlSubString = "Category"
        
        if manufacturerFlag == true  //Manufacturer will be True when We click from HomePage Manufacture Cell
        {
            urlSubString = "Manufacturer"
        }
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@/%d", urlSubString, categoryId) as String, parameters: paramDic, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if let list = Mapper<ProductsInfoModel>().map(json.dictionaryObject) {
                            
                            SessionManager.manager.productModel! = list
                            self.collectionView.reloadData()
                            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadFilterTableView", object: 1);
                        }
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }

        
    }
    
    
    
    func getMoreProductInfo(manufacturerFlag: Bool ,categoryId: NSInteger,  paramDic :[String:AnyObject]) {
        
        AppUtility.showLoading(self.view)
        
        //pagingIndex = 1
        //var paramDic = [String:AnyObject]()
        //paramDic["pagenumber"] = 1
        
        
        var urlSubString = "Category"
        
        if manufacturerFlag == true  //Manufacturer will be True when We click from HomePage Manufacture Cell
        {
            urlSubString = "Manufacturer"
        }
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@/%d", urlSubString, categoryId) as String, parameters: paramDic, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if let list = Mapper<ProductsInfoModel>().map(json.dictionaryObject) {
                            let products = list.products
                            SessionManager.manager.productModel!.products.appendContentsOf(products)
                            
                            self.collectionView.reloadData()
                            //self.animateTable()
                            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadFilterTableView", object: 1);
                        }
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }
    
    
    func getProductInfo() {
        
        AppUtility.showLoading(self.view)
        
        pagingIndex = 1
        var paramDic = [String:AnyObject]()
        paramDic["pagenumber"] = 1
        
        
        var urlSubString = "Category"
        
        if manufacturerFlag == true  //Manufacturer will be True when We click from HomePage Manufacture Cell
         {
            urlSubString = "Manufacturer"
         }
        
        categoryId = 1;
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@/%d", urlSubString, categoryId) as String, parameters: paramDic, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        if let list = Mapper<ProductsInfoModel>().map(json.dictionaryObject) {
                         
                         SessionManager.manager.productModel = list
                            
                         self.gridsBtnAction(self)
                         NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadFilterTableView", object: 1);
                            
                        }
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }
}
