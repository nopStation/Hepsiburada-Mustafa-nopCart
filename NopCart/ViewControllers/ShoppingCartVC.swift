//
//  ShoppingCartVC.swift
//  NopCart
//
//  Created by BS-125 on 8/9/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import MBProgressHUD
import DropDown

class ShoppingCartVC: BaseVC , UITextFieldDelegate {

    @IBOutlet weak var attributsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var itemTableViewHeightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var attributsTableView: UITableView!
    
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var shippingLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var couponTextFeild: UITextField!
    @IBOutlet weak var applyCouponBtn: UIButton!
    
    var shoppingCartModel = ShoppingCartModel?()
    var dictionaryArray     = NSMutableArray ()
    var textFieldDic        = [Int:AnyObject]()
    var totalAttributsTableViewHeight = NSInteger ()
    var dropDownArray       = [AnyObject]()
    var popViewController : PopUpViewControllerSwift!
    var checkOutGuestflag = false
    //-----------------------------------------------------------------
    // MARK: - ViewController LifeCycle
    //-----------------------------------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(ShoppingCartVC.proceedToLogin(_:)), name:"com.notification.proceedToLogin", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(ShoppingCartVC.proceedToRegister(_:)), name:"com.notification.proceedToRegister", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(ShoppingCartVC.proceedToCheckOut(_:)), name:"com.notification.proceedToCheckOut", object: nil)

        
        
        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = false
        
        self.getShoppingCartInfo()
    }

    
    deinit {
        if self.view != nil {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.proceedToLogin", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.proceedToRegister", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.proceedToCheckOut", object: nil)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    
    //-----------------------------------------------------------------
    // MARK: - Notofication Action
    //-----------------------------------------------------------------
    

    
    func proceedToLogin(notification: NSNotification) {
        
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let signInViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("SignInVC") as! SignInVC
        self.navigationController?.pushViewController(signInViewC, animated: true)
        
    }
    
    func proceedToRegister(notification: NSNotification) {
        
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let registerViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(registerViewC, animated: true)
        
        
    }
    
    func proceedToCheckOut(notification: NSNotification) {
        
        let finalDicArray = NSMutableArray ()
        for key in self.textFieldDic.keys {
            let parameters:Dictionary<String,AnyObject> = ["value": self.textFieldDic[key]!,
                                                           "key":String(format: "checkout_attribute_%d",(self.shoppingCartModel?.CheckoutAttributes![key].Id)!)]
            finalDicArray.addObject(parameters)
            
        }
        finalDicArray.addObjectsFromArray(self.dictionaryArray as [AnyObject])
        self.applyCheckoutAttributs(finalDicArray)

        
    }

    
    
    
    
    //-----------------------------------------------------------------
    // MARK: - User Interaction
    //-----------------------------------------------------------------

    
    @IBAction func proceedToCheckOutBtnAct(sender: AnyObject) {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            print(token)
            let finalDicArray = NSMutableArray ()
            for key in self.textFieldDic.keys {
                let parameters:Dictionary<String,AnyObject> = ["value": self.textFieldDic[key]!,
                                                               "key":String(format: "checkout_attribute_%d",(self.shoppingCartModel?.CheckoutAttributes![key].Id)!)]
                finalDicArray.addObject(parameters)
                
            }
            
            finalDicArray.addObjectsFromArray(self.dictionaryArray as [AnyObject])
            
            self.applyCheckoutAttributs(finalDicArray)

        }else {
            
            let bundle = NSBundle(forClass: PopUpViewControllerSwift.self)
            self.popViewController = PopUpViewControllerSwift(nibName: "PopUpViewController_iPhone6", bundle: bundle)
            //self.popViewController.checkOutGuestflag = self.checkOutGuestflag
            self.popViewController.showInView(self.view, withImage: UIImage(named: "typpzDemo"), withMessage: "You just triggered a great popup window", animated: true, checkOutGuestflag: self.checkOutGuestflag)
            
            
            
        }
        
        
    }

    
    
    @IBAction func couponBtnAction(sender: AnyObject) {
        
        
        
        //if self.couponTextFeild.text?.characters.count > 0 {
         
            if applyCouponBtn.titleLabel?.text == "Apply Coupon" && self.couponTextFeild.text?.characters.count > 0{
            
             self.applyDiscountCoupon(self.couponTextFeild.text!)
                
            }else if applyCouponBtn.titleLabel?.text == "Remove Coupon" {
                
                self.removeDiscountCoupon()
                
            }
    }
    
    
    
    
    func textFieldDidChanged(textField:UITextField ){
        
        print(textField.tag)
        print(textField.text)
        self.textFieldDic[textField.tag] = textField.text!
        
        
    }
    
    
    func radioListButtonAction(sender:UIButton!)
    {
        
        var values = sender.accessibilityValue?.componentsSeparatedByString("/")
        let rowIndex:Int? = Int(values![0])
        let itemIndex:Int? = Int(values![1])
        
        
        let productAttributes = (self.shoppingCartModel?.CheckoutAttributes![rowIndex!])! as Checkoutattributes
        
        for i in 0  ..< productAttributes.values!.count  {
            
            productAttributes.values![i].IsPreSelected = false
        }
        
        productAttributes.values![itemIndex!].IsPreSelected = true
        let indexPath = NSIndexPath(forRow: rowIndex!, inSection: 0)
        self.attributsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        
        self.updatePriceLabel(productAttributes, itemIndex: itemIndex!)
        
        
    }
    
    
    
    func showDropDown(sender: UIButton) {
        
        
        let innerDropDown = dropDownArray[sender.tag] as! DropDown
        
        let pointInTable: CGPoint = sender.convertPoint(sender.bounds.origin, toView: self.attributsTableView)
        let cellIndexPath = self.attributsTableView.indexPathForRowAtPoint(pointInTable)
        let cell = self.attributsTableView.cellForRowAtIndexPath(cellIndexPath!) as! DropdownListTableViewCell
        
        
        innerDropDown.anchorView = cell
        innerDropDown.bottomOffset = CGPoint(x: cell.frame.origin.x, y:cell.frame.origin.y)
        
        if innerDropDown.hidden {
            innerDropDown.show()
        } else {
            innerDropDown.hide()
        }
        
        innerDropDown.selectionAction = { (index, item) in
            
            cell.titleLbl.text = item
            
            let productAttributes = (self.shoppingCartModel?.CheckoutAttributes![sender.tag])! as Checkoutattributes
            self.updatePriceLabel(productAttributes, itemIndex: index)
        }
    }

    
    func checkMarkbuttonAction(sender:UIButton!)
    {
        
        var values = sender.accessibilityValue?.componentsSeparatedByString("/")
        //let rowIndex:Int? = Int(values![0])
        let itemIndex:Int? = Int(values![1])
        
        
        if  sender.selected {
            sender.selected = false
            
        }
        else {
            sender.selected = true
            
        }
        
        let productAttributes = (self.shoppingCartModel?.CheckoutAttributes![sender.tag])! as Checkoutattributes
        self.updatePriceLabelForCheckList(productAttributes, itemIndex: itemIndex!)
        
        
    }
    
    
    func updatePriceLabelForCheckList(productattributes : Checkoutattributes , itemIndex: NSInteger){
        
        if Int(dictionaryArray.count as NSInteger) != 0 {
            
            var indexValueCheckflag = NSInteger ()
            indexValueCheckflag = 0
            
            for j in 0  ..< dictionaryArray.count  {
                let dic  = dictionaryArray.objectAtIndex(j)
                
                let id  = dic.valueForKey("value") as! NSInteger
                if Int(productattributes.values![itemIndex].Id) == Int(id)  {
                    indexValueCheckflag = 1
                    self.dictionaryArray.removeObjectAtIndex(j)
                    break
                }
            }
            
            if indexValueCheckflag == 0{
                
                let parameters:Dictionary<String,AnyObject> = ["value": productattributes.values![itemIndex].Id,
                                                               "key":String(format: "checkout_attribute_%d",productattributes.Id)]
                
                self.dictionaryArray.addObject(parameters)
                
                
            }
            
        }
        else {
            
            let parameters:Dictionary<String,AnyObject> = ["value": productattributes.values![itemIndex].Id,
                                                           "key":String(format: "checkout_attribute_%d",productattributes.Id)]
            self.dictionaryArray.addObject(parameters)
            
        }
        
        
        
       //self.getProductPrice(self.dictionaryArray)
    }
    
    func updatePriceLabel(productattributes : Checkoutattributes , itemIndex: NSInteger){
        
        if Int(dictionaryArray.count as NSInteger) != 0 {
            
            var indexValueCheckflag = NSInteger ()
            indexValueCheckflag = 0
            for i in 0  ..< productattributes.values!.count  {
                
                for j in 0  ..< dictionaryArray.count  {
                    let dic  = dictionaryArray.objectAtIndex(j)
                    
                    let id  = dic.valueForKey("value") as! NSInteger
                    if Int(productattributes.values![i].Id) == Int(id)  {
                        
                        indexValueCheckflag = 1
                        let parameters:Dictionary<String,AnyObject> = ["value": productattributes.values![itemIndex].Id,
                                                                       "key":String(format: "checkout_attribute_%d",productattributes.Id)]
                        self.dictionaryArray.replaceObjectAtIndex(j, withObject: parameters)
                        break
                    }
                }
            }
            
            if indexValueCheckflag == 0{
                let parameters:Dictionary<String,AnyObject> = ["value": productattributes.values![itemIndex].Id,
                                                               "key":String(format: "checkout_attribute_%d",productattributes.Id)]
                self.dictionaryArray.addObject(parameters)
                
                
            }
            
        }
        else {
            
            let parameters:Dictionary<String,AnyObject> = ["value": productattributes.values![itemIndex].Id,
                                                           "key":String(format: "checkout_attribute_%d",productattributes.Id)]
            self.dictionaryArray.addObject(parameters)
            
        }
        
        
        
        //self.getProductPrice(self.dictionaryArray)
    }
    
    
    
    func plusBtnAction(sender:UIButton!)
    {
        
        print(sender.tag)
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        print(indexPath)
        let cell  = self.itemsTableView.cellForRowAtIndexPath(indexPath) as! ItemsTableViewCell
        
        self.shoppingCartModel!.items![sender.tag].Quantity += 1
        
        cell.incrementLbl.text = String(format: "%d", self.shoppingCartModel!.items![sender.tag].Quantity)
        
        
        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartModel!.items![sender.tag].Quantity,
                                                       "key": String(format: "itemquantity%d",self.shoppingCartModel!.items![sender.tag].Id )]
        
        let dicArray = NSMutableArray ()
        
        dicArray.addObject(parameters)
        self.Delete_Update_IncrementCartInfo(dicArray)
        
        /*
        self.showLoading()
        
        aPIManager?.DeleteAndUpdate(dicArray, onSuccess: {
            
            updateCart in
            
            self.shoppingCartInfoArray = updateCart
            
            
            self.subTotalLbl.text = self.shoppingCartInfoArray[0].subTotal as? String
            self.shippingLbl.text = self.shoppingCartInfoArray[0].shipping as? String
            self.taxLbl.text      = self.shoppingCartInfoArray[0].tax as? String
            self.totalLbl.text    = self.shoppingCartInfoArray[0].orderTotal as? String

            self.updateShoppingCartCount()
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            //self.loadTotalOrder()
            
            
            },
                                    onError: {
                                        message in
                                        print(message)
                                        self.hideLoading()
                                        self.showToast(message)
                                        
        })*/
        
    }
    
    
    
    func deleteBtnAction(sender:UIButton!)
    {
        
        
        //self.showLoading()
        
        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartModel!.items![sender.tag].Id,
                                                       "key":"removefromcart"]
        let dicArray = NSMutableArray ()
        dicArray.addObject(parameters)
        self.Delete_Update_IncrementCartInfo(dicArray)
        
        /*
        aPIManager?.DeleteAndUpdate(dicArray, onSuccess: {
            
            updateCart in

            self.shoppingCartInfoArray = updateCart
            self.subTotalLbl.text = self.shoppingCartInfoArray[0].subTotal as? String
            self.shippingLbl.text = self.shoppingCartInfoArray[0].shipping as? String
            self.taxLbl.text      = self.shoppingCartInfoArray[0].tax as? String
            self.totalLbl.text    = self.shoppingCartInfoArray[0].orderTotal as? String
            
            
            self.updateShoppingCartCount()

            
            let pointInTable: CGPoint = sender.convertPoint(sender.bounds.origin, toView: self.contentTableView)
            let cellIndexPath = self.contentTableView.indexPathForRowAtPoint(pointInTable)
            self.contentTableView.deleteRowsAtIndexPaths([cellIndexPath!], withRowAnimation: UITableViewRowAnimation.Automatic)
            self.contentTableViewHeightConstant.constant = CGFloat(self.shoppingCartInfoArray[0].Items.count)  * 150.0
            self.contentTableView.performSelector("reloadData", withObject: nil, afterDelay: 0.5)
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            
            if self.shoppingCartInfoArray[0].Items.count == 0 {
                
                self.navigationController?.popViewControllerAnimated(true)
            }
            
            
            },
                                    onError: {
                                        message in
                                        print(message)
                                        self.hideLoading()
                                        self.showToast(message)
                                        
        })*/
    }
    
    func minusBtnAction(sender:UIButton!)
    {
        if self.shoppingCartModel!.items![sender.tag].Quantity > 1 {
            
            print(sender.tag)
            let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
            
            print(indexPath)
            let cell  = self.itemsTableView.cellForRowAtIndexPath(indexPath) as! ItemsTableViewCell
            self.shoppingCartModel!.items![sender.tag].Quantity -= 1
            cell.incrementLbl.text = String(format: "%d", self.shoppingCartModel!.items![sender.tag].Quantity)
            
            
            
            let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartModel!.items![sender.tag].Quantity,
                                                           "key": String(format: "itemquantity%d",self.shoppingCartModel!.items![sender.tag].Id )]
            
            let dicArray = NSMutableArray ()
            
            dicArray.addObject(parameters)
            
            self.Delete_Update_IncrementCartInfo(dicArray)
            /*
            aPIManager?.DeleteAndUpdate(dicArray, onSuccess: {
                
                updateCart in
                
                self.shoppingCartInfoArray = updateCart
                
                
                self.subTotalLbl.text = self.shoppingCartInfoArray[0].subTotal as? String
                self.shippingLbl.text = self.shoppingCartInfoArray[0].shipping as? String
                self.taxLbl.text      = self.shoppingCartInfoArray[0].tax as? String
                self.totalLbl.text    = self.shoppingCartInfoArray[0].orderTotal as? String
                
                
                
                self.updateShoppingCartCount()
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)

                },
                                        onError: {
                                            message in
                                            print(message)
                                            self.hideLoading()
                                            self.showToast(message)
            })*/
        }
    }
    
    
    
    
    
    //-----------------------------------------------------------------
    // MARK: - Utility Method
    //-----------------------------------------------------------------

    
    func setUpAttributsTableViewContent () {
        
        
        self.totalAttributsTableViewHeight = 0
        self.attributsViewHeightConstraint.constant = 0
        
        for var i = 0 ; i < self.shoppingCartModel?.CheckoutAttributes!.count ; i += 1 {
            
            //self.attributsViewHeightConstraint.constant = 8
            self.totalAttributsTableViewHeight = 8
            let productAttributes = (self.shoppingCartModel?.CheckoutAttributes![i])! as Checkoutattributes
            
            if productAttributes.AttributeControlType == 1
            {
                
                self.totalAttributsTableViewHeight = self.totalAttributsTableViewHeight + 65
                
                let dropDown = DropDown()
                dropDown.tag = i
                for j in 0  ..< productAttributes.values!.count  {
                    
                    dropDown.dataSource.append(productAttributes.values![j].Name!)
                    
                    if productAttributes.values![j].IsPreSelected {
                        
                        let parameters:Dictionary<String,AnyObject> = ["value": productAttributes.values![j].Id,
                                                                       "key":String(format: "checkout_attribute_%d",productAttributes.Id)]
                        
                        print("%@",parameters);
                        self.dictionaryArray.addObject(parameters)
                        break
                    }
                }
                
                self.dropDownArray.append(dropDown)
   
            }
            else if productAttributes.AttributeControlType == 2{
                self.totalAttributsTableViewHeight = self.totalAttributsTableViewHeight + 85
                for j in 0  ..< productAttributes.values!.count  {
                    if productAttributes.values![j].IsPreSelected {
                        
                        let parameters:Dictionary<String,AnyObject> = ["value": productAttributes.values![j].Id,
                                                                       "key":String(format: "checkout_attribute_%d",productAttributes.Id)]
                        self.dictionaryArray.addObject(parameters)
                        break
                        
                    }
                }
                
            }
            else if productAttributes.AttributeControlType == 3{
                
                self.totalAttributsTableViewHeight = self.totalAttributsTableViewHeight + 31 + 44 * productAttributes.values!.count
                
            }
            else if productAttributes.AttributeControlType == 4{
                self.totalAttributsTableViewHeight = self.totalAttributsTableViewHeight + 65
                if let defaultValue = productAttributes.DefaultValue   {
                    
                    self.textFieldDic[i] = defaultValue
                }
            }
        }
        self.attributsViewHeightConstraint.constant = CGFloat(self.totalAttributsTableViewHeight)
        
    }
    
    
    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    
    func applyCheckoutAttributs(params: NSMutableArray) {
        
        
        AppUtility.showLoading(self.view)
        
        let cartInfoUpdate = String(format: "%@%@",SessionManager.manager.baseURL,AppConstants.Api.ApplyCheckoutAttribut)
        let manager = RequestManager.manager
        let request = NSMutableURLRequest(URL: NSURL(string: cartInfoUpdate)!)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = AppUtility.getHeaderDic()
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(params, options: [])
        
        manager.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .Success:
                    print("Success")
                    
                    AppUtility.hideLoading(self.view)
                    
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        
                        print(json)
                        if json["StatusCode"].intValue == 200 {
                            
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let checkOutView = mainStoryBoard.instantiateViewControllerWithIdentifier("CheckOutViewController") as! CheckOutViewController
                            //self.presentViewController(checkOutView, animated: true, completion: nil)
                            self.navigationController?.pushViewController(checkOutView, animated: true)
                            
                            
                        }else {
                            var errorMessage = ""
                            if let errorList = json["ErrorList"].arrayObject {
                                for error in errorList {
                                    errorMessage += "\(error as! String)\n"
                                }
                            }
                            AppUtility.showToast(errorMessage, view: self.view)
                        }
                    }
                    
                case .Failure:
                    print("Failure")
                    print("Error:\(response.result.error?.localizedDescription)")
                    AppUtility.showToast("\(response.result.error?.localizedDescription)", view: self.view)
                }
        }
        
        
        
    }
    
    func Delete_Update_IncrementCartInfo(params: NSMutableArray) {
    
    
        AppUtility.showLoading(self.view)
        
        let cartInfoUpdate = String(format: "%@%@",SessionManager.manager.baseURL,AppConstants.Api.ShopppingCart_Delete_Update_Increment)
        let manager = RequestManager.manager
        let request = NSMutableURLRequest(URL: NSURL(string: cartInfoUpdate)!)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = AppUtility.getHeaderDic()
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(params, options: [])
        
        manager.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .Success:
                    print("Success")
                    
                    AppUtility.hideLoading(self.view)
                    
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        
                        print(json)
                        if json["StatusCode"].intValue == 200 {
                            
                            
                        if let list = Mapper<ShoppingCartModel>().map(json.dictionaryObject) {
                            self.shoppingCartModel = list
                            
                            self.subTotalLbl.text = self.shoppingCartModel?.OrderTotalResponseModel?.SubTotal
                            self.shippingLbl.text = self.shoppingCartModel?.OrderTotalResponseModel?.Shipping
                            self.taxLbl.text = self.shoppingCartModel?.OrderTotalResponseModel?.Tax
                            self.totalLbl.text = self.shoppingCartModel?.OrderTotalResponseModel?.OrderTotal
                            
                            
                            appDelegate.cartView.cartLabel.text = "\(self.shoppingCartModel!.Count)"
                            
                            
                            
                            //self.setUpAttributsTableViewContent()
                            if let count = self.shoppingCartModel?.items!.count {
                                
                                self.itemTableViewHeightConstraints.constant = CGFloat (count) * 150.0
                            }
                            self.itemsTableView.reloadData()
                            self.attributsTableView.reloadData()
                        }
                         
                            
                        }else {
                            var errorMessage = ""
                            if let errorList = json["ErrorList"].arrayObject {
                                for error in errorList {
                                    errorMessage += "\(error as! String)\n"
                                }
                            }
                            AppUtility.showToast(errorMessage, view: self.view)
                        }
                    }
                    
                case .Failure:
                    print("Failure")
                    print("Error:\(response.result.error?.localizedDescription)")
                    AppUtility.showToast("\(response.result.error?.localizedDescription)", view: self.view)
                }
        }


    
    }
    
    
    func checkoutForGuest() {
        
       
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@%@", SessionManager.manager.baseURL,AppConstants.Api.CheckoutForGuest ) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if json["Data"].int == 1 {
                            self.checkOutGuestflag = true
                        }else {
                           
                            self.checkOutGuestflag = false
                        }
                        
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }

        
    }
    
    func removeDiscountCoupon () {

        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@%@/%@",SessionManager.manager.baseURL,AppConstants.Api.ShoppingCart, AppConstants.Api.RemoveDiscountCoupon ) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        self.subTotalLbl.text = json["OrderTotalResponseModel"]["SubTotal"].string
                        self.shippingLbl.text = json["OrderTotalResponseModel"]["Shipping"].string
                        self.taxLbl.text      = json["OrderTotalResponseModel"]["Tax"].string
                        self.totalLbl.text    = json["OrderTotalResponseModel"]["OrderTotal"].string
                        
                        self.applyCouponBtn.setTitle("Apply Coupon", forState: UIControlState.Normal)
                        
                        AppUtility.showToast("Your Coupon Deleted successfully", view: self.view)
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }

        
        
    }
    
    
    func applyDiscountCoupon (couponText : NSString) {
        
        
        AppUtility.showLoading(self.view)

        let manager = RequestManager.manager
        manager.request(.POST, String(format: "%@%@/%@", SessionManager.manager.baseURL,AppConstants.Api.ShoppingCart, AppConstants.Api.DiscountCoupon ) as String, parameters: ["value":couponText], headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                    self.subTotalLbl.text = json["OrderTotalResponseModel"]["SubTotal"].string
                    self.shippingLbl.text = json["OrderTotalResponseModel"]["Shipping"].string
                    self.taxLbl.text      = json["OrderTotalResponseModel"]["Tax"].string
                    self.totalLbl.text    = json["OrderTotalResponseModel"]["OrderTotal"].string
                        
                     self.applyCouponBtn.setTitle("Remove Coupon", forState: UIControlState.Normal)
                        
                    AppUtility.showToast("Your Coupon Added successfully", view: self.view)
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }

        
    }
    
    
    func getShoppingCartInfo() {
        
        
        AppUtility.showLoading(self.view)

        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@%@", SessionManager.manager.baseURL, AppConstants.Api.ShoppingCart ) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if let list = Mapper<ShoppingCartModel>().map(json.dictionaryObject) {
                            
                            print("%@",list)
                            self.shoppingCartModel = list
                            
                            self.subTotalLbl.text = self.shoppingCartModel?.OrderTotalResponseModel?.SubTotal
                            self.shippingLbl.text = self.shoppingCartModel?.OrderTotalResponseModel?.Shipping
                            self.taxLbl.text = self.shoppingCartModel?.OrderTotalResponseModel?.Tax
                            self.totalLbl.text = self.shoppingCartModel?.OrderTotalResponseModel?.OrderTotal

                            
                            
                            self.setUpAttributsTableViewContent()
                            if let count = self.shoppingCartModel?.items!.count {
                                
                             self.itemTableViewHeightConstraints.constant = CGFloat (count) * 150.0
                            }
                            self.itemsTableView.reloadData()
                            self.attributsTableView.reloadData()
                            
                            self.checkoutForGuest()
                            
                        }
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
        
    }

    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        if tableView == attributsTableView {
        
        let productAttributes = (self.shoppingCartModel?.CheckoutAttributes![indexPath.row])! as Checkoutattributes
        var cell = CGFloat()
        
        if productAttributes.AttributeControlType == 1 {
            cell = 65
        }
        else if productAttributes.AttributeControlType == 2 {
            cell = 85
        }
        else if productAttributes.AttributeControlType == 3 {
            cell = 31 + 44 * CGFloat(productAttributes.values!.count)
        }else if productAttributes.AttributeControlType == 4 {
            cell = 65
        }
        
         return cell;
        }
        
        
        return 150
        
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == attributsTableView {
          if let count = self.shoppingCartModel?.CheckoutAttributes?.count {
            return count
        }
        }else if tableView ==  itemsTableView {
            if let count = self.shoppingCartModel?.items?.count {
                return count
            }
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if tableView == attributsTableView {
        
        let productAttributes = (self.shoppingCartModel?.CheckoutAttributes![indexPath.row])! as Checkoutattributes
        if productAttributes.AttributeControlType == 1 {
            
            var cell: DropdownListTableViewCell! = tableView.dequeueReusableCellWithIdentifier("DropdownListTableViewCell") as? DropdownListTableViewCell
            
            print(productAttributes.AttributeControlType)
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "DropdownListTableViewCell", bundle: nil), forCellReuseIdentifier: "DropdownListTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("DropdownListTableViewCell") as? DropdownListTableViewCell
            }
            
            print(cell.frame.origin.y)
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.dropDownBtn.tag = indexPath.row
            
            if productAttributes.IsRequired {
                
                let labelText = NSMutableAttributedString(string: productAttributes.Name!)
                labelText.appendAttributedString(NSAttributedString(string:"*"))
                let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                cell.nameLbl.attributedText = labelText
            }
            else
            {
                cell.nameLbl.text = productAttributes.Name! as String
            }
            
            for j in 0  ..< productAttributes.values!.count  {
                if productAttributes.values![j].IsPreSelected {
                    cell.titleLbl.text = productAttributes.values![j].Name
                    break
                    
                }
            }
            
            
            //            if self.dropDownMenuSequenceNumber == indexPath.row {
            //
            //                print(self.dropDownMenuSequenceNumber)
            //                cell.titleLbl.text = self.productDetailsArray[0].ProductAttributes[self.dropDownMenuSequenceNumber].Values[dropDownMenuSelectedIndex].Name as String
            //            }
            cell.dropDownBtn.addTarget(self, action: #selector(ProductDetailsVC.showDropDown(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            return cell
            
            
        }
        else if productAttributes.AttributeControlType == 2 {
            
            var cell: RadioListTableViewCell! = tableView.dequeueReusableCellWithIdentifier("RadioListTableViewCell") as? RadioListTableViewCell
            
            // if cell == nil {
            tableView.registerNib(UINib(nibName: "RadioListTableViewCell", bundle: nil), forCellReuseIdentifier: "RadioListTableViewCell")
            cell = tableView.dequeueReusableCellWithIdentifier("RadioListTableViewCell") as? RadioListTableViewCell
            // }
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            
            
            if productAttributes.IsRequired {
                
                let labelText = NSMutableAttributedString(string: productAttributes.Name!)
                labelText.appendAttributedString(NSAttributedString(string:"*"))
                let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                cell.titleLbl.attributedText = labelText
            }
            else
            {
                cell.titleLbl.text = productAttributes.Name! as String
            }
            
            
            
            cell.scrollViewForRadioList.contentSize = CGSize(width:productAttributes.values!.count * 150, height:44)
            var titleXPoint = NSInteger ()
            var buttonXPoint = NSInteger ()
            titleXPoint = 38
            buttonXPoint = 5
            
            
            for i in 0  ..< productAttributes.values!.count  {
                
                let button   = UIButton(type: UIButtonType.Custom) as UIButton
                button.frame = CGRectMake( CGFloat(buttonXPoint), 10, 30, 30)
                button.setImage(UIImage(named: "radio-selected.png"), forState: UIControlState.Selected)
                button.setImage(UIImage(named: "radio-unselected.png"), forState: UIControlState.Normal)
                button.accessibilityValue = String(format: "%d/%d",indexPath.row,i)
                button.tag = i
                button.addTarget(self, action: #selector(ProductDetailsVC.radioListButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                
                if productAttributes.values![i].IsPreSelected {
                    button.selected = true
                }
                else
                {
                    button.selected = false
                }
                
                
                cell.scrollViewForRadioList.addSubview(button)
                
                let label = UILabel(frame: CGRectMake(CGFloat(titleXPoint), 10, 100, 30))
                label.text = productAttributes.values![i].Name
                cell.scrollViewForRadioList.addSubview(label)
                buttonXPoint = buttonXPoint + 100 + 32
                titleXPoint =  buttonXPoint + 35
                
            }
            
            return cell
            
        }
            
        else if productAttributes.AttributeControlType == 3{
            
            
            
            var cell: CheckboxesTableViewCell! = tableView.dequeueReusableCellWithIdentifier("CheckboxesTableViewCell") as? CheckboxesTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "CheckboxesTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckboxesTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("CheckboxesTableViewCell") as? CheckboxesTableViewCell
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.checkBoxScrollView.contentSize = CGSize(width: 200, height: productAttributes.values!.count * 44 )
            var titleXPoint = NSInteger ()
            var buttonXPoint = NSInteger ()
            
            if productAttributes.IsRequired {
                
                let labelText = NSMutableAttributedString(string: productAttributes.Name! as String)
                labelText.appendAttributedString(NSAttributedString(string:"*"))
                let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                
                cell.titleLbl.attributedText = labelText
            }
            else
            {
                cell.titleLbl.text = productAttributes.Name! as String
            }
            
            
            
            titleXPoint = 5
            buttonXPoint = 5
            for i in 0  ..< productAttributes.values!.count  {
                
                let button   = UIButton(type: UIButtonType.Custom) as UIButton
                button.frame = CGRectMake( 5, CGFloat (buttonXPoint), 30, 30)
                button.setImage(UIImage(named: "unselected.png"), forState: UIControlState.Normal)
                button.setImage(UIImage(named: "selected.png"), forState: UIControlState.Selected)
                button.accessibilityValue = String(format: "%d/%d",indexPath.row,i)
                button.tag = indexPath.row
                button.addTarget(self, action: #selector(ProductDetailsVC.checkMarkbuttonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                
                let label = UILabel(frame: CGRectMake(38, CGFloat(titleXPoint), 250, 30))
                
                if productAttributes.values![i].IsPreSelected {
                    
                    button.selected = true
                    
                    let parameters:Dictionary<String,AnyObject> = ["value": productAttributes.values![i].Id,
                                                                   "key":String(format: "checkout_attribute_%d",productAttributes.Id)]
                    self.dictionaryArray.addObject(parameters)
                    
                    
                }
                else
                {
                    button.selected = false
                }
                
                cell.checkBoxScrollView.addSubview(button)
                label.text = productAttributes.values![i].Name
                
                cell.checkBoxScrollView.addSubview(label)
                buttonXPoint = buttonXPoint + 38
                titleXPoint =  titleXPoint + 38
                
            }
            
            return cell
            
        }
        else //if productAttributes.AttributeControlType == 4
         {
            var cell: TextBoxTableViewCell! = tableView.dequeueReusableCellWithIdentifier("TextBoxTableViewCell") as? TextBoxTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "TextBoxTableViewCell", bundle: nil), forCellReuseIdentifier: "TextBoxTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("TextBoxTableViewCell") as? TextBoxTableViewCell
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.titleLbl.text = productAttributes.DefaultValue
            cell.titleLbl.tag = indexPath.row;
            
            cell.titleLbl.delegate = self
            cell.titleLbl.addTarget(self, action: #selector(ProductDetailsVC.textFieldDidChanged(_:)), forControlEvents: UIControlEvents.EditingChanged)
            
            if productAttributes.IsRequired {
                
                let labelText = NSMutableAttributedString(string: productAttributes.Name!)
                labelText.appendAttributedString(NSAttributedString(string:"*"))
                let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                
                cell.nameLbl.attributedText = labelText
            }
            else
            {
                cell.nameLbl.text = productAttributes.Name!
            }
            
            return cell
        }
        }else {
            
            
            var cell: ItemsTableViewCell! = tableView.dequeueReusableCellWithIdentifier("ItemsTableViewCell") as? ItemsTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "ItemsTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemsTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("ItemsTableViewCell") as? ItemsTableViewCell
            }
            
            cell.itemImageView.layer.borderWidth=1.0
            cell.itemImageView.layer.masksToBounds = false
            cell.itemImageView.layer.borderColor = UIColor.whiteColor().CGColor
            cell.itemImageView.layer.cornerRadius = 13
            cell.itemImageView.layer.cornerRadius = cell.itemImageView.frame.size.height/2
            cell.itemImageView.clipsToBounds = true
            
            let imageUrl = self.shoppingCartModel!.items![indexPath.row].picture!.ImageUrl

            cell.itemImageView.sd_setImageWithURL(NSURL(string: imageUrl!))
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            cell.titleLbl.text = self.shoppingCartModel!.items![indexPath.row].ProductName
            cell.amountLbl.text = self.shoppingCartModel!.items![indexPath.row].UnitPrice
            cell.plusBtn.tag   =  indexPath.row;
            cell.minusBtn.tag  =  indexPath.row;
            cell.deleteBtn.tag =  indexPath.row;
            
            cell.incrementLbl.text = String(format: "%d",self.shoppingCartModel!.items![indexPath.row].Quantity as NSInteger)
            
            cell.plusBtn.addTarget(self, action: #selector(ShoppingCartVC.plusBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.minusBtn.addTarget(self, action: #selector(ShoppingCartVC.minusBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            cell.deleteBtn.addTarget(self, action: #selector(ShoppingCartVC.deleteBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            //self.roundRectToAllView(cell.containerView)
            
            return cell
            
        }
        
    }
    
    // MARK: - Table View Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
    }
    

    
    
}
