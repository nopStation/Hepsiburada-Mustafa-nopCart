//
//  MoreVC.swift
//  NopCart
//
//  Created by BS85 on 5/3/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit

class MoreVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.customNavBarView.navigationBackButton.hidden = true
        self.customNavBarView.menuButton.hidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
