//
//  SettingsVC.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper
class SettingsVC: BaseVC {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var urlTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func testURLBtnAct(sender: AnyObject) {
        
        let url = self.urlTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if url.characters.count == 0 {
            AppUtility.showToast("Enter URL", view: self.view)
            return
        }
        self.urlTextField.resignFirstResponder()
        
        self.TestApi(url)
    }
    
    
    @IBAction func defaultURLBtnAct(sender: AnyObject) {
        

        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey("accessToken")

        SessionManager.manager.baseURL = SessionManager.manager.defaultBaseUrl
        appDelegate.slideMenuSetup()
        
    }

    
    func showNewBaseUrlSelectAlert(newBaseUrl: String)
    {
        let alertMessage = String(format: "The URL you typed is a valid URL. Do you want to set this as your nopCommerce URL ?")
        let alertController = UIAlertController(title: "Success", message: alertMessage, preferredStyle: .Alert)
        let yesAction = UIAlertAction(title: "Yes", style: .Default) { (action:UIAlertAction!) in
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.removeObjectForKey("accessToken")
            SessionManager.manager.baseURL = newBaseUrl
            appDelegate.slideMenuSetup()
        }
        alertController.addAction(yesAction)
        
        let noAction = UIAlertAction(title: "No", style: .Cancel, handler: nil)
        alertController.addAction(noAction)
        self.presentViewController(alertController, animated: true, completion:nil)

    }
    
    // MARK: - API
    
    func TestApi(url : String)
    {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        let testUrlString = String(format: "%@/api/%@", url,AppConstants.Api.Categories)
        manager.request(.GET, testUrlString, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        let testUrlString = String(format: "%@/api/", url)
                        self.showNewBaseUrlSelectAlert(testUrlString)
                        
                    }
                    else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
    }


    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder();
        return true;
    }

}
