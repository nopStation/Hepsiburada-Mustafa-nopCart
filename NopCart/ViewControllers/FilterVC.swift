//
//  FilterVC.swift
//  NopCart
//
//  Created by BS-125 on 6/29/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit

class FilterVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var lowerPriceLabel: UILabel!
    @IBOutlet weak var upperPriceLabel: UILabel!
    @IBOutlet weak var slideContainerView: UIView!
    
    @IBOutlet weak var containerTableView: UITableView!
    @IBOutlet weak var containerTableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var alreadyFilteredItemScrollView: UIScrollView!
    
    @IBOutlet weak var alreadyFilteredItemsVIewHeightConstraint: NSLayoutConstraint!
    
    //let rangeSlider = GZRangeSlider ()
    var notFilteredItemsOrderedSet: NSMutableOrderedSet = []
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame = UIScreen.mainScreen().bounds
        alreadyFilteredItemsVIewHeightConstraint.constant = 0
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(FilterVC.reloadFilterTableView(_:)), name:"com.notification.reloadFilterTableView", object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(FilterVC.SliderEndTrackingWithTouch(_:)), name:"com.notification.SliderEndTrackingWithTouch", object: nil)


        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        
    }
    
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.reloadFilterTableView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.SliderEndTrackingWithTouch", object: nil)
        
    }
    
    
    
    
    
    
    @IBAction func removeFilterBtnAct(sender: AnyObject) {
        
        
        SessionManager.manager.productModel!.AlreadyFilteredItems!.removeAll()
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadProductsTableView", object:nil);


        appDelegate.slideMenuController.toggleRightSideMenuCompletion(nil)
        
        
        
    }
    
    func SliderEndTrackingWithTouch(notification: NSNotification) {
        
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadProductsTableView", object:nil);
        appDelegate.slideMenuController.toggleRightSideMenuCompletion(nil)
        
    }
    
    @IBAction func backBtnAct(sender: AnyObject) {
        
        appDelegate.slideMenuController.toggleRightSideMenuCompletion(nil)
        
    }
    
    
    
    
    func reloadFilterTableView(notification: NSNotification) {
        
        
        for view in slideContainerView.subviews{
            view.removeFromSuperview()
        }
        
        
        let width: CGFloat = slideContainerView.bounds.width
        let rangeSlider = GZRangeSlider(frame: CGRect(x: 0, y: 0 ,
            width: width, height: 30.0))
        
        
        
        self.lowerPriceLabel.text = String(format: "%.1f", SessionManager.manager.productModel!.PriceRange!.From)
        self.upperPriceLabel.text = String(format: "%.1f", SessionManager.manager.productModel!.PriceRange!.To)
        
        rangeSlider.setRange(Int(SessionManager.manager.productModel!.PriceRange!.From), maxRange: Int( SessionManager.manager.productModel!.PriceRange!.To), accuracy: 1)
        rangeSlider.valueChangeClosure = {
            (left, right) -> () in
            print("left = \(left)  right = \(right) \n")
            
            SessionManager.manager.productModel?.PriceRange?.From = Double(left)
            SessionManager.manager.productModel?.PriceRange?.To   = Double(right)
            
            self.lowerPriceLabel.text = String(format: "%.1f",  Double(left))
            self.upperPriceLabel.text = String(format: "%.1f",  Double(right))
        }
        
        slideContainerView.addSubview(rangeSlider)
        
        
        
        if SessionManager.manager.productModel?.AlreadyFilteredItems!.count == 0 {
            alreadyFilteredItemsVIewHeightConstraint.constant = 0
        } else {
            alreadyFilteredItemsVIewHeightConstraint.constant = 105
            
            
            let subViews = alreadyFilteredItemScrollView.subviews
            for subview in subViews{
                subview.removeFromSuperview()
            }
            
            var itemScrollViewTotalWidth = 0
            var buttonXPoint: CGFloat = 5.0
            for var i = 0 ; i < SessionManager.manager.productModel?.AlreadyFilteredItems!.count ; i += 1 {
                
                let buttonTitle = String(format: "%@ %@",(SessionManager.manager.productModel?.AlreadyFilteredItems![i].SpecificationAttributeName)!, (SessionManager.manager.productModel!.AlreadyFilteredItems![i].SpecificationAttributeOptionName)!)
                let constraintRect = CGSize(width: CGFloat.max, height: 30)
                let boundingBox = buttonTitle.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(18.0)], context: nil)
                
                
                let button   = UIButton(type: UIButtonType.Custom) as UIButton
                button.frame = CGRectMake( buttonXPoint, 5, boundingBox.width + 10, 30)
                button.tag = SessionManager.manager.productModel!.AlreadyFilteredItems![i].FilterId
                
                button.setTitle(buttonTitle , forState: UIControlState.Normal)
                
                button.layer.cornerRadius = 4.0
                //button.layer.borderWidth = 1
                //button.layer.borderColor = UIColor.grayColor().CGColor
                button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                button.backgroundColor = UIColor(red: 22/255.0, green: 158/255.0, blue: 179/255.0, alpha: 1)
                
                
                button.titleLabel?.font = UIFont(name: "System", size: 13)
                alreadyFilteredItemScrollView.addSubview(button)
                
                buttonXPoint += (boundingBox.width + 10) + 10
                itemScrollViewTotalWidth += 1
            }
            
            alreadyFilteredItemScrollView.contentSize = CGSize(width:buttonXPoint, height:40)
            
            
            
        }
        
        
        notFilteredItemsOrderedSet.removeAllObjects()
        
        if let notFilteredItems    = SessionManager.manager.productModel!.NotFilteredItems {
        for notFilteredItem in notFilteredItems {
            let specificationAttributeName = notFilteredItem.SpecificationAttributeName!
            if !notFilteredItemsOrderedSet.containsObject(specificationAttributeName) {
                notFilteredItemsOrderedSet.addObject(specificationAttributeName)
            }
        }
      }
        print(notFilteredItemsOrderedSet.count)
        
        self.containerTableViewHeightConstraint.constant = CGFloat(notFilteredItemsOrderedSet.count) * CGFloat(77.0)
        self.containerTableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    func rangeSliderValueChanged(rangeSlider: RangeSlider) {
//        print("Range slider value changed: (\(rangeSlider.lowerValue) , \(rangeSlider.upperValue))")
//        
//        lowerPriceLabel.text = String(format:"%.1f", rangeSlider.lowerValue)
//        upperPriceLabel.text = String(format:"%.1f", rangeSlider.upperValue)
//        
//    }
    
    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notFilteredItemsOrderedSet.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        
        var cell: FilterTableViewCell! = tableView.dequeueReusableCellWithIdentifier("FilterTableViewCell") as? FilterTableViewCell
        
        if cell == nil {
            tableView.registerNib(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
            cell = tableView.dequeueReusableCellWithIdentifier("FilterTableViewCell") as? FilterTableViewCell
        }
        
        
        cell.itemName.text = notFilteredItemsOrderedSet[indexPath.row] as? String
        
        let spAttributeName = notFilteredItemsOrderedSet[indexPath.row] as! NSString
        
        
        let subViews = cell.itemScrollView.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        
        var itemScrollViewTotalWidth = 0
        var buttonXPoint: CGFloat = 5.0
        for i in 0  ..< SessionManager.manager.productModel!.NotFilteredItems!.count {
            
            if spAttributeName == SessionManager.manager.productModel?.NotFilteredItems![i].SpecificationAttributeName
            {
                
                let buttonTitle = SessionManager.manager.productModel?.NotFilteredItems![i].SpecificationAttributeOptionName
                
                let constraintRect = CGSize(width: CGFloat.max, height: 30)
                let boundingBox = buttonTitle!.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(18.0)], context: nil)
                
                
                let button   = UIButton(type: UIButtonType.Custom) as UIButton
                button.frame = CGRectMake( buttonXPoint, 5, boundingBox.width + 10, 30)
                button.tag = SessionManager.manager.productModel!.NotFilteredItems![i].FilterId
                
                button.setTitle(buttonTitle! , forState: UIControlState.Normal)
                
                button.layer.cornerRadius = 4.0
                button.layer.borderWidth = 1
                button.layer.borderColor = UIColor.grayColor().CGColor
                button.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
                
                
                
                button.titleLabel?.font = UIFont(name: "System", size: 13)
                button.addTarget(self, action: #selector(FilterVC.buttonActionForAttributeItem(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                cell.itemScrollView.addSubview(button)
                
                buttonXPoint += (boundingBox.width + 10) + 10
                itemScrollViewTotalWidth += 1
            }
        }
        
        cell.itemScrollView.contentSize = CGSize(width:buttonXPoint, height:40)
        
        
        return cell
    }
    
    // MARK: - Table View Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
    }
    
    func buttonActionForAttributeItem(sender:UIButton!)
    {
        
        sender.backgroundColor = UIColor.lightGrayColor()
        for notFilteredItem in SessionManager.manager.productModel!.NotFilteredItems! {
            if notFilteredItem.FilterId == sender.tag {
                
                SessionManager.manager.productModel!.AlreadyFilteredItems!.append(notFilteredItem)
            }
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadCategoryTableView", object:nil);
        
        //let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //let aVariable = appDelegate.slideMenuController
        appDelegate.slideMenuController.toggleRightSideMenuCompletion(nil)
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
