//
//  WishlistVC.swift
//  NopCart
//
//  Created by BS-125 on 7/26/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
class WishlistVC: BaseVC {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var wishListTableView: UITableView!
    @IBOutlet weak var wishListTableViewHeightConstraint: NSLayoutConstraint!
    
    
    
    var wishListInfo  = WishListModel?()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = false
        

        self.getWishListInfo()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    @IBAction func addToCartFromWishList(sender: AnyObject) {
        
        let dicArray = NSMutableArray()
        if let items = self.wishListInfo?.items {
            for item in items {
                let parameters : Dictionary <String, AnyObject> = ["value": item.Id, "key": "addtocart"]
                dicArray.addObject(parameters)
            }
            
            self.addProductFromWishlistToShoppingCart(dicArray)
        }
    }
    
    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    func getWishListInfo()
    {
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, AppConstants.Api.WishListInfo, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if let list = Mapper<WishListModel>().map(json.dictionaryObject) {
                            
                            self.wishListInfo = list
                            
                            if let items = self.wishListInfo?.items {
                                if items.count == 0 {
                                AppUtility.showToast("Your wish list is empty",view: self.view)
                                }else {
                                    self.containerView.hidden = false
                                    self.wishListTableViewHeightConstraint.constant = CGFloat((self.wishListInfo?.items!.count)! * 150)
                                    self.wishListTableView.reloadData()
                                }
                            }else{
                                AppUtility.showToast("Your wish list is empty",view: self.view)
                            }
                        }
                        
                    }
                    else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
    }

    
    
    func addProductFromWishlistToShoppingCart(params: NSMutableArray) {
        
        AppUtility.showLoading(self.view)
        
        
        let productDetails = String(format: "%@%@",SessionManager.manager.baseURL,AppConstants.Api.AddItemsToCartFromWishlist)
        let manager = RequestManager.manager
        
        let request = NSMutableURLRequest(URL: NSURL(string: productDetails)!)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = AppUtility.getHeaderDic()
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(params, options: [])
        
        manager.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .Success:
                    print("Success")
                    
                    AppUtility.hideLoading(self.view)
                    
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        
                        print(json)
                        if json["StatusCode"].intValue == 200 {
                            
                            appDelegate.cartView.cartLabel.text =  "\(json["Count"].intValue)"

                            self.wishListTableViewHeightConstraint.constant = 0
                            self.containerView.hidden = true
                            AppUtility.showToast("Wishlist items added to cart successfully",view: self.view)
                            
                        }else {
                            var errorMessage = ""
                            if let errorList = json["ErrorList"].arrayObject {
                                for error in errorList {
                                    errorMessage += "\(error as! String)\n"
                                }
                            }
                            AppUtility.showToast(errorMessage, view: self.view)
                        }
                    }
                    
                case .Failure:
                    print("Failure")
                    print("Error:\(response.result.error?.localizedDescription)")
                    AppUtility.showToast("\(response.result.error?.localizedDescription)", view: self.view)
                }
        }
        
    }
    
    func UpdateWishlist(parameters: NSMutableArray, sender:UIButton!) {
        
        AppUtility.showLoading(self.view)
        
        
        let wishListUrl = String(format: "%@%@",SessionManager.manager.baseURL,AppConstants.Api.UpdateWishList)
        let manager = RequestManager.manager
        
        let request = NSMutableURLRequest(URL: NSURL(string: wishListUrl)!)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = AppUtility.getHeaderDic()
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        manager.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .Success:
                    print("Success")
                    
                    AppUtility.hideLoading(self.view)
                    
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        
                        print(json)
                        if json["StatusCode"].intValue == 200 {
                            
                            let pointInTable: CGPoint = sender.convertPoint(sender.bounds.origin, toView: self.wishListTableView)
                            let cellIndexPath = self.wishListTableView.indexPathForRowAtPoint(pointInTable)
                            
                            self.wishListInfo?.items!.removeAtIndex(sender.tag)
                            if self.wishListInfo?.items!.count == 0 {
                                self.wishListTableView.deleteRowsAtIndexPaths([cellIndexPath!], withRowAnimation: UITableViewRowAnimation.Automatic)
                                self.wishListTableViewHeightConstraint.constant = CGFloat((self.wishListInfo?.items!.count)!) * 150
                                self.containerView.hidden = true
                                AppUtility.showToast("Your wishlist is empty", view: self.view)
                            } else {
                                self.wishListTableView.deleteRowsAtIndexPaths([cellIndexPath!], withRowAnimation: UITableViewRowAnimation.Automatic)
                                self.wishListTableViewHeightConstraint.constant = CGFloat((self.wishListInfo?.items!.count)!) * 150
                            }
                            
                        }else {
                            var errorMessage = ""
                            if let errorList = json["ErrorList"].arrayObject {
                                for error in errorList {
                                    errorMessage += "\(error as! String)\n"
                                }
                            }
                            AppUtility.showToast(errorMessage, view: self.view)
                        }
                    }
                    
                case .Failure:
                    print("Failure")
                    print("Error:\(response.result.error?.localizedDescription)")
                    AppUtility.showToast("\(response.result.error?.localizedDescription)", view: self.view)
                }
        }
        
    }

    
    
    // MARK: - TableView Delegate and DataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let items = self.wishListInfo?.items {
          return items.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell : WishListTableViewCell = tableView.dequeueReusableCellWithIdentifier("WishListTableViewCell") as! WishListTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(WishlistVC.deleteBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        
        cell.titleLabel.text = self.wishListInfo?.items![indexPath.row].ProductName
        cell.amountLabel.text = self.wishListInfo?.items![indexPath.row].UnitPrice
        
        cell.itemImageView.layer.borderWidth=1.0
        cell.itemImageView.layer.masksToBounds = false
        cell.itemImageView.layer.borderColor = UIColor.whiteColor().CGColor
        cell.itemImageView.layer.cornerRadius = 13
        cell.itemImageView.layer.cornerRadius = cell.itemImageView.frame.size.height/2
        cell.itemImageView.clipsToBounds = true
        
        cell.itemImageView.image = nil
        
        let imageUrl = self.wishListInfo?.items![indexPath.row].picture!.ImageUrl
        cell.itemImageView.sd_setImageWithURL(NSURL(string: imageUrl!))
        
        cell.containerView.layer.cornerRadius = 8

        
        return cell
    }
    
    
    // MARK: - Utility Methods
    
    func deleteBtnAction(sender:UIButton!)
    {

        let parameters : Dictionary <String, AnyObject> = ["value": (self.wishListInfo?.items![sender.tag].Id)!, "key": "removefromcart"]
        
        let dicArray = NSMutableArray()
        dicArray.addObject(parameters)
        print(dicArray)
        
        self.UpdateWishlist(dicArray, sender: sender)
        //self.wishListTableView.performSelector(#selector(UITableView.reloadData), withObject: nil, afterDelay: 0.5)
    }


}
