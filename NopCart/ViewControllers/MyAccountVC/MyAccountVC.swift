//
//  MyAccountVC.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import ObjectMapper

class MyAccountVC: BaseVC , UITextFieldDelegate{

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var newsLetterButton: UIButton!
    //    @IBOutlet weak var passwordTextField: UITextField!
    //    @IBOutlet weak var confirmPassTextField: UITextField!
    
    
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    
    var dobSelected: Bool = false
    var dobDay: Int = 0
    var dobMonth: Int = 0
    var dobYear: Int = 0
    
    var genderSelected: Bool = false
    var gender: String = ""
    
    var newsLetterSelected: Bool = false
    
    
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!
    
    
   
    
    
    //---------------------------------------------------------------------
    //MARK: - ViewController LifeCycle
    //---------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = false
        self.customNavBarView.navigationMenuButton.hidden = false

        
        self.containerView.layer.cornerRadius = 4.0
        self.getCustomerInfo()
    }

    override func viewWillAppear(animated: Bool) {
        
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - User Interaction
    //-----------------------------------------------------------------

    @IBAction func showDOBPicker(sender: AnyObject) {
        
        DatePickerDialog().show("Choose Date of birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .Date) {
            (date) -> Void in
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            self.dobSelected = true
            self.dobLabel.text = "\(dateFormatter.stringFromDate(date))"
            
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Day , .Month , .Year], fromDate: date)
            
            self.dobDay = components.day
            self.dobMonth = components.month
            self.dobYear =  components.year
        }
    }
    
    @IBAction func selectMale(sender: UIButton) {
        self.maleRadioButton.selected = true
        self.femaleRadioButton.selected = false
        
        self.genderSelected = true
        self.gender = "M"
    }
    
    @IBAction func selectFemale(sender: UIButton) {
        self.femaleRadioButton.selected = true
        self.maleRadioButton.selected = false
        
        self.genderSelected = true
        self.gender = "F"
    }
    
    @IBAction func toggleNewsLetterOption(sender: UIButton) {
        if self.newsLetterButton.selected {
            self.newsLetterButton.selected = false
            self.newsLetterSelected = false
        } else {
            self.newsLetterButton.selected = true
            self.newsLetterSelected = true
        }
    }
    
    @IBAction func saveAccountInfo(sender: UIButton) {
        
        let firstName = self.firstNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if firstName.characters.count == 0 {
            AppUtility.showToast("Enter First Name", view: self.view)
            return
        }
        
        let lastName = self.lastNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if lastName.characters.count == 0 {
            AppUtility.showToast("Enter Last Name", view: self.view)
            return
        }
        
        //        if self.dobSelected == false {
        //            self.showToast("Choose Date of birth")
        //            return
        //        }
        //
        //        if self.genderSelected == false {
        //            self.showToast("Choose gender")
        //            return
        //        }
        
        let email = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if email.characters.count == 0 {
            AppUtility.showToast("Enter E-Mail address", view: self.view)
            return
        }
        
        if self.isValidEmail(email) == false {
            AppUtility.showToast("Invalid E-Mail address", view: self.view)
            return
        }
        
        //        let company = self.companyTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        //        if company.characters.count == 0 {
        //            self.showToast("Enter company name")
        //            return
        //        }
        //
        //        let password = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        //        if password.characters.count == 0 {
        //            self.showToast("Enter password")
        //            return
        //        }
        //
        //        let confirmedPassword = self.confirmPassTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        //        if confirmedPassword.characters.count == 0 {
        //            self.showToast("Confirm your password")
        //            return
        //        }
        //
        //        if password != confirmedPassword {
        //            self.showToast("Passwords don't match")
        //            return
        //        }
        
        self.proceedToSave()
    }
    
    
    
    //---------------------------------------------------------------------
    //MARK: - Utility Methods
    //---------------------------------------------------------------------

    func isValidEmail(testEmail:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testEmail)
    }
    
    func proceedToSave()
    {
        var params = [String: AnyObject]()
        params["FirstName"] = self.firstNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["LastName"] = self.lastNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["DateOfBirthDay"] = self.dobDay
        params["DateOfBirthMonth"] = self.dobMonth
        params["DateOfBirthYear"] = self.dobYear
        params["Email"] = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["Phone"] = self.phoneNumberTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        params["Company"] = self.companyTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["Newsletter"] = self.newsLetterSelected ? "true":"false"
        params["Gender"] = self.gender
        //params["Password"] = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        //params["ConfirmPassword"] = self.confirmPassTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        
        self.saveCustomerInfo(params)
        
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    
    func getCustomerInfo() {
        
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, AppConstants.Api.CustomerInfo, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let accountInfo = JSON(responseValue)
                    print("json::\(accountInfo)")
                    if accountInfo["StatusCode"].intValue == 200 {
                        
                        if let firstName = accountInfo["FirstName"].string {
                            self.firstNameTextField.text = firstName
                        } else {
                            self.firstNameTextField.text = ""
                        }
                        
                        if let lastName = accountInfo["LastName"].string {
                            self.lastNameTextField.text = lastName
                        } else {
                            self.lastNameTextField.text = ""
                        }
                        
                        if let email =  accountInfo["Email"].string {
                            self.emailTextField.text = email
                        } else {
                            self.emailTextField.text = ""
                        }
                        
                        if let phoneNumber  =  accountInfo["Phone"].string {
                            self.phoneNumberTextField.text = phoneNumber
                        } else {
                            self.phoneNumberTextField.text = ""
                        }
                        
                        
                        if let customerGender =  accountInfo["Gender"].string {
                            self.genderSelected = true
                            self.gender = customerGender
                            if self.gender == "M" {
                                self.maleRadioButton.selected = true
                                self.femaleRadioButton.selected = false
                            } else if self.gender == "F" {
                                self.maleRadioButton.selected = false
                                self.femaleRadioButton.selected = true
                            }
                        } else {
                            self.genderSelected = false
                            self.gender = ""
                            self.maleRadioButton.selected = false
                            self.femaleRadioButton.selected = false
                        }
                        
                        if let company = accountInfo["Company"].string{
                            self.companyTextField.text = company
                        } else {
                            self.companyTextField.text = ""
                        }
                        
                        
                        if let newsletter =  accountInfo["Newsletter"].bool {
                            self.newsLetterSelected =  newsletter
                            if self.newsLetterSelected == true {
                                self.newsLetterButton.selected = true
                            } else {
                                self.newsLetterButton.selected = false
                            }
                        }
                        
                        
                        if let bday = accountInfo["DateOfBirthDay"].int, bmonth = accountInfo["DateOfBirthMonth"].int, byear = accountInfo["DateOfBirthYear"].int {
                            self.dobSelected = true
                            self.dobDay = bday
                            self.dobMonth = bmonth
                            self.dobYear = byear
                            self.dobLabel.text = "\(self.dobDay)/\(self.dobMonth)/\(self.dobYear)"
                        } else {
                            self.dobSelected = false
                            self.dobDay = 0
                            self.dobMonth = 0
                            self.dobYear = 0
                            self.dobLabel.text = "dd/mm/yyyy"
                        }
                    }
                    else {
                        var errorMessage = ""
                        if let errorList = accountInfo["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)

                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }

        
        
    }
    
    func saveCustomerInfo( params : [String: AnyObject]) {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.POST, AppConstants.Api.CustomerInfo, parameters: params, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        AppUtility.showToast("Account information saved successfully", view: self.view)
                        
                        AppUtility.backToRootView(self.navigationController!)
                    }
                    else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }

    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder();
        return true;
    }

}
