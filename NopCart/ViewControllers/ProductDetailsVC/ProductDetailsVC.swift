//
//  ProductDetailsVC.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import MBProgressHUD
import DropDown
class ProductDetailsVC: BaseVC , UICollectionViewDataSource,UICollectionViewDelegate , UIWebViewDelegate, UITableViewDataSource, UITableViewDelegate ,UITextFieldDelegate {

    
    @IBOutlet weak var imageContainerVIewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var fullDescription: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var htmlWebView: UIWebView!
    @IBOutlet weak var webViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionVIewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var attributsViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var attributsViewBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionVIewHeightConstraints: NSLayoutConstraint!
    
    
    var productId : NSInteger!
    var productDetails = ProductDetailsModel?()
    var relatedProducts     = [RelatedProductsModel]()
    var bannerList : [String] = []
    var dictionaryArray     = NSMutableArray ()
    var textFieldDic        = [Int:AnyObject]()
    var totalAttributsTableViewHeight = NSInteger ()
    var dropDownArray       = [AnyObject]()

    
    
    //---------------------------------------------------------------------
    //MARK: - ViewController LifeCycle
    //---------------------------------------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.collectionView.registerNib(UINib(nibName: "HomeCategoryCollectionCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "HomeCategoryCollectionCell")

        
        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = false
        self.customNavBarView.navigationMenuButton.hidden = false
       
        //self.productId = 4
        self.getProductDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //-----------------------------------------------------------------
    // MARK: - User Interaction
    //-----------------------------------------------------------------

    
    
    
    func textFieldDidChanged(textField:UITextField ){
        
        print(textField.tag)
        print(textField.text)
        self.textFieldDic[textField.tag] = textField.text!
        
        
    }
    
    
    func radioListButtonAction(sender:UIButton!)
    {
        
        var values = sender.accessibilityValue?.componentsSeparatedByString("/")
        let rowIndex:Int? = Int(values![0])
        let itemIndex:Int? = Int(values![1])

        
        let productAttributes = (self.productDetails?.ProductAttributes![rowIndex!])! as Productattributes
        
        for i in 0  ..< productAttributes.values!.count  {
            
            productAttributes.values![i].IsPreSelected = false
        }
        
        productAttributes.values![itemIndex!].IsPreSelected = true
        let indexPath = NSIndexPath(forRow: rowIndex!, inSection: 0)
        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        
        self.updatePriceLabel(productAttributes, itemIndex: itemIndex!)
        
        
    }

    
    
    func showDropDown(sender: UIButton) {
        
        
        let innerDropDown = dropDownArray[sender.tag] as! DropDown
        
        let pointInTable: CGPoint = sender.convertPoint(sender.bounds.origin, toView: self.tableView)
        let cellIndexPath = self.tableView.indexPathForRowAtPoint(pointInTable)
        let cell = self.tableView.cellForRowAtIndexPath(cellIndexPath!) as! DropdownListTableViewCell

        
        innerDropDown.anchorView = cell
        innerDropDown.bottomOffset = CGPoint(x: cell.frame.origin.x, y:cell.frame.origin.y)
        
        if innerDropDown.hidden {
            innerDropDown.show()
        } else {
            innerDropDown.hide()
        }
        
        innerDropDown.selectionAction = { (index, item) in
            
            cell.titleLbl.text = item
            
            let productAttributes = (self.productDetails?.ProductAttributes![sender.tag])! as Productattributes
            self.updatePriceLabel(productAttributes, itemIndex: index)
        }
    }

    @IBAction func wishListBtnAction(sender: AnyObject) {
        
        let button = UIButton()
        button.tag = 2
        self.addToCartBtnAction(button)
    }

    @IBAction func addToCartBtnAction(sender: UIButton) {
        
        
        //sender.tag == 1 For Add to cart
        //sender.tag == 2 For Add to Wishlist
        
        
        let finalDicArray = NSMutableArray ()
        for key in self.textFieldDic.keys {
            let parameters:Dictionary<String,AnyObject> = ["value": self.textFieldDic[key]!,
                                                           "key":String(format: "product_attribute_%d_%d_%d", (self.productDetails?.ProductAttributes![key].ProductId)!, (self.productDetails?.ProductAttributes![key].ProductAttributeId)!, (self.productDetails?.ProductAttributes![key].Id)!)]
            finalDicArray.addObject(parameters)
        }
        
        let parameters:Dictionary<String,AnyObject> = ["value": sender.tag,
                                                       "key":"addtocart_1.EnteredQuantity"]
        finalDicArray.addObject(parameters)
        finalDicArray.addObjectsFromArray(self.dictionaryArray as [AnyObject])
        self.addProductToCart(finalDicArray, cartType: sender.tag)
        
        
    }
    
    
    
    @IBAction func plusBtnAction(sender: UIButton) {
        
        
        if sender.selected {
            
            self.plusBtn.selected = false
            self.descriptionVIewHeightConstraints.constant = 45
            self.webViewHeightConstraint.constant = 0
        }
        else{
            self.plusBtn.selected = true
            self.descriptionVIewHeightConstraints.constant = 270
            self.webViewHeightConstraint.constant = 225
        }
 
        
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - Utilities Methods
    //-----------------------------------------------------------------

    func checkMarkbuttonAction(sender:UIButton!)
    {
        
        var values = sender.accessibilityValue?.componentsSeparatedByString("/")
        //let rowIndex:Int? = Int(values![0])
        let itemIndex:Int? = Int(values![1])
        
        
        if  sender.selected {
            sender.selected = false
            
        }
        else {
            sender.selected = true
            
        }
        
        let productAttributes = (self.productDetails?.ProductAttributes![sender.tag])! as Productattributes
        self.updatePriceLabelForCheckList(productAttributes, itemIndex: itemIndex!)
        
        
    }
    
    
    func updatePriceLabelForCheckList(productattributes : Productattributes , itemIndex: NSInteger){
        
        if Int(dictionaryArray.count as NSInteger) != 0 {
            
            var indexValueCheckflag = NSInteger ()
            indexValueCheckflag = 0
            
                for j in 0  ..< dictionaryArray.count  {
                    let dic  = dictionaryArray.objectAtIndex(j)
                    
                    let id  = dic.valueForKey("value") as! NSInteger
                    if Int(productattributes.values![itemIndex].Id) == Int(id)  {
                        indexValueCheckflag = 1
                        self.dictionaryArray.removeObjectAtIndex(j)
                        break
                    }
            }
            
            if indexValueCheckflag == 0{
                let parameters:Dictionary<String,AnyObject> = ["value": productattributes.values![itemIndex].Id,
                                                               "key":String(format: "product_attribute_%d_%d_%d",productattributes.ProductId,
                                                                productattributes.ProductAttributeId ,
                                                                productattributes.Id)]
                self.dictionaryArray.addObject(parameters)
                
                
            }
            
        }
        else {
            
            let parameters:Dictionary<String,AnyObject> = ["value": productattributes.values![itemIndex].Id,
                                                           "key":String(format: "product_attribute_%d_%d_%d",productattributes.ProductId,
                                                            productattributes.ProductAttributeId ,
                                                            productattributes.Id)]
            self.dictionaryArray.addObject(parameters)
            
        }
        
        
        
        self.getProductPrice(self.dictionaryArray)
    }
    
    func updatePriceLabel(productattributes : Productattributes , itemIndex: NSInteger){
        
        if Int(dictionaryArray.count as NSInteger) != 0 {
            
            var indexValueCheckflag = NSInteger ()
            indexValueCheckflag = 0
            for i in 0  ..< productattributes.values!.count  {
                
                for j in 0  ..< dictionaryArray.count  {
                    let dic  = dictionaryArray.objectAtIndex(j)
                    
                    let id  = dic.valueForKey("value") as! NSInteger
                    if Int(productattributes.values![i].Id) == Int(id)  {
                        
                        indexValueCheckflag = 1
                        let parameters:Dictionary<String,AnyObject> = ["value": productattributes.values![itemIndex].Id,
                                                                       "key":String(format: "product_attribute_%d_%d_%d",productattributes.ProductId,
                                                                        productattributes.ProductAttributeId ,
                                                                        productattributes.Id)]
                        self.dictionaryArray.replaceObjectAtIndex(j, withObject: parameters)
                        break
                    }
                }
            }
            
            if indexValueCheckflag == 0{
                let parameters:Dictionary<String,AnyObject> = ["value": productattributes.values![itemIndex].Id,
                                                               "key":String(format: "product_attribute_%d_%d_%d",productattributes.ProductId,
                                                                productattributes.ProductAttributeId ,
                                                                productattributes.Id)]
                self.dictionaryArray.addObject(parameters)
                
                
            }
            
        }
        else {
            
            let parameters:Dictionary<String,AnyObject> = ["value": productattributes.values![itemIndex].Id,
                                                           "key":String(format: "product_attribute_%d_%d_%d",productattributes.ProductId,
                                                            productattributes.ProductAttributeId ,
                                                            productattributes.Id)]
            self.dictionaryArray.addObject(parameters)
            
        }
        
        
        
        self.getProductPrice(self.dictionaryArray)
    }
    
    
    
    
    func setUpAttributsTableViewContent () {
        
        
        self.totalAttributsTableViewHeight = 0
        self.attributsViewBottomConstraints.constant = 0
        
        for var i = 0 ; i < self.productDetails?.ProductAttributes!.count ; i += 1 {
            
            self.attributsViewBottomConstraints.constant = 8
            
            let productAttributes = (self.productDetails?.ProductAttributes![i])! as Productattributes
            
            if productAttributes.AttributeControlType == 1
            {
                
                self.totalAttributsTableViewHeight = self.totalAttributsTableViewHeight + 65
                
                let dropDown = DropDown()
                dropDown.tag = i
                for j in 0  ..< productAttributes.values!.count  {
                    
                    dropDown.dataSource.append(productAttributes.values![j].Name!)

                    if productAttributes.values![j].IsPreSelected {
                        
                        let parameters:Dictionary<String,AnyObject> = ["value": productAttributes.values![j].Id,
                                                                       "key":String(format: "product_attribute_%d_%d_%d",productAttributes.ProductId,
                                                                        productAttributes.ProductAttributeId ,
                                                                        productAttributes.Id)]
                        self.dictionaryArray.addObject(parameters)
                        break
                    }
                }
                
                self.dropDownArray.append(dropDown)

                
                
            }
            else if productAttributes.AttributeControlType == 2{
                self.totalAttributsTableViewHeight = self.totalAttributsTableViewHeight + 85
                for j in 0  ..< productAttributes.values!.count  {
                    if productAttributes.values![j].IsPreSelected {
                        
                        let parameters:Dictionary<String,AnyObject> = ["value": productAttributes.values![j].Id,
                                                                       "key":String(format: "product_attribute_%d_%d_%d",productAttributes.ProductId,
                                                                        productAttributes.ProductAttributeId ,
                                                                        productAttributes.Id)]
                        self.dictionaryArray.addObject(parameters)
                        break

                    }
                }
                
            }
                
            else if productAttributes.AttributeControlType == 3{
                
                self.totalAttributsTableViewHeight = self.totalAttributsTableViewHeight + 31 + 44 * productAttributes.values!.count
                
            }
            else if productAttributes.AttributeControlType == 4{
                self.totalAttributsTableViewHeight = self.totalAttributsTableViewHeight + 65
                if let defaultValue = productAttributes.DefaultValue   {
                    
                    self.textFieldDic[i] = defaultValue
                }
            }
        }
        self.attributsViewHeightConstraint.constant = CGFloat(self.totalAttributsTableViewHeight)
  
    }
    
    
    func setUpImageSlidingView() {
        
        
        if let imageList = self.productDetails?.PictureModels {
            for sliderImg in imageList  {
                if let imageUrl = sliderImg.ImageUrl {
                    self.bannerList.append(imageUrl)
                }
            }
            let imageSlidingView = NSBundle.mainBundle().loadNibNamed("ImageSlidingView", owner: nil, options: nil)[0] as! ImageSlidingView
            imageSlidingView.imageArray = self.bannerList
            imageSlidingView.populateImage()
            imageSlidingView.frame = self.imageContainerView.frame
            //self.imageContainerVIewHeightConstraints.constant = self.getImageContainerViewHeight()
            self.imageContainerView.addSubview(imageSlidingView)
        }
        
    }
    
    func getImageContainerViewHeight () -> CGFloat {
        
        
        let bound = UIScreen.mainScreen().bounds
        let hr: CGFloat = 256/190
        
        let w: CGFloat = (bound.width/2)
        let h: CGFloat = hr*w
        return h
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    
    func addProductToCart(params: NSMutableArray, cartType: NSInteger) {
        
        AppUtility.showLoading(self.view)
        
        
        let productDetails = String(format: "%@%@/%d/%d",SessionManager.manager.baseURL,AppConstants.Api.AddProductToCart,self.productId, cartType)
        let manager = RequestManager.manager
        
        let request = NSMutableURLRequest(URL: NSURL(string: productDetails)!)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = AppUtility.getHeaderDic()
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(params, options: [])
        
        manager.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .Success:
                    print("Success")
                    
                    AppUtility.hideLoading(self.view)
                    
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        
                        print(json)
                        if json["StatusCode"].intValue == 200 {
                            
                            
                          if cartType == 1 { // Add to cart
                           appDelegate.cartView.cartLabel.hidden = false
                           appDelegate.cartView.cartLabel.text = "\(json["Count"].intValue)"
                           AppUtility.showToast("Successfully added to cart", view: self.view)
                            }else if cartType == 2 { // Add to WishList
                             AppUtility.showToast("Successfully added to Wishlist", view: self.view)
                                
                            }
                        }else {
                            var errorMessage = ""
                            if let errorList = json["ErrorList"].arrayObject {
                                for error in errorList {
                                    errorMessage += "\(error as! String)\n"
                                }
                            }
                            AppUtility.showToast(errorMessage, view: self.view)
                        }
                    }
                    
                case .Failure:
                    print("Failure")
                    print("Error:\(response.result.error?.localizedDescription)")
                    AppUtility.showToast("\(response.result.error?.localizedDescription)", view: self.view)
                }
        }
        
    }
    
    
    func getProductPrice(params: NSMutableArray) {
        
        AppUtility.showLoading(self.view)
        
        
        let productDetails = String(format: "%@%@/%d",SessionManager.manager.baseURL,AppConstants.Api.ProductPrice,self.productId)
        let manager = RequestManager.manager

        let request = NSMutableURLRequest(URL: NSURL(string: productDetails)!)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = AppUtility.getHeaderDic()
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(params, options: [])
        
        manager.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .Success:
                    print("Success")
                    
                    AppUtility.hideLoading(self.view)
                    
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        
                        print(json)
                        if json["StatusCode"].intValue == 200 {

                            if  let priceValue = json["Price"].string {
                                self.priceLbl.text = priceValue
                            }
                        }else {
                            var errorMessage = ""
                            if let errorList = json["ErrorList"].arrayObject {
                                for error in errorList {
                                    errorMessage += "\(error as! String)\n"
                                }
                            }
                            AppUtility.showToast(errorMessage, view: self.view)
                        }
                    }
                    
                case .Failure:
                    print("Failure")
                    print("Error:\(response.result.error?.localizedDescription)")
                    AppUtility.showToast("\(response.result.error?.localizedDescription)", view: self.view)
            }
        }
        
    }

    func getProductDetails() {
        
        AppUtility.showLoading(self.view)
        
        let productDetails = String(format: "%@/%d",AppConstants.Api.ProductDetails,self.productId)
        let manager = RequestManager.manager
        manager.request(.GET, productDetails , parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        if let list = Mapper<ProductDetailsModel>().map(json["Data"].dictionaryObject) {
                          self.productDetails = list
                            
                            
                            self.titleLbl.text = (self.productDetails!.Name ?? "") as String
                            self.priceLbl.text = "\(self.productDetails!.ProductPrice!.Price!)"
                            self.htmlWebView.delegate = self
                            if let fullDesc = self.productDetails!.FullDescription {
                                
                                self.htmlWebView.loadHTMLString(fullDesc as String, baseURL: nil)
                            }
                            
                          self.setUpImageSlidingView()
                          self.setUpAttributsTableViewContent()
                          self.tableView.reloadData()
                            
                          self.getRelatedProduct()

                        }
                        
                    }else {
                     var errorMessage = ""
                     if let errorList = json["ErrorList"].arrayObject {
                        for error in errorList {
                            errorMessage += "\(error as! String)\n"
                        }
                    }
                    AppUtility.showToast(errorMessage, view: self.view)
                 }
                    
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }
    
    
    func getRelatedProduct() {
        
        
        AppUtility.showLoading(self.view)
        
        let productDetails = String(format: "%@/%d",AppConstants.Api.RelatedProducts,self.productId)
        let manager = RequestManager.manager
        manager.request(.GET, productDetails , parameters: ["thumbPictureSize":"320"], headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        if let list = Mapper<RelatedProductsModel>().mapArray(json["Data"].arrayObject) {
                            self.collectionVIewHeightConstraints.constant = 274
                            self.relatedProducts = list
                            self.collectionView.reloadData()
                        }else {
                          self.collectionVIewHeightConstraints.constant = 0
                        }
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                    }
                    
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
 
        
        
    }
    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let productAttributes = (self.productDetails?.ProductAttributes![indexPath.row])! as Productattributes
        
        var cell = CGFloat()
        
        if productAttributes.AttributeControlType == 1 {
            
            cell = 65
            
        }
        else if productAttributes.AttributeControlType == 2 {
            
            cell = 85
        }
        else if productAttributes.AttributeControlType == 3 {
            
            cell = 31 + 44 * CGFloat(productAttributes.values!.count)
            
            
        }else if productAttributes.AttributeControlType == 4 {
            
            cell = 65
            
            
        }
        
        return cell;
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = self.productDetails?.ProductAttributes?.count {
            
            return count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let productAttributes = (self.productDetails?.ProductAttributes![indexPath.row])! as Productattributes
        //let index = indexPath.row as Int
        
        
        if productAttributes.AttributeControlType == 1 {
            
            var cell: DropdownListTableViewCell! = tableView.dequeueReusableCellWithIdentifier("DropdownListTableViewCell") as? DropdownListTableViewCell
            
            print(productAttributes.AttributeControlType)
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "DropdownListTableViewCell", bundle: nil), forCellReuseIdentifier: "DropdownListTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("DropdownListTableViewCell") as? DropdownListTableViewCell
            }
            
            print(cell.frame.origin.y)
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.dropDownBtn.tag = indexPath.row
            
            if self.productDetails!.ProductAttributes![indexPath.row].IsRequired {
                
                let labelText = NSMutableAttributedString(string: productAttributes.Name!)
                labelText.appendAttributedString(NSAttributedString(string:"*"))
                let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                cell.nameLbl.attributedText = labelText
            }
            else
            {
                cell.nameLbl.text = productAttributes.Name! as String
            }
            
            for j in 0  ..< productAttributes.values!.count  {
                if productAttributes.values![j].IsPreSelected {
                    cell.titleLbl.text = productAttributes.values![j].Name
                    break
                    
                }
            }
            
            
//            if self.dropDownMenuSequenceNumber == indexPath.row {
//                
//                print(self.dropDownMenuSequenceNumber)
//                cell.titleLbl.text = self.productDetailsArray[0].ProductAttributes[self.dropDownMenuSequenceNumber].Values[dropDownMenuSelectedIndex].Name as String
//            }
            cell.dropDownBtn.addTarget(self, action: #selector(ProductDetailsVC.showDropDown(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            return cell
            
            
        }
        else if productAttributes.AttributeControlType == 2 {
            
            var cell: RadioListTableViewCell! = tableView.dequeueReusableCellWithIdentifier("RadioListTableViewCell") as? RadioListTableViewCell
            
            // if cell == nil {
            tableView.registerNib(UINib(nibName: "RadioListTableViewCell", bundle: nil), forCellReuseIdentifier: "RadioListTableViewCell")
            cell = tableView.dequeueReusableCellWithIdentifier("RadioListTableViewCell") as? RadioListTableViewCell
            // }
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            
            
            if self.productDetails!.ProductAttributes![indexPath.row].IsRequired {
                
                let labelText = NSMutableAttributedString(string: productAttributes.Name!)
                labelText.appendAttributedString(NSAttributedString(string:"*"))
                let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                cell.titleLbl.attributedText = labelText
            }
            else
            {
                cell.titleLbl.text = productAttributes.Name! as String
            }
            
            
            
            cell.scrollViewForRadioList.contentSize = CGSize(width:productAttributes.values!.count * 150, height:44)
            var titleXPoint = NSInteger ()
            var buttonXPoint = NSInteger ()
            titleXPoint = 38
            buttonXPoint = 5
            
            
            for i in 0  ..< productAttributes.values!.count  {
                
                let button   = UIButton(type: UIButtonType.Custom) as UIButton
                button.frame = CGRectMake( CGFloat(buttonXPoint), 10, 30, 30)
                button.setImage(UIImage(named: "radio-selected.png"), forState: UIControlState.Selected)
                button.setImage(UIImage(named: "radio-unselected.png"), forState: UIControlState.Normal)
                button.accessibilityValue = String(format: "%d/%d",indexPath.row,i)
                button.tag = i
                button.addTarget(self, action: #selector(ProductDetailsVC.radioListButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                
                if productAttributes.values![i].IsPreSelected {
                    button.selected = true
                }
                else
                {
                    button.selected = false
                }
                
                
                cell.scrollViewForRadioList.addSubview(button)
                
                let label = UILabel(frame: CGRectMake(CGFloat(titleXPoint), 10, 100, 30))
                label.text = productAttributes.values![i].Name
                cell.scrollViewForRadioList.addSubview(label)
                buttonXPoint = buttonXPoint + 100 + 32
                titleXPoint =  buttonXPoint + 35
                
            }
            
            return cell
            
        }
            
        else if productAttributes.AttributeControlType == 3{
            
            
            var cell: CheckboxesTableViewCell! = tableView.dequeueReusableCellWithIdentifier("CheckboxesTableViewCell") as? CheckboxesTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "CheckboxesTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckboxesTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("CheckboxesTableViewCell") as? CheckboxesTableViewCell
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.checkBoxScrollView.contentSize = CGSize(width: 200, height: productAttributes.values!.count * 44 )
            var titleXPoint = NSInteger ()
            var buttonXPoint = NSInteger ()
            
            if productAttributes.IsRequired {
                
                let labelText = NSMutableAttributedString(string: productAttributes.Name! as String)
                labelText.appendAttributedString(NSAttributedString(string:"*"))
                let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                
                cell.titleLbl.attributedText = labelText
            }
            else
            {
                cell.titleLbl.text = productAttributes.Name! as String
            }
            
            
            
            titleXPoint = 5
            buttonXPoint = 5
            for i in 0  ..< productAttributes.values!.count  {
                
                let button   = UIButton(type: UIButtonType.Custom) as UIButton
                button.frame = CGRectMake( 5, CGFloat (buttonXPoint), 30, 30)
                button.setImage(UIImage(named: "unselected.png"), forState: UIControlState.Normal)
                button.setImage(UIImage(named: "selected.png"), forState: UIControlState.Selected)
                button.accessibilityValue = String(format: "%d/%d",indexPath.row,i)
                button.tag = indexPath.row
                button.addTarget(self, action: #selector(ProductDetailsVC.checkMarkbuttonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                
                let label = UILabel(frame: CGRectMake(38, CGFloat(titleXPoint), 250, 30))
                
                if productAttributes.values![i].IsPreSelected {
                    
                    button.selected = true
                    
                    let parameters:Dictionary<String,AnyObject> = ["value": productAttributes.values![i].Id,
                                                                   "key":String(format: "product_attribute_%d_%d_%d",productAttributes.ProductId,
                                                                    productAttributes.ProductAttributeId ,
                                                                    productAttributes.Id)]
                    self.dictionaryArray.addObject(parameters)
                    
                    
                }
                else
                {
                    button.selected = false
                }
                
                cell.checkBoxScrollView.addSubview(button)
                label.text = productAttributes.values![i].Name
                
                cell.checkBoxScrollView.addSubview(label)
                buttonXPoint = buttonXPoint + 38
                titleXPoint =  titleXPoint + 38
                
            }
            
            return cell
            
        }
        else {
            //if productAttributes.AttributeControlType == 4{
            var cell: TextBoxTableViewCell! = tableView.dequeueReusableCellWithIdentifier("TextBoxTableViewCell") as? TextBoxTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "TextBoxTableViewCell", bundle: nil), forCellReuseIdentifier: "TextBoxTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("TextBoxTableViewCell") as? TextBoxTableViewCell
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.titleLbl.text = productAttributes.DefaultValue
            cell.titleLbl.tag = indexPath.row;
            
            cell.titleLbl.delegate = self
            cell.titleLbl.addTarget(self, action: #selector(ProductDetailsVC.textFieldDidChanged(_:)), forControlEvents: UIControlEvents.EditingChanged)
            
            if productAttributes.IsRequired {
                
                let labelText = NSMutableAttributedString(string: productAttributes.Name!)
                labelText.appendAttributedString(NSAttributedString(string:"*"))
                let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                
                cell.nameLbl.attributedText = labelText
            }
            else
            {
                cell.nameLbl.text = productAttributes.Name!
            }

            return cell
        }
        
    }
    
    // MARK: - Table View Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
    }
    

    
    
    
    // MARK: UICollectionViewDataSource
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
//    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
//        
//        return CGSizeMake(150, 176)
//        
//    }
    
    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        
        //return CGSizeMake(collectionView.bounds.size.width/2, 265)
        let bound = UIScreen.mainScreen().bounds
        let hr: CGFloat = 274/200
        let w: CGFloat = (bound.width/2.4)
        let h: CGFloat = hr*w
        return CGSize(width: ceil(w),height: ceil(h))
        
    }
    

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return self.relatedProducts.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        let cell : HomeCategoryCollectionCell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeCategoryCollectionCell", forIndexPath: indexPath) as! HomeCategoryCollectionCell
        
        let relatedProduct = relatedProducts[indexPath.item] 
        cell.productImage!.sd_setImageWithURL(NSURL(string: (relatedProduct.DefaultPictureModel?.ImageUrl) ?? ""))
        cell.titleLabel.attributedText = AppUtility.getAttributeString(relatedProduct.Name!, secondString: "\n\(relatedProduct.ProductPrice?.Price ?? "" )", secondStrColor: AppUtility.getPrimaryCellColor())
        
        return cell
    }
    
    
    // MARK: UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        
        let relatedProduct = relatedProducts[indexPath.item]
        if relatedProduct.Id != 0 {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let productDetails = storyBoard.instantiateViewControllerWithIdentifier("ProductDetailsVC") as? ProductDetailsVC
            productDetails!.productId  = relatedProduct.Id
            self.navigationController!.pushViewController(productDetails!, animated: true)
        }
        
    }
    
    
    
    // MARK: - UITextField Delegate
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(textField: UITextField) {
        print("TextField did end editing method called")
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        print("TextField should snd editing method called")
        return true;
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        print("TextField should return method called")
        
        //textFieldString = String(format: "%@",textField.text!)
        //print(textField)
        //print(textFieldString)
        
        textField.resignFirstResponder();
        
        
        return true;
    }


}
