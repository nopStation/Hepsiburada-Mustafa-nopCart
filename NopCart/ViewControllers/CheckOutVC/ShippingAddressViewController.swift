//
//  ShippingAddressViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import DropDown
import SwiftyJSON
import ObjectMapper

class ShippingAddressViewController: UIViewController , UITextFieldDelegate{

    let dropDown = DropDown()
    let countryDropDown = DropDown()
    let stateDropDown = DropDown()
    
    var newAddressesSelected: Bool?
    var selectedExistingAddressId: Int?
    var selectedCountryId: Int?
    var selectedStateId: Int?
    var billingAddressModel = BillingAndShippingAddressModel?()
    var statesModel = StatesModel?()
    var requiredTextFieldArray = [UITextField]()
    
    @IBOutlet weak var addressOptionLabel: UILabel!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var newAddressScrollView: UIScrollView!
    @IBOutlet weak var continueButtonBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var firstNameRequiredLabel: UILabel!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var lastNameRequiredLabel: UILabel!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailRequiredLabel: UILabel!
    
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var companyRequiredLabel: UILabel!
    
    @IBOutlet weak var countryDropDownView: UIView!
    @IBOutlet weak var countryLabel: UILabel!
    
    @IBOutlet weak var stateDropDownView: UIView!
    @IBOutlet weak var stateLabel: UILabel!
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var cityRequiredLabel: UILabel!
    
    @IBOutlet weak var address1TextField: UITextField!
    @IBOutlet weak var address1RequiredLabel: UILabel!
    
    @IBOutlet weak var address2TextField: UITextField!
    @IBOutlet weak var address2RequiredLabel: UILabel!
    
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var zipCodeRequiredLabel: UILabel!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var phoneNumberRequiredTextField: UILabel!
    
    @IBOutlet weak var faxNumberTextField: UITextField!
    @IBOutlet weak var faxNumberRequiredLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.accessNextAddressPage", object: nil);
        
        //self.aPIManager = APIManager()
        
        self.newAddressesSelected = false
        self.selectedExistingAddressId = -1
        self.selectedCountryId = 0
        self.selectedStateId = -1
        self.newAddressScrollView.hidden = true
        
        dropDown.anchorView = self.dropDownView
        dropDown.bottomOffset = CGPoint(x: self.dropDownView.bounds.origin.x, y: self.dropDownView.bounds.origin.y + 40)
        dropDown.selectionAction = {
            [unowned self] (index, item) in
            
            //let address = self.billingAddressModel?.ExistingAddresses![index]
            self.addressOptionLabel.text = item
            
            if index == 0 {
                self.newAddressesSelected = true
                self.selectedExistingAddressId = -1
                self.newAddressScrollView.hidden = false
            }
            else {
                let address = self.billingAddressModel?.ExistingAddresses![index-1]
                self.newAddressesSelected = false
                self.selectedExistingAddressId = address?.Id;
                self.newAddressScrollView.hidden = true
            }
        }
        
        countryDropDown.anchorView = self.countryDropDownView
        countryDropDown.bottomOffset = CGPoint(x: self.countryDropDownView.bounds.origin.x, y: self.countryDropDownView.bounds.origin.y + 40)
        countryDropDown.selectionAction = {
            [unowned self] (index, item) in
            
            self.countryLabel.text = item
            
            let country = self.billingAddressModel!.NewAddress?.AvailableCountries![index]
            
            self.selectedCountryId = Int(country!.Value! as String)
            self.selectedStateId = -1
            self.stateLabel.text = NSLocalizedString("Selectstate", comment: "")
            
            self.stateDropDown.dataSource.removeAll()
            //self.currentlyAvailabeStates.removeAll()
            
            if self.selectedCountryId == 0 {
                return
            }
            
            
            self.getStatesInfo(self.selectedCountryId!)
            
        }
        
        stateDropDown.anchorView = self.stateDropDownView
        stateDropDown.bottomOffset = CGPoint(x: self.stateDropDownView.bounds.origin.x, y: self.stateDropDownView.bounds.origin.y + 40)
        stateDropDown.selectionAction = {
            [unowned self] (index, item) in
            
            let state = self.statesModel!.data![index]
            self.selectedStateId = Int(state.id)
            
            self.stateLabel.text = item
        }
    
    }
    
//    deinit
//    {
//        
//        if self.view != nil {
//            NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillShowNotification, object: self.view.window)
//            NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillHideNotification, object: self.view.window)
//        }
//    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.dropDown.dataSource.removeAll()
        self.countryDropDown.dataSource.removeAll()
        self.stateDropDown.dataSource.removeAll()
        self.requiredTextFieldArray.removeAll()
        
        self.newAddressScrollView.hidden = true
        
        self.addressOptionLabel.text =  NSLocalizedString("Chooseanaddress", comment: "")
        self.countryLabel.text       =  NSLocalizedString("Selectcountry", comment: "")
        self.stateLabel.text         =  NSLocalizedString("Selectstate", comment: "")
        
        self.newAddressesSelected = false
        self.selectedExistingAddressId = -1
        self.selectedCountryId = 0
        self.selectedStateId = -1
        
        
        self.getShippingAddressForm()

        
//        self.showLoading()
//        
//        self.aPIManager!.loadBillingAddressForm(
//            onSuccess: { billingAddress in
//                
//                self.hideLoading()
//                
//                self.newBillingAddress = billingAddress.newBillingAddress!
//                self.allBillingAddresses.appendContentsOf(billingAddress.existingBillingAddresses)
//                self.allBillingAddresses.append(billingAddress.newBillingAddress!)
//                
//                for billingArrdess in self.allBillingAddresses {
//                    
//                    if billingArrdess.id == 0 {
//                        self.dropDown.dataSource.append(NSLocalizedString("NewAddress", comment: ""))
//                    } else {
//                        let title = "\(billingArrdess.firstName!) \(billingArrdess.lastName!), \(billingArrdess.address1!), \(billingArrdess.stateProvinceName!) \(billingArrdess.zipPostalCode!), \(billingArrdess.cityName!), \(billingArrdess.countryName!)"
//                        self.dropDown.dataSource.append(title)
//                    }
//                }
//                self.populateNewAddressForm()
//            },
//            onError: { message in
//                print(message)
//                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
//        })
    }
    
    func populateNewAddressForm() {
        if let firstName = self.billingAddressModel!.NewAddress?.FirstName {
            self.firstNameTextField.text = firstName as String
        } else {
            self.firstNameTextField.text = ""
        }
        self.requiredTextFieldArray.append(self.firstNameTextField)
        
        if let lastName = self.billingAddressModel!.NewAddress?.LastName {
            self.lastNameTextField.text = lastName as String
        } else {
            self.lastNameTextField.text = ""
        }
        self.requiredTextFieldArray.append(self.lastNameTextField)
        
        if let email = self.billingAddressModel!.NewAddress?.Email {
            self.emailTextField.text = email as String
        } else {
            self.emailTextField.text = ""
        }
        self.requiredTextFieldArray.append(self.emailTextField)
        
        if let company = self.billingAddressModel!.NewAddress?.Company {
            self.companyTextField.text = company as String
        } else {
            self.companyTextField.text = ""
        }
        self.companyRequiredLabel.hidden = true
        if self.billingAddressModel!.NewAddress?.CompanyRequired == true {
            self.companyRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.companyTextField)
        }
        
        for country in (self.billingAddressModel?.NewAddress?.AvailableCountries)! {
            self.countryDropDown.dataSource.append(country.Text! as String)
        }
        
        if let city = self.billingAddressModel?.NewAddress?.City {
            self.cityTextField.text = city as String
        } else {
            self.cityTextField.text = ""
        }
        self.cityRequiredLabel.hidden = true
        if self.billingAddressModel?.NewAddress?.CityRequired == true{
            self.cityRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.cityTextField)
        }
        
        if let address1 = self.billingAddressModel?.NewAddress?.Address1 {
            self.address1TextField.text = address1 as String
        } else {
            self.address1TextField.text = ""
        }
        self.address1RequiredLabel.hidden = true
        if self.billingAddressModel?.NewAddress?.StreetAddressRequired == true {
            self.address1RequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.address1TextField)
        }
        
        if let address2 = self.billingAddressModel?.NewAddress?.Address2 {
            self.address2TextField.text = address2 as String
        } else {
            self.address2TextField.text = ""
        }
        self.address2RequiredLabel.hidden = true
        if self.billingAddressModel?.NewAddress?.StreetAddress2Required == true {
            self.address2RequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.address2TextField)
        }
        
        if let zipCode = self.billingAddressModel?.NewAddress?.ZipPostalCode {
            self.zipCodeTextField.text = zipCode as String
        } else {
            self.zipCodeTextField.text = ""
        }
        self.zipCodeRequiredLabel.hidden = true
        if self.billingAddressModel?.NewAddress?.ZipPostalCodeRequired == true {
            self.zipCodeRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.zipCodeTextField)
        }
        
        if let phone = self.billingAddressModel?.NewAddress?.PhoneNumber {
            self.phoneNumberTextField.text = phone as String
        } else {
            self.phoneNumberTextField.text = ""
        }
        self.phoneNumberRequiredTextField.hidden = true
        if self.billingAddressModel?.NewAddress?.PhoneRequired == true {
            self.phoneNumberRequiredTextField.hidden = false
            self.requiredTextFieldArray.append(self.phoneNumberTextField)
        }
        
        if let fax = self.billingAddressModel?.NewAddress?.FaxNumber {
            self.faxNumberTextField.text = fax as String
        } else {
            self.faxNumberTextField.text = ""
        }
        self.faxNumberRequiredLabel.hidden = true
        if self.billingAddressModel?.NewAddress?.FaxRequired == true {
            self.faxNumberRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.faxNumberTextField)
        }
    }
    
    @IBAction func dropDownButtonTapped(sender: AnyObject) {
        if dropDown.hidden {
            dropDown.show()
        } else {
            dropDown.hide()
        }
    }
    
    @IBAction func countryDropDownTapped(sender: AnyObject) {
        if countryDropDown.hidden {
            countryDropDown.show()
        } else {
            countryDropDown.hide()
        }
    }
    
    @IBAction func stateDropDownTapped(sender: AnyObject) {
        if stateDropDown.hidden {
            stateDropDown.show()
        } else {
            stateDropDown.hide()
        }
    }
    
    @IBAction func continueToNextPage(sender: AnyObject) {
        if (self.selectedExistingAddressId == -1 && self.newAddressesSelected == false) {
            AppUtility.showToast("Choose an address", view: self.view)
        }
        else {
            if self.selectedExistingAddressId != -1 {
                
                self.saveBillingAddressFromExistingAddress(2 , existingAddress: self.selectedExistingAddressId!)
 
                
                /*self.showLoading()
                self.aPIManager!.saveBillingOrShippingAddress(2, from: self.selectedExistingAddressId!,
                    onSuccess: {
                        self.hideLoading()
                        self.showToast("Shipping address saved successfully")
                        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueAddressCheckout", object: 2);
                    },
                    onError: { message in
                        self.hideLoading()
                        print(message)
                        self.showToast(message)
                })*/
                
            } else {
                for textField in self.requiredTextFieldArray {
                    if textField.text?.characters.count == 0 {
                        AppUtility.showToast("Please fill up all the required fields.", view: self.view)
                        return
                    }
                }
                
//                if self.selectedCountryId == 0 || self.selectedStateId == -1 {
//                    self.showToast("Please fill up all the required fields.")
//                    return
//                }
                
                let firstNameDictionary = ["value":self.firstNameTextField.text!, "key":"ShippingNewAddress.FirstName"]
                let lastNameDictionary = ["value":self.lastNameTextField.text!, "key":"ShippingNewAddress.LastName"]
                let emailDictionary = ["value":self.emailTextField.text!, "key":"ShippingNewAddress.Email"]
                let companyDictionary = ["value":self.companyTextField.text!, "key":"ShippingNewAddress.Company"]
                let countryDictionary = ["value":String(self.selectedCountryId!), "key":"ShippingNewAddress.CountryId"]
                let stateDictionary = ["value":String(self.selectedStateId!), "key":"ShippingNewAddress.StateProvinceId"]
                let cityDictionary = ["value":self.cityTextField.text!, "key":"ShippingNewAddress.City"]
                let address1Dictionary = ["value":self.address1TextField.text!, "key":"ShippingNewAddress.Address1"]
                let address2Dictionary = ["value":self.address2TextField.text!, "key":"ShippingNewAddress.Address2"]
                let zipCodeDictionary = ["value":self.zipCodeTextField.text!, "key":"ShippingNewAddress.ZipPostalCode"]
                let phoneDictionary = ["value":self.phoneNumberTextField.text!, "key":"ShippingNewAddress.PhoneNumber"]
                let faxDictionary = ["value":self.faxNumberTextField.text!, "key":"ShippingNewAddress.FaxNumber"]
                
                let parameters = [firstNameDictionary, lastNameDictionary, emailDictionary, companyDictionary, countryDictionary, stateDictionary, cityDictionary, address1Dictionary, address2Dictionary, zipCodeDictionary, phoneDictionary, faxDictionary]
                print(parameters)
                
                
                self.saveBillingOrShippingAddress(2, params: parameters)
                /*
                self.showLoading()
                self.aPIManager!.saveBillingOrShippingAddress(2, parameters: parameters,
                    onSuccess: {
                        self.hideLoading()
                        self.showToast("Shipping address saved successfully")

                        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueAddressCheckout", object: 2);
                    },
                    onError: { message in
                        self.hideLoading()
                        print(message)
                        self.showToast(message)
                })*/
                
            }
        }
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    
    
    func saveBillingAddressFromExistingAddress(address:NSInteger, existingAddress: NSInteger) {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.POST, String(format: "%@%@/%d", SessionManager.manager.baseURL,AppConstants.Api.CheckoutSaveAddressID,address ) as String, parameters: ["value":existingAddress], headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        AppUtility.showToast("Shipping address saved successfully", view: self.view)
                        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueAddressCheckout", object: 2);
                        
                        
                    }
                    else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }
    
    
    
    func saveBillingOrShippingAddress(address:NSInteger, params: [AnyObject]) {
        
        
        AppUtility.showLoading(self.view)
        
        let checkoutInfo = String(format: "%@%@/%d",SessionManager.manager.baseURL,AppConstants.Api.CheckoutSaveAddress, address)
        let manager = RequestManager.manager
        let request = NSMutableURLRequest(URL: NSURL(string: checkoutInfo)!)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = AppUtility.getHeaderDic()
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(params, options: [])
        
        manager.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .Success:
                    print("Success")
                    
                    AppUtility.hideLoading(self.view)
                    
                    if let responseValue = response.result.value {
                        let json = JSON(responseValue)
                        
                        print(json)
                        if json["StatusCode"].intValue == 200 {
                            
                          AppUtility.showToast("Shipping address saved successfully", view: self.view)
                          NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueAddressCheckout", object: 2);
                            
                        }else {
                            
                            var errorMessage = "Billing Address"
                            if address == 2 {
                                errorMessage = "Shipping Address"
                            }
                            errorMessage += "couldn't be saved, try again."
                            
                            /*
                             var errorMessage = ""
                             if let errorList = json["ErrorList"].arrayObject {
                             for error in errorList {
                             errorMessage += "\(error as! String)\n"
                             }
                             }*/
                            AppUtility.showToast(errorMessage, view: self.view)
                        }
                    }
                    
                case .Failure:
                    print("Failure")
                    print("Error:\(response.result.error?.localizedDescription)")
                    AppUtility.showToast("\(response.result.error?.localizedDescription)", view: self.view)
                }
        }
  
    }
    
    
    
    func getStatesInfo(countryId:NSInteger) {
        
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@%@/%d", SessionManager.manager.baseURL,AppConstants.Api.StatesByCountryId,countryId ) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if let list = Mapper<StatesModel>().map(json.dictionaryObject) {
                            self.statesModel = list
                            for state in (self.statesModel?.data)! {
                                self.stateDropDown.dataSource.append(state.name! as String)
                                
                            }
                            
                        }
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }
    
    
    func getShippingAddressForm () {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@%@", SessionManager.manager.baseURL,AppConstants.Api.BillingAddressForm ) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        
                        if let list = Mapper<BillingAndShippingAddressModel>().map(json.dictionaryObject) {
                            
                            self.billingAddressModel = list
                            self.dropDown.dataSource.append("New Address")
                            for billingArrdess in (self.billingAddressModel?.ExistingAddresses)! {
                                
                                //if billingArrdess.Id == 0 {
                                  //  self.dropDown.dataSource.append(NSLocalizedString("NewAddress", comment: ""))
                                //} else {
                                    let title = "\(billingArrdess.FirstName!) \(billingArrdess.LastName!), \(billingArrdess.Address1!), \(billingArrdess.StateProvinceName!) \(billingArrdess.ZipPostalCode!), \(billingArrdess.City!), \(billingArrdess.CountryName!)"
                                    self.dropDown.dataSource.append(title)
                                //}
                            }
                            
                            self.populateNewAddressForm()
                            
                        }
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }
    

    
    
    
    // MARK: - UITextField Delegate
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("TextField did begin editing method called")
        
        
    }
    func textFieldDidEndEditing(textField: UITextField) {
        print("TextField did end editing method called")
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        print("TextField should snd editing method called")
        return true;
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        print("TextField should return method called")
        
        textField.resignFirstResponder();
        
        //self.attributsViewHeight.constant = CGFloat(self.totalAttributsViewHeight) - 40.0
        
        return true;
    }
    


}
