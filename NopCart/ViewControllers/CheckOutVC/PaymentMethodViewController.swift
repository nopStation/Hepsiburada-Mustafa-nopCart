//
//  PaymentMethodViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import SwiftyJSON
import ObjectMapper


class PaymentMethodViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{

    var paymentMethodInfo = PaymentMethodModel?()
    var selectedRow = NSInteger ()

    @IBOutlet weak var containerTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.headerView.hidden = true
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.accessNextPage", object: nil);
        
        
        self.getPaymentMethod()
        
        /*
        self.showLoading()
        self.aPIManager?.getPaymentMethod(onSuccess: {
            gotShippingMethod in
            
            self.roundHeaderView()
            self.headerView.hidden = false
            
            self.paymentMethodArray = gotShippingMethod
            
            self.containerTableView.reloadData()
            print(gotShippingMethod)
            
            self.decorateContainerTableView()
            
            self.hideLoading()
            
            }, onError: { message in
                print(message)
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        })*/
    }
    
    func roundHeaderView()
    {
        self.roundView(self.headerView, byRoundingCorners: UIRectCorner.TopLeft.union(.TopRight))
    }
    
    func decorateContainerTableView()
    {
        self.containerTableView.layoutIfNeeded()
        
        if (self.containerTableView.frame.size.height - self.containerTableView.contentSize.height) > 0
        {
            self.tableViewBottomConstraint.constant = (self.containerTableView.frame.size.height - self.containerTableView.contentSize.height) + 10
        }
        
        self.containerTableView.layoutIfNeeded()
        
        self.roundView(self.containerTableView, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight))
        
        self.containerTableView.layer.borderWidth = 2
        self.containerTableView.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    func roundView(aView: UIView, byRoundingCorners corners: UIRectCorner)
    {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: aView.bounds, byRoundingCorners: corners, cornerRadii: CGSizeMake(4, 4)).CGPath
        aView.layer.mask = maskLayer
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let paymentMethods = self.paymentMethodInfo?.PaymentMethods {
            return paymentMethods.count
        }
        return 0
            
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell:PaymentMethodTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! PaymentMethodTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.containerView.layer.cornerRadius = 4.0
        cell.titleLbl.text = self.paymentMethodInfo?.PaymentMethods![indexPath.row].Name
        cell.logoImage!.image = nil
        let imageUrl = self.paymentMethodInfo?.PaymentMethods![indexPath.row].LogoUrl
        Alamofire.request(.GET,imageUrl! ).response{
            (
            request, response, data, error) in
            cell.logoImage!.image = UIImage(data: data!, scale:1)
        }

        
        
        //cell.selectedBtn.tag = indexPath.row
        //cell.selectedBtn.addTarget(self, action: "buttonActionForSelectedRow:", forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        
        if self.paymentMethodInfo?.PaymentMethods![indexPath.row].Selected == true {
            
            cell.containerView.backgroundColor = UIColor.clearColor()
            cell.selectedButton.selected = true
            self.selectedRow = indexPath.row
        }else {
            
            cell.containerView.backgroundColor = UIColor.whiteColor()
            cell.selectedButton.selected = false
            
        }
        
        return cell
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        self.selectedRow = indexPath.row
        
        for checkSelectedIndex in (self.paymentMethodInfo?.PaymentMethods)!{
            checkSelectedIndex.Selected = false
            
        }
        
        self.paymentMethodInfo?.PaymentMethods![indexPath.row].Selected = true
        self.containerTableView.reloadData()
        
        
    }
    
    
//    func buttonActionForSelectedRow(sender:UIButton!)
//    {
//        
//        self.selectedRow = sender.tag
//        
//        
//        for checkSelectedIndex in self.paymentMethodArray{
//            checkSelectedIndex.Selected = 0
//            
//        }
//        
//        self.paymentMethodArray[sender.tag].Selected = 1
//        
//        self.containerTableView.reloadData()
//        
//    }
    
    
    @IBAction func continueToNextPage(sender: AnyObject) {
        
        
        if let paymentMethodSystemName = self.paymentMethodInfo?.PaymentMethods![selectedRow].PaymentMethodSystemName {
            
            self.saveCheckoutPaymentMethod(paymentMethodSystemName)
        }
        //let str = String(format: "%@", self.paymentMethodInfo?.PaymentMethods![selectedRow].PaymentMethodSystemName)
        //self.saveCheckoutPaymentMethod(str)
        
        /*
        self.showLoading()
        let str = String(format: "%@", self.paymentMethodArray[selectedRow].PaymentMethodSystemName)
        aPIManager?.setPaymentMethods(str, onSuccess: {
            
            
            
            self.hideLoading()
            self.showToast("Payment method saved successfully")

            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueCheckout", object: 3);
            },
            onError: { message in
                self.hideLoading()
                print(message)
                self.showToast(message)
        })*/

        
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    
    func saveCheckoutPaymentMethod(paymentName : String) {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.POST, String(format: "%@%@", SessionManager.manager.baseURL,AppConstants.Api.SavePaymentMethod ) as String, parameters: ["value":paymentName], headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                      NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueCheckout", object: 3);
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }

        
    }
    
    func getPaymentMethod() {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@%@", SessionManager.manager.baseURL,AppConstants.Api.GetPaymentMethod ) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                      if let list = Mapper<PaymentMethodModel>().map(json.dictionaryObject) {
                        
                        self.paymentMethodInfo = list
                        
                        self.containerTableView.reloadData()
                        self.decorateContainerTableView()
                        
                        }
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
