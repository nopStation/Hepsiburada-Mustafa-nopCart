//
//  PaymentMethodTableViewCell.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Alamofire

class PaymentMethodTableViewCell: UITableViewCell {

    
    var request: Alamofire.Request?
    

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectedButton: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var logoImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
