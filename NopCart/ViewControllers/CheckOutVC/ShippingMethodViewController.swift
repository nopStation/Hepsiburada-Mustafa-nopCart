//
//  ShippingMethodViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import ObjectMapper

class ShippingMethodViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{

    
    @IBOutlet weak var containerTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    //@IBOutlet weak var tableViewContainerHeightConstraint: NSLayoutConstraint!
    
    
    //var shippingMethodArray = [ShippingMethod]()
    var shippingMethodInfo = ShippingMethodModel?()
    var selectedRow = NSInteger ()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.headerView.hidden = true
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.accessNextPage", object: nil);
        
        self.containerTableView.estimatedRowHeight = 60.0
        self.containerTableView.rowHeight = UITableViewAutomaticDimension
        
        
        self.getShippingMethod()
        
        /*
        self.showLoading()
        self.aPIManager?.getShippingMethod(onSuccess: {
            gotShippingMethod in
        
            self.roundHeaderView()
            self.headerView.hidden = false
        
            self.shippingMethodArray = gotShippingMethod
            
            self.containerTableView.reloadData()
            print(self.shippingMethodArray.count)
            
            self.decorateContainerTableView()
            
            self.hideLoading()
            
            }, onError: { message in
                print(message)
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        })*/
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func roundHeaderView()
    {
        self.roundView(self.headerView, byRoundingCorners: UIRectCorner.TopLeft.union(.TopRight))
    }
    
    func decorateContainerTableView()
    {
        self.containerTableView.layoutIfNeeded()
        
        if (self.containerTableView.frame.size.height - self.containerTableView.contentSize.height) > 0
        {
            self.tableViewBottomConstraint.constant = (self.containerTableView.frame.size.height - self.containerTableView.contentSize.height) + 10
        }
        
        self.containerTableView.layoutIfNeeded()
        
        self.roundView(self.containerTableView, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight))
        
        self.containerTableView.layer.borderWidth = 2
        self.containerTableView.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    func roundView(aView: UIView, byRoundingCorners corners: UIRectCorner)
    {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: aView.bounds, byRoundingCorners: corners, cornerRadii: CGSizeMake(4, 4)).CGPath
        aView.layer.mask = maskLayer
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let shippingMethods = self.shippingMethodInfo?.ShippingMethods {
          return shippingMethods.count
        }
        return 0
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
       let cell:ShippingMethodTableViewCell! = tableView.dequeueReusableCellWithIdentifier("cell") as? ShippingMethodTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
//        if indexPath.row == self.shippingMethodArray.count - 1
//        {
//            self.roundView(cell.containerView, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight))
//        }
        
        cell.titleLbl.text = self.shippingMethodInfo?.ShippingMethods![indexPath.row].Name
        cell.descripTionLbl.text = self.shippingMethodInfo?.ShippingMethods![indexPath.row].Description
        cell.selectedBtn.tag = indexPath.row
        cell.selectedBtn.addTarget(self, action: #selector(ShippingMethodViewController.buttonActionForSelectedRow(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        if self.shippingMethodInfo?.ShippingMethods![indexPath.row].Selected == true {
            
          cell.containerView.backgroundColor = UIColor.clearColor()
          cell.selectedBtn.selected = true
            self.selectedRow = indexPath.row
        } else {
            cell.containerView.backgroundColor = UIColor.whiteColor()
            cell.selectedBtn.selected = false
            
        }
        
        return cell
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        self.selectedRow = indexPath.row

        
        for checkSelectedIndex in (self.shippingMethodInfo?.ShippingMethods!)! {
            checkSelectedIndex.Selected = false
            
        }
        
        self.shippingMethodInfo?.ShippingMethods![indexPath.row].Selected = true
        self.containerTableView.reloadData()

        
    }

    
    func buttonActionForSelectedRow(sender:UIButton!)
    {
        
        self.selectedRow = sender.tag

        
        for checkSelectedIndex in (self.shippingMethodInfo?.ShippingMethods!)! {
           checkSelectedIndex.Selected = false
            
        }
        
        self.shippingMethodInfo?.ShippingMethods![sender.tag].Selected = true
        
        self.containerTableView.reloadData()
        
    }
    
    
    @IBAction func continueToNextPage(sender: AnyObject) {
        
        
        if let shippingMethods = self.shippingMethodInfo?.ShippingMethods {
            
         if shippingMethods.count > 0 {
           let str = String(format: "%@___%@", (self.shippingMethodInfo?.ShippingMethods![selectedRow].Name)!,(self.shippingMethodInfo?.ShippingMethods![selectedRow].ShippingRateComputationMethodSystemName)!)
            self.saveCheckoutShippingMethod(str)
            
        }
        }else {
            
            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueCheckout", object: 2);
            
        }


        
    }
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    
    func saveCheckoutShippingMethod(paymentName : String) {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.POST, String(format: "%@%@", SessionManager.manager.baseURL,AppConstants.Api.SaveShippingMethod ) as String, parameters: ["value":paymentName], headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        AppUtility.showToast("Shipping method saved successfully", view: self.view)
                        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueCheckout", object: 2);
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
        
    }
    
    func getShippingMethod() {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@%@", SessionManager.manager.baseURL,AppConstants.Api.GetShippingMethod ) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        if let list = Mapper<ShippingMethodModel>().map(json.dictionaryObject) {
                            
                            self.shippingMethodInfo = list
                            
                            self.containerTableView.reloadData()
                            self.decorateContainerTableView()
                        }
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
    }

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
