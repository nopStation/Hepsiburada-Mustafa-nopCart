//
//  AuthorizedDotNetViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 1/11/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import DropDown
import SwiftyJSON
import ObjectMapper

class AuthorizedDotNetViewController: UIViewController, UITextFieldDelegate {

    let dropDown = DropDown()
    let monthDropDown = DropDown()
    let yearDropDown = DropDown()

    //var aPIManager : APIManager!
    //var apiManagerClient: APIManagerClient!
    
     var OrderId = NSInteger ()
    
    @IBOutlet weak var cardcodeTextF: UITextField!
    @IBOutlet weak var cardnumberTextF: UITextField!
    @IBOutlet weak var cardholderTextF: UITextField!
    @IBOutlet weak var dropDownSelectedLbl: UILabel!
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var monthDropDownContainerView: UIView!
    
    @IBOutlet weak var yearDropDownContainerView: UIView!
    @IBOutlet weak var yearLbl: UILabel!
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.dropDownSelectedLbl.text = "Visa"
        self.monthLbl.text = "01"

        
        dropDown.anchorView = dropDownContainerView
        dropDown.bottomOffset = CGPoint(x: dropDownContainerView.frame.origin.x-100, y:dropDownContainerView.frame.origin.y-55)

        
        dropDown.dataSource.append("Visa")
        dropDown.dataSource.append("Master card")
        dropDown.dataSource.append("Discover")
        dropDown.dataSource.append("Amex")

        dropDown.selectionAction = {
             (index, item) in
            
            self.dropDownSelectedLbl.text = self.dropDown.dataSource[index] as String
            
            
        }

        yearDropDown.anchorView = yearDropDownContainerView
        yearDropDown.bottomOffset = CGPoint(x: yearDropDownContainerView.frame.origin.x-198, y:yearDropDownContainerView.frame.origin.y - 55)
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: date)
        let year =  components.year
        let month = components.month
        let day = components.day
        self.yearLbl.text = String(format: "%d", year)
        print(year)
        print(month)
        print(day)
        for var i = year ; i<=year+14; i += 1 {
            yearDropDown.dataSource.append(String(format: "%d", i))
        }
        yearDropDown.selectionAction = {
            (index, item) in
            
            self.yearLbl.text = self.yearDropDown.dataSource[index] as String
        }
        
        
        
        
        monthDropDown.anchorView = monthDropDownContainerView
        monthDropDown.bottomOffset = CGPoint(x: monthDropDownContainerView.frame.origin.x-98, y:monthDropDownContainerView.frame.origin.y - 55)
        monthDropDown.dataSource.append("01")
        monthDropDown.dataSource.append("02")
        monthDropDown.dataSource.append("03")
        monthDropDown.dataSource.append("04")
        monthDropDown.dataSource.append("05")
        monthDropDown.dataSource.append("06")
        monthDropDown.dataSource.append("07")
        monthDropDown.dataSource.append("08")
        monthDropDown.dataSource.append("09")
        monthDropDown.dataSource.append("10")
        monthDropDown.dataSource.append("11")
        monthDropDown.dataSource.append("12")
        monthDropDown.selectionAction = {
            (index, item) in
            
            self.monthLbl.text = self.monthDropDown.dataSource[index] as String
            
            
        }

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backBtnAct(sender: AnyObject) {
        
        //self.navigationController?.popViewControllerAnimated(true)
        //self.dismissViewControllerAnimated(true, completion: nil)
        
        //APIManagerClient.sharedInstance.shoppingCartCount = 0

        self.dismissViewControllerAnimated(true, completion:  {
            () -> Void in
            
            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.backHomePage", object: nil);
            
        })
        
 
    }
    
    
    @IBAction func continueBtnAct(sender: AnyObject) {
        
         if self.cardnumberTextF.text?.characters.count != 0  && self.cardcodeTextF.text?.characters.count != 0 {
            
           var params = [String: AnyObject]()
           params["CreditCardNumber"]      = self.cardnumberTextF.text
           params["OrderId"]               = self.OrderId
           params["CreditCardExpireYear"]  = self.yearLbl.text
           params["CreditCardExpireMonth"] = self.monthLbl.text
           params["CreditCardCvv2"]        = self.cardcodeTextF.text

           self.confirmAuthorizeDotNet(params)
        } else {
           
            AppUtility.showToast("Please fill up all the required fields.", view: self.view)

            
        }
   
    }
    
    @IBAction func yearDropDownBtnAct(sender: AnyObject) {
        

        if yearDropDown.hidden {
            yearDropDown.show()
        } else {
            yearDropDown.hide()
        }

    }
    
    
    
    @IBAction func monthDropDownBtnAct(sender: AnyObject) {
        
        if monthDropDown.hidden {
            monthDropDown.show()
        } else {
            monthDropDown.hide()
        }
        
    }
    
    @IBAction func dropDownBtnAct(sender: AnyObject) {
        
        
        if dropDown.hidden {
            dropDown.show()
        } else {
            dropDown.hide()
        }
        

        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------

    func confirmAuthorizeDotNet(params:[String: AnyObject]) {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.POST, String(format: "%@%@", SessionManager.manager.baseURL,AppConstants.Api.CheckAuthorizePayment ) as String, parameters: params, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        appDelegate.cartView.cartLabel.hidden = true
                        appDelegate.cartView.cartLabel.text = "\(0)"
                        
                        self.dismissViewControllerAnimated(true, completion:  {
                            () -> Void in
                            
                            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.backHomePage", object: nil);
                        })
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
        
    }

    
    
    
    
    // MARK: - UITextField Delegate
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("TextField did begin editing method called")
        
        
    }
    func textFieldDidEndEditing(textField: UITextField) {
        print("TextField did end editing method called")
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        print("TextField should snd editing method called")
        return true;
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        print("TextField should return method called")
        
        textField.resignFirstResponder();
        
        //self.attributsViewHeight.constant = CGFloat(self.totalAttributsViewHeight) - 40.0
        
        return true;
    }
    
    

}
