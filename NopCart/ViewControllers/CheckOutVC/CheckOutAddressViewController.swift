//
//  CheckOutAddressViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 1/14/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit

class CheckOutAddressViewController: UIViewController {

    @IBOutlet weak var pageMenuView: UIView!
    
    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.accessNextPage", object: nil);
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(CheckOutAddressViewController.continueToNextAddressPage(_:)), name:"com.notification.continueAddressCheckout", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(CheckOutAddressViewController.accessNextAddressPage(_:)), name:"com.notification.accessNextAddressPage", object: nil)
        
        var controllerArray : [UIViewController] = []
        
        let controller1 = (storyboard?.instantiateViewControllerWithIdentifier("BillingAddressViewController"))! as! BillingAddressViewController
        controller1.title = NSLocalizedString("BillingAddress", comment: "")
        controllerArray.append(controller1)
        
        let controller2 = (storyboard?.instantiateViewControllerWithIdentifier("ShippingAddressViewController"))! as! ShippingAddressViewController
        controller2.title = NSLocalizedString("ShippingAddress", comment: "")
        controllerArray.append(controller2)
        
        
        let parameters: [CAPSPageMenuOption] = [
            .MenuHeight(35),
            .SelectionIndicatorHeight(3.0),
            .MenuItemSeparatorWidth(0),
            .UseMenuLikeSegmentedControl(false),
            .MenuItemSeparatorPercentageHeight(0.1),
            .ViewBackgroundColor(UIColor.clearColor()),
            .ScrollMenuBackgroundColor(UIColor(red: 85/255.0, green: 158/255.0, blue: 199/255.0, alpha: 1)),
            .AddBottomMenuHairline(true),
            .EnableHorizontalBounce(false),
            .MenuMargin(0),
            .MenuItemWidth(self.view.frame.size.width/2),
            .MenuItemFont(UIFont.systemFontOfSize(13.0))
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRectMake(0.0, 0.0, self.pageMenuView.frame.width, self.pageMenuView.frame.height), pageMenuOptions: parameters)
        
        
        self.pageMenuView.addSubview(pageMenu!.view)
        self.addChildViewController(pageMenu!)
        self.pageMenu?.didMoveToParentViewController(self)
    }
    
    deinit {
        if self.view != nil {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.continueAddressCheckout", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.accessNextAddressPage", object: nil)
        }
    }
    
    func accessNextAddressPage(notification: NSNotification) {
        
        pageMenu?.accessableControllerArray.append(0)
        
    }
    
    func continueToNextAddressPage(notification: NSNotification) {
        print((notification.object?.integerValue)!)
        
        if (notification.object?.integerValue) == 2 {
            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueCheckout", object: 1);
        } else {
            self.pageMenu?.moveToPage((notification.object?.integerValue)!)
        }
    }

}
