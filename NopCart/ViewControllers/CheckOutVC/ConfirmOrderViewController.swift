//
//  ConfirmOrderViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import SwiftyJSON
import ObjectMapper

class ConfirmOrderViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var containerScollView: UIScrollView!
    
    @IBOutlet weak var containerTableView: UITableView!
    
    var checkOutOrderInfo = ConfirmOrderModel?()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var containerTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var billingAddressLbl: UILabel!
    @IBOutlet weak var shippingAddressLbl: UILabel!
    
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var shippingLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    var OrderId = NSInteger ()

    var pageMenu : CAPSPageMenu?
    var flag = Bool ()
    
    
    /*
    var payPalConfig = PayPalConfiguration()
    
    var environment:String = PayPalEnvironmentSandbox {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnectWithEnvironment(newEnvironment)
            }
        }
    }
    
    var acceptCreditCards: Bool = true {
        didSet {
            payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }*/

    override func viewWillAppear(animated: Bool) {
        
        
      
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.containerScollView.hidden = true

        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.accessNextPage", object: nil);
        
        self.getConfirmOrderInfo()

        // Do any additional setup after loading the view.
    }

    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    
    func checkoutComplete() {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@%@", SessionManager.manager.baseURL,AppConstants.Api.CheckoutComplete ) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        
                        
                    let PaymentType = json["PaymentType"].intValue
                    self.OrderId = json["OrderId"].intValue
                    let completeOrder = json["CompleteOrder"].intValue
                    
                    if  completeOrder == 1  {
                        
                        let alertMessage = String(format: "Your Order is Complete.\nYour Order Id : %d",self.OrderId)
                        let alertController = UIAlertController(title: "Congratulations", message: alertMessage, preferredStyle: .Alert)
                        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action:UIAlertAction!) in
                            
                            self.appDelegate.cartView.cartLabel.hidden = true
                            self.appDelegate.cartView.cartLabel.text = "\(0)"
                            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.backHomePage", object: nil);
                            
                        }
                        alertController.addAction(OKAction)
                        
                        self.presentViewController(alertController, animated: true, completion:nil)
                        
                    }
                        
                    else if PaymentType == 2 {
                        
                        
                        /*
                         PayPalMobile.initializeWithClientIdsForEnvironments([PayPalEnvironmentProduction: checkOutConplete["PayPal"]["ClientId"].string!, PayPalEnvironmentSandbox: checkOutConplete["PayPal"]["ClientId"].string!])
                         
                         
                         self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
                         //self.payPalConfig.merchantName = "Siva Ganesh Inc."
                         //self.payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.sivaganesh.com/privacy.html")
                         //self.payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.sivaganesh.com/useragreement.html")
                         self.payPalConfig.languageOrLocale = NSLocale.preferredLanguages()[0]
                         self.payPalConfig.payPalShippingAddressOption = .PayPal;
                         PayPalMobile.preconnectWithEnvironment(self.environment)
                         
                         self.PaymentMethodVarification()*/
                        
                    }
                    else if PaymentType == 3 {
                        
                        let authorizedDotNetView = (self.storyboard?.instantiateViewControllerWithIdentifier("AuthorizedDotNetViewController"))! as! AuthorizedDotNetViewController
                        authorizedDotNetView.OrderId  = self.OrderId
                        self.presentViewController(authorizedDotNetView, animated: true, completion: nil)
                        
                    }
                    else if PaymentType == 4 {
                        let checkOutRedirectPayment = (self.storyboard?.instantiateViewControllerWithIdentifier("CheckOutRedirectPaymentViewController"))! as! CheckOutRedirectPaymentViewController
                        checkOutRedirectPayment .orderId  = self.OrderId
                        self.presentViewController(checkOutRedirectPayment, animated: true, completion: nil)
                    }
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }
    
    func getConfirmOrderInfo() {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, String(format: "%@%@", SessionManager.manager.baseURL,AppConstants.Api.CheckoutOrderInformation ) as String, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                     
                      if let list = Mapper<ConfirmOrderModel>().map(json.dictionaryObject) {
                        self.flag = true
                        
                        self.checkOutOrderInfo = list
                        self.billingAddressLbl.text = "Billing Address"
                        self.shippingAddressLbl.text = "Shipping Address"
                        
                        
                        if self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.BillingAddress?.FirstName?.characters.count > 0 {
                            
                            let billingAddress = "\(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.BillingAddress?.FirstName ?? "") \(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.BillingAddress?.LastName ?? "")\n\(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.BillingAddress?.Email ?? "")\n\(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.BillingAddress?.PhoneNumber ?? "")\n\(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.BillingAddress?.Address1 ?? ""), \(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.BillingAddress?.City ?? "")\n\(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.BillingAddress?.CountryName ?? "")"
                            self.billingAddressLbl.text = billingAddress
                            
                            
                        }
                        
                        
                        if self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.ShippingAddress?.FirstName?.characters.count > 0 {
                            
                            
                            let shippingAddress = "\(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.ShippingAddress?.FirstName ?? "") \(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.ShippingAddress?.LastName ?? "")\n\(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.ShippingAddress?.Email ?? "")\n\(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.ShippingAddress?.PhoneNumber ?? "")\n\(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.ShippingAddress?.Address1 ?? ""), \(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.ShippingAddress?.City ?? "")\n\(self.checkOutOrderInfo?.ShoppingCartModel?.OrderReviewData?.ShippingAddress?.CountryName ?? "")"
                            print(shippingAddress)
                            self.shippingAddressLbl.text = shippingAddress
                            
                        }
                        
                        self.subTotalLbl.text    = self.checkOutOrderInfo?.OrderTotalModel?.SubTotal
                        self.shippingLbl.text    = self.checkOutOrderInfo?.OrderTotalModel?.Shipping
                        self.taxLbl.text         = self.checkOutOrderInfo?.OrderTotalModel?.Tax
                        self.discountLbl.text    = self.checkOutOrderInfo?.OrderTotalModel?.SubTotalDiscount
                        self.totalLbl.text       = self.checkOutOrderInfo?.OrderTotalModel?.OrderTotal
                        
                        
                        self.containerTableViewHeight.constant = CGFloat((self.checkOutOrderInfo?.ShoppingCartModel?.items!.count)!) * 150.0
                        self.containerTableView.reloadData()
                        
                        
                        self.containerScollView.hidden = false
                     }
                        
                    }else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
        
    }

    
    //-----------------------------------------------------------------
    // MARK: - User Interaction
    //-----------------------------------------------------------------
    
    @IBAction func confirmBtnAct(sender: AnyObject) {
        
        self.checkoutComplete()
        
        
        
        //let authorizedDotNetView = AuthorizedDotNetViewController(nibName: "AuthorizedDotNetViewController", bundle: nil)
        //authorizedDotNetView.OrderId  = self.OrderId
        //self.parentViewController?.presentViewController(authorizedDotNetView, animated: true, completion: nil)
        
        
        
        /*
        self.showLoading()
       
        self.aPIManager?.checkOutComplete(onSuccess: {
            checkOutConplete in
            
            self.hideLoading()

            let PaymentType = checkOutConplete["PaymentType"].intValue
            self.OrderId = checkOutConplete["OrderId"].intValue
            
            let completeOrder = checkOutConplete["CompleteOrder"].intValue

            
            if  completeOrder == 1  {
                
               let alertMessage = String(format: "Your Order is Complete.\nYour Order Id : %d",self.OrderId)
                
                let alertController = UIAlertController(title: "Congratulations", message: alertMessage, preferredStyle: .Alert)

                let OKAction = UIAlertAction(title: "OK", style: .Default) { (action:UIAlertAction!) in
                    
                    APIManagerClient.sharedInstance.shoppingCartCount = 0
                    NSNotificationCenter.defaultCenter().postNotificationName("com.notification.backHomePage", object: nil);
    
                }
                alertController.addAction(OKAction)
                
                self.presentViewController(alertController, animated: true, completion:nil)
                
            }
            
            else if PaymentType == 2 {
                
                
                /*
              PayPalMobile.initializeWithClientIdsForEnvironments([PayPalEnvironmentProduction: checkOutConplete["PayPal"]["ClientId"].string!, PayPalEnvironmentSandbox: checkOutConplete["PayPal"]["ClientId"].string!])
                
                
                self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
                //self.payPalConfig.merchantName = "Siva Ganesh Inc."
                //self.payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.sivaganesh.com/privacy.html")
                //self.payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.sivaganesh.com/useragreement.html")
                self.payPalConfig.languageOrLocale = NSLocale.preferredLanguages()[0]
                self.payPalConfig.payPalShippingAddressOption = .PayPal;
                PayPalMobile.preconnectWithEnvironment(self.environment)
                
                self.PaymentMethodVarification()*/
   
            }
            else if PaymentType == 3 {

                let authorizedDotNetView = AuthorizedDotNetViewController(nibName: "AuthorizedDotNetViewController", bundle: nil)
                authorizedDotNetView.OrderId  = self.OrderId
                self.presentViewController(authorizedDotNetView, animated: true, completion: nil)

                
            }
//            else if PaymentType == 4 {
//                
//                
//                
//                let checkOutRedirectPayment = (self.storyboard?.instantiateViewControllerWithIdentifier("CheckOutRedirectPaymentViewController"))! as! CheckOutRedirectPaymentViewController
//                checkOutRedirectPayment .orderId  = self.OrderId
//                self.presentViewController(checkOutRedirectPayment, animated: true, completion: nil)
//
//            }


            },
            onError: { message in
                self.hideLoading()
                print(message)
                self.showToast(message)
        })*/
   
    }
    
    
    //func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        
        //if buttonIndex == 1 {
            
          
            
        //}
      
        
    //}
    
    /*
    func PaymentMethodVarification() {
        
        
        let newString = self.totalLbl.text!.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        let newString2 = newString.stringByReplacingOccurrencesOfString(",", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        print(newString2)
        
        let item1 = PayPalItem(name: "", withQuantity: 1, withPrice: NSDecimalNumber(string: newString2), withCurrency: "USD", withSku: "SivaGanesh-0001")
    
        
        let items = [item1]
        let subtotal = PayPalItem.totalPriceForItems(items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.decimalNumberByAdding(shipping).decimalNumberByAdding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Total Price", intent: .Sale)
        
        //payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            presentViewController(paymentViewController, animated: true, completion: nil)
        }
        else {
            
            print("Payment not processable: \(payment)")
        }
      }*/
    
    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
         if let items = self.checkOutOrderInfo?.ShoppingCartModel?.items {
            
            return items.count
        }

        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
            var cell: ConfirmOrderTableViewCell! = tableView.dequeueReusableCellWithIdentifier("ConfirmOrderTableViewCell") as? ConfirmOrderTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "ConfirmOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "ConfirmOrderTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("ConfirmOrderTableViewCell") as? ConfirmOrderTableViewCell
            }
        
            cell.itemImageView.layer.borderWidth=1.0
            cell.itemImageView.layer.masksToBounds = false
            cell.itemImageView.layer.borderColor = UIColor.whiteColor().CGColor
            cell.itemImageView.layer.cornerRadius = 13
            cell.itemImageView.layer.cornerRadius = cell.itemImageView.frame.size.height/2
            cell.itemImageView.clipsToBounds = true
        
           cell.itemImageView!.image = nil
        
            let imageUrl = self.checkOutOrderInfo?.ShoppingCartModel?.items![indexPath.row].picture!.ImageUrl
            Alamofire.request( .GET,  imageUrl! ).response{
                (
                request, response, data, error) in
                cell.itemImageView!.image = UIImage(data: data!, scale:1)
            }
        
            cell.titleLbl.text =  self.checkOutOrderInfo?.ShoppingCartModel?.items![indexPath.row].ProductName
            cell.UnitPrice.text = self.checkOutOrderInfo?.ShoppingCartModel?.items![indexPath.row].UnitPrice
            cell.subTotal.text = self.checkOutOrderInfo?.ShoppingCartModel?.items![indexPath.row].SubTotal
            cell.quantity.text = String(format: "Quantity:%d", (self.checkOutOrderInfo?.ShoppingCartModel?.items![indexPath.row].Quantity)!) ?? ""
            cell.selectionStyle = UITableViewCellSelectionStyle.None

            //self.roundRectToAllView(cell.containerView)
        
            if indexPath.row == (self.checkOutOrderInfo?.ShoppingCartModel?.items!.count)!-1 {
                cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            }
        
            return cell
        }

    
    
    
    func roundRectToAllView(view: UIView )->Void{
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.whiteColor()
        view.layer.shadowColor = UIColor.darkGrayColor().CGColor
        view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        view.layer.shadowOpacity = 0.50
        view.layer.shadowRadius = 1
        view.layer.masksToBounds = true
        view.clipsToBounds = false
        
        
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
//        if tableView == containerTableView {
//            let productDetailsViewController = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
//            productDetailsViewController.productId  = self.checkOutOrderInfo.Items[indexPath.row].ProductId
//            self.navigationController!.pushViewController(productDetailsViewController, animated: true)
//            
//            
//        }
        
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    /*
    // MARK: - PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(paymentViewController: PayPalPaymentViewController!) {
        print("PayPal Payment Cancelled")
        
        paymentViewController?.dismissViewControllerAnimated(true, completion: {
            () -> Void in
            // send completed confirmaion to your server
            
            APIManagerClient.sharedInstance.shoppingCartCount = 0

            let array = (self.navigationController?.viewControllers)! as NSArray
            self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
            
        })
        
    }
    
    func payPalPaymentViewController(paymentViewController: PayPalPaymentViewController!, didCompletePayment completedPayment: PayPalPayment!) {
        
        print("PayPal Payment Success !")
 
        print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")

        
        
        let dic = completedPayment.confirmation["response"] as! NSDictionary
        let paymentId  = dic.valueForKey("id") as! NSString
        
        print(paymentId)
        
        
        paymentViewController?.dismissViewControllerAnimated(true, completion:nil)
        
        /*
        [client: {
        environment = sandbox;
        "paypal_sdk_version" = "2.12.9";
        platform = iOS;
        "product_name" = "PayPal iOS SDK";
        }, response_type: payment, response: {
        "create_time" = "2016-01-08T12:20:58Z";
        id = "PAY-8RK22991L8553672XK2H2TGQ";
        intent = sale;
        state = approved;
        }]*/
        
        self.showLoading()

        aPIManager?.paypalConfirm(paymentId, orderId: self.OrderId, onSuccess: {

            self.hideLoading()
            self.showToast("PayPal Payment Successfull!")
            
            APIManagerClient.sharedInstance.shoppingCartCount = 0
            let array = (self.navigationController?.viewControllers)! as NSArray
            self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
            
            },
            onError: { message in
                self.hideLoading()
                print(message)
                self.showToast(message)

                APIManagerClient.sharedInstance.shoppingCartCount = 0
                
                let array = (self.navigationController?.viewControllers)! as NSArray
                self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
                
        })
    }*/

}
