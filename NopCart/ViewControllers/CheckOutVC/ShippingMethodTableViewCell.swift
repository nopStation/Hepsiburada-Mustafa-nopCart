//
//  ShippingMethodTableViewCell.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

class ShippingMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectedBtn: UIButton!
    @IBOutlet weak var descripTionLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
