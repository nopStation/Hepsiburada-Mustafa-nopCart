//
//  CheckOutViewController.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 12/18/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

class CheckOutViewController: UIViewController {

    @IBOutlet weak var pageMenuView: UIView!
    
    var pageMenu : CAPSPageMenu?
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(CheckOutViewController.continueToNextPage(_:)), name:"com.notification.continueCheckout", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(CheckOutViewController.accessNextPage(_:)), name:"com.notification.accessNextPage", object: nil)

        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(CheckOutViewController.backHomePage(_:)), name:"com.notification.backHomePage", object: nil)

        
        var controllerArray : [UIViewController] = []
        
        let controller1 = (storyboard?.instantiateViewControllerWithIdentifier("CheckOutAddressViewController"))! as! CheckOutAddressViewController
        controller1.title = NSLocalizedString("Address", comment: "")
        controllerArray.append(controller1)
        
        
        let controller3 = (storyboard?.instantiateViewControllerWithIdentifier("ShippingMethodViewController"))! as! ShippingMethodViewController
        controller3.title = NSLocalizedString("Shipping", comment: "")
        controllerArray.append(controller3)
        
        let controller4 = (storyboard?.instantiateViewControllerWithIdentifier("PaymentMethodViewController"))! as! PaymentMethodViewController
        controller4.title = NSLocalizedString("Payment", comment: "")
        controllerArray.append(controller4)
        
        let controller5 = (storyboard?.instantiateViewControllerWithIdentifier("ConfirmOrderViewController"))! as! ConfirmOrderViewController
        controller5.title = NSLocalizedString("ConfirmOrder", comment: "")
        controllerArray.append(controller5)
        
        let parameters: [CAPSPageMenuOption] = [
            .MenuHeight(35),
            .SelectionIndicatorHeight(3.0),
            .MenuItemSeparatorWidth(0),
            .UseMenuLikeSegmentedControl(false),
            .MenuItemSeparatorPercentageHeight(0.1),
            .ViewBackgroundColor(UIColor.clearColor()),
            .ScrollMenuBackgroundColor(UIColor(red: 68/255.0, green: 124/255.0, blue: 159/255.0, alpha: 1)),
            .AddBottomMenuHairline(true),
            .EnableHorizontalBounce(false),
            .MenuMargin(0),
            .MenuItemWidth(self.view.frame.size.width/4),
            .MenuItemFont(UIFont.systemFontOfSize(13.0))
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRectMake(0.0, 0.0, self.pageMenuView.frame.width, self.pageMenuView.frame.height), pageMenuOptions: parameters)
        
        
        self.pageMenuView.addSubview(pageMenu!.view)
        self.addChildViewController(pageMenu!)
        self.pageMenu?.didMoveToParentViewController(self)
        
    }

    deinit {
        if self.view != nil {
         NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.continueCheckout", object: nil)
         NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.accessNextPage", object: nil)
         NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.backHomePage", object: nil)

        }
    }
    
    
    
    
    func accessNextPage(notification: NSNotification) {
        
        pageMenu?.accessableControllerArray.append(0)
        
    }
    
    
    func backHomePage(notification: NSNotification) {
       
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)

        
    }
    
    func continueToNextPage(notification: NSNotification) {
        print((notification.object?.integerValue)!)
        
        self.pageMenu?.moveToPage((notification.object?.integerValue)!)
    }
    
    @IBAction func dismissViewController(sender: AnyObject) {
        //self.dismissViewControllerAnimated(true, completion: nil)
        self.navigationController?.popViewControllerAnimated(true)
        
    }
}


