//
//  ViewController.swift
//  NopCart
//
//  Created by BS85 on 5/3/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import MBProgressHUD

class HomeVC: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    
    
    var homePageBannerList : [HomePageBannerModel]!
    var homePageCategoryList : [HomePageCategoryModel]?
    var bannerList : [String] = []
    var categoryArray = NSMutableArray()

    
    //---------------------------------------------------------------------
    //MARK: - ViewController LifeCycle
    //---------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.customNavBarView.navigationBackButton.hidden = true
        self.customNavBarView.menuButton.hidden = false
        self.customNavBarView.navigationSearchButton.hidden = true
        
        self.tableView.registerNib(UINib.init(nibName: "ImageSliderHeaderCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "ImageSliderHeaderCell")
        self.tableView.registerNib(UINib.init(nibName: "HomeCategoryCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "HomeCategoryCell")
        
        self.getHomePageBanner()
    }

    
    
    // MARK: - Table view data source

    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
      
        if indexPath.section == 0 {
            let bound = UIScreen.mainScreen().bounds
            let hr: CGFloat = 256/190
            let w: CGFloat = (bound.width/2)
            let h: CGFloat = hr*w
            return h
        }else {
        
            let bound = UIScreen.mainScreen().bounds
            let hr: CGFloat = 272/190
            let w: CGFloat = (bound.width/2)
            let h: CGFloat = hr*w
            return h
            
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if self.categoryArray.count > 0 {
         return self.categoryArray.count+1
        }
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0 {
          
            let cell: ImageSliderHeaderCell! = tableView.dequeueReusableCellWithIdentifier("ImageSliderHeaderCell") as? ImageSliderHeaderCell

            cell.imageArray = self.bannerList
            cell.populateImage()
            return cell
        }
            
        else {
            
        var cell:HomeCategoryCell! = tableView.dequeueReusableCellWithIdentifier("HomeCategoryCell") as? HomeCategoryCell
        let categoryList = self.categoryArray[indexPath.section-1]
  
            
        if(cell == nil)
           {
            cell = HomeCategoryCell(style: UITableViewCellStyle.Subtitle,reuseIdentifier:"HomeCategoryCell")
         }
        
            
        if indexPath.section == 1 {
            
         cell.titleLable.text = "Featured Products"
            
        }else if indexPath.section == self.categoryArray.count {
            
          cell.titleLable.text = "Featured Manufacturers"
        }
        else {
            
           let homePageCategory = categoryList as! [HomePageCategoryModel]
           cell.titleLable.text =  homePageCategory[0].category?.Name
        }
            
         cell.getCategoryArray(categoryList as! [AnyObject] , sectionIndex:indexPath.section-1 , totalSection: self.categoryArray.count)
         return cell
        }
    }
    
    
    
    // MARK: - Table view delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // let cell = tableView.cellForRowAtIndexPath(indexPath) as! CartTableViewCell
        
    }
    
    //-----------------------------------------------------------------
    // MARK: - User Interaction
    //-----------------------------------------------------------------
    
    
    @IBAction func searchBtnAction(sender: AnyObject) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let searchView = storyBoard.instantiateViewControllerWithIdentifier("SearchVC") as? SearchVC
        self.navigationController?.pushViewController(searchView!, animated: true)
        
        
    }
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    func getHomePageBanner() {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, AppConstants.Api.HomePageBanner, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                    if let list = Mapper<HomePageBannerModel>().mapArray(json["Data"].arrayObject) {
                        
                        self.homePageBannerList = list
                        
                        for sliderImg in list  {
                          
                            if let imageUrl = sliderImg.ImageUrl {
                             self.bannerList.append(imageUrl)
                            }
                        }
                        self.getHomePageFeaturedProduct()
                       }
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }
    
    func getHomePageFeaturedProduct() {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, AppConstants.Api.HomePageFeaturedProduct, parameters:["thumbPictureSize":"320"], headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        if let list = Mapper<HomePageFeaturedModel>().mapArray(json["Data"].arrayObject) {
                            
                            self.categoryArray.addObject(list)
                            self.getHomePageCategories()
                        }
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }

    func getHomePageCategories() {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, AppConstants.Api.HomePageCategory, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        if let list = Mapper<HomePageCategoryModel>().mapArray(json["Data"].arrayObject) {
                            
                            self.homePageCategoryList = list
                            
                            for homePageCategory in list {
                                let homePageCategoryModelArray = [homePageCategory]
                               // self.categoryArray.addObject(homePageCategory.product!)
                                self.categoryArray.addObject(homePageCategoryModelArray)
                            }
                            self.getHomePageManufacture ()
                        }
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
        
    }

    
    func getHomePageManufacture() {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, AppConstants.Api.HomePageManufacture, parameters:["thumbPictureSize":"320"], headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        if let list = Mapper<HomePageManufactureModel>().mapArray(json["Data"].arrayObject) {
                            
                            self.categoryArray.addObject(list)
                            self.tableView.reloadData()
                            
                            self.getAllCatogory()

                        }
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
    }
    
    func getAllCatogory() {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, AppConstants.Api.Categories, parameters:nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                    if json["Count"].intValue > 0 {
                       appDelegate.cartView.cartLabel.hidden = false
                       appDelegate.cartView.cartLabel.text =  "\(json["Count"].intValue)"
                    }
                        
                        if let getAllCategories = Mapper<CategoryModel>().mapArray(json["Data"].arrayObject) {
                            
                            for category in getAllCategories {
                                category.subCategories = []
                                for subCategory in getAllCategories {
                                    if category.Id == subCategory.ParentCategoryId {
                                        category.subCategories.append(subCategory)
                                    }
                                }
                            }
                            
                            
                            for lowestCategory in getAllCategories {
                                
                                let parentOfLowestCategory = lowestCategory.ParentCategoryId
                                for subCategory in getAllCategories {
                                    let subCategoryId = subCategory.Id
                                    if parentOfLowestCategory == subCategoryId {
                                        let parentOfSubCategory = subCategory.ParentCategoryId
                                        for parentCategory in getAllCategories {
                                            let parentCategoryId = parentCategory.Id
                                            if parentOfSubCategory == parentCategoryId {
                                                if parentCategory.ParentCategoryId == 0 {
                                                    lowestCategory.isLowestItem = true
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                                
                            }
                            
                         appDelegate.categoryList = getAllCategories
                            
                        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadTableView", object: 1);
                            
                        }
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

