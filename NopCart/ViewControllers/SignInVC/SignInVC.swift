//
//  SignInVC.swift
//  NopCart
//
//  Created by BS-125 on 6/17/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper


class SignInVC: BaseVC , UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
   
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!
    
    
    
    
    
    
    //---------------------------------------------------------------------
    //MARK: - ViewController LifeCycle
    //---------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customNavBarView.navigationBackButton.hidden = false
        self.customNavBarView.menuButton.hidden = true
        self.customNavBarView.navigationSearchButton.hidden = true

        
        
    }
    override func viewWillAppear(animated: Bool) {
        
        
    }
    
    
    
    //-----------------------------------------------------------------
    // MARK: - User Interaction
    //-----------------------------------------------------------------
    

    @IBAction func login(sender: UIButton) {
        
        let email = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if email.characters.count == 0 {
            AppUtility.showToast("Enter E-Mail address", view: self.view)
            return
        }
        
        if self.isValidEmail(email) == false {
            AppUtility.showToast("Invalid E-Mail address", view: self.view)
            return
        }
        
        let password = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if password.characters.count == 0 {
            AppUtility.showToast("Enter password", view: self.view)
            return
        }
        
        self.proceedToLogIn()
    }
    
    func isValidEmail(testEmail:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testEmail)
        
    }
    
    
    @IBAction func register(sender: UIButton) {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let registerViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(registerViewC, animated: true)
    }
    
    
    //-----------------------------------------------------------------
    // MARK: - API
    //-----------------------------------------------------------------
    
    
    func proceedToLogIn()
    {
        var params = [String: AnyObject]()
        params["email"] = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["password"] = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.POST, AppConstants.Api.Login, parameters: params, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        let token = json["Token"].string
                        let defaults = NSUserDefaults.standardUserDefaults()
                        defaults.setObject(token, forKey: "accessToken")
                        defaults.synchronize()
                        
                        self.getShoppingCartCount()
                        
                        //AppUtility.showToast("Login Successful", view: self.view)
                        //AppUtility.backToRootView(self.navigationController!)

                       }
                    else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                        for error in errorList {
                            errorMessage += "\(error as! String)\n"
                          }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)

                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }

        /*
        
        aPIManager!.signInCustomer(params,
                                   onSuccess: {
                                    self.hideLoading()
                                    self.showToast("Login Successful")
                                    self.performSelector("backToRootView:", withObject: nil, afterDelay:2)
            },
                                   onError: {
                                    message in
                                    self.hideLoading()
                                    self.showToast(message)
        })*/
    }

    
    func getShoppingCartCount()
    {
        
        AppUtility.showLoading(self.view)
        
        let manager = RequestManager.manager
        manager.request(.GET, AppConstants.Api.Categories, parameters: nil, headers:AppUtility.getHeaderDic()).responseJSON {
            response in
            
            switch response.result {
            case .Success:
                print("Success")
                AppUtility.hideLoading(self.view)
                
                if let responseValue = response.result.value {
                    
                    let json = JSON(responseValue)
                    print("json::\(json)")
                    if json["StatusCode"].intValue == 200 {
                        
                        appDelegate.cartView.cartLabel.text =  "\(json["Count"].intValue)"
                        AppUtility.showToast("Login Successful", view: self.view)
                        AppUtility.backToRootView(self.navigationController!)
                        
                    }
                    else {
                        var errorMessage = ""
                        if let errorList = json["ErrorList"].arrayObject {
                            for error in errorList {
                                errorMessage += "\(error as! String)\n"
                            }
                        }
                        AppUtility.showToast(errorMessage, view: self.view)
                        
                    }
                }
                
            case .Failure:
                print("Failure")
                print("Error:\(response.result.error?.localizedDescription)")
                AppUtility.showToast("\(response.result.error!.localizedDescription ?? "")", view: self.view)
            }
        }
    }

    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder();
        return true;
    }

}
