//
//  AppDelegate.swift
//  NopCart
//
//  Created by BS85 on 5/3/16.
//  Copyright © 2016 BS85. All rights reserved.
//

import UIKit
//import SlideMenuControllerSwift
import MFSideMenu
import IQKeyboardManager
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var BaseURL: String?

    //Slide Menu
    var slideMenuController = MFSideMenuContainerViewController()
    //Category List VC
    
    var homeVC : HomeVC?
    var categoryListVC : CategoryListVC?
    var categoryList = [CategoryModel]()
    var tempCategoryList = [Children]()
    var statusBar = UIView ()
    var navigationController = UINavigationController ()
    var cartView = CartButtonView ()
    var categoryDic = NSMutableDictionary()
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 20
        slideMenuSetup()
        
        
        return true
    }
    
    
    func slideMenuSetup() {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        self.cartView = NSBundle.mainBundle().loadNibNamed("CartButtonView", owner: nil, options: nil)[0] as! CartButtonView

        categoryListVC = storyBoard.instantiateViewControllerWithIdentifier("CategoryListVC") as? CategoryListVC
        
        self.homeVC = storyBoard.instantiateViewControllerWithIdentifier("HomeVC") as? HomeVC
        slideMenuController.leftMenuViewController = categoryListVC
        slideMenuController.centerViewController = self.homeVC
        
        
        navigationController = UINavigationController(rootViewController: slideMenuController)
        navigationController.navigationBarHidden = true
        window!.rootViewController = navigationController
        
        //Status Bar
         self.statusBar=UIView(frame: CGRectMake(0, 0,UIScreen.mainScreen().bounds.size.width, 20))
        self.statusBar.backgroundColor=UIColor.whiteColor()
        self.window!.rootViewController!.view.addSubview(self.statusBar)
        
        
        self.addCartButtonView()
        
        self.window!.makeKeyAndVisible()
    }

    func addCartButtonView () {
        
        cartView.frame = CGRectMake(UIScreen.mainScreen().bounds.size.width-70, UIScreen.mainScreen().bounds.size.height-90, 50, 50)
        self.window?.rootViewController!.view.addSubview(cartView)
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

